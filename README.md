## Requisitos
- Git
- PHP 7.2+
- Apache 2.4+ / NGNIX
- MariaDB 10.3+
- Yarn version stable
- Agregar los repositorios correspondientes para la instalacion

## Preparación del Servidor


#### Actualizar servidor
```sh
sudo yum update
``` 

#### Instalar Php
```sh
sudo yum install php
``` 

#### Instalar paquetes de php
```sh
sudo yum install php7.3 libapache2-mod-php7.3 php7.3-cli php7.3-common php7.3-opcache php7.3-curl php7.3-mbstring php7.3-mysql php7.3-zip php7.3-xml php7.3-bcmath
php-pear php7.3-json php7.3-pdo
```

##### Instalar Apache (Se puede hacer con nginx, tambien se puede correr con NGNIX)
```sh
sudo yum install apache2
# Iniciar en el arranque
sudo systemctl enable apache2.service
sudo systemctl start apache2
```

##### GIT
```sh
sudo yum install git
```

##### Instalacion Composer
```sh
sudo yum install wget php-cli php-zip unzip
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
HASH="$(wget -q -O - https://composer.github.io/installer.sig)"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
```

Despues de hacer es ultimo comando debe aparecer un mensaje: Installer verified
Por ultimo hejecutamos este ultimo comando para poder ejecutar composer luego
```sh
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
```

##### Instalacion Yarn
se debe terner instalado previamente Nodejs y npm y luego ejecutar:
```sh
npm install -g yarn
```

## Instalar Aplicación
##### Descargar y configurar
El siguiente paso, es descargar y configurar la aplicación.

Para ello, nos situamos en donde queremos la aplicación y clonamos el repositorio:
```sh
cd /var/www

# Va a pedir usuario y contraseñas del repositorio
# Lo hacemos con el usuario apache así cuando se haga el pull automático no hay problema de permisos
sudo git clone https://gitlab.com/josuesmith/como-viajo-jujuy.git
```

Una vez que termina de descargar la aplicación, para descargar las dependencias debe ubicarse dentro de la raiz del proyecto y ejecutar el comando:
```sh
composer install
```

Este comando realizara la configuracion de la conexion con la base de datos si es la primera vez que se ejecuta el mismo o si no existiera el archivo parameters.yml:

1. La configuración de la base de datos. Se irán solicitando en modo interactivo los datos para establecer la conexión. Los datos requeridos son:
      
              • database_host (localhost por defecto - *importante)
              
              • database_port (3306 por defecto - *importante)
              
              • database_name (microcredito por defecto - *importante)
              
              • database_user (root por defecto -  * importante )
              
              • database_password( null por defecto - * importante )

## Recursos que no deben tener acceso público
Se deberá proteger el directorio {projecto}/web/uploads y todos sus subdirectorios, dependiendo del tipo de servidor web donde se instale la aplicación. 
Si el servidor web es apache deberá crearse un archivo .htaccess en ese directorio y deberá   contener lo siguiente: “Deny from all”.