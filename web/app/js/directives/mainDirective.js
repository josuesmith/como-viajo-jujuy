app.directive('inputCuit', ['$timeout', '$parse', function ($timeout, $parse) {
		var controller = ['$scope', 'EmpresaService', '$timeout', function ($scope, EmpresaService, $timeout) {
				console.log($scope.object);
				$scope.initDirectiveCuit = function () {
					jQuery.validator.addMethod("validateCuit", function (value, element) {
						return ($scope.object.$$stateCuit == 1);
					}, "CUIT ya ingresado en el sistema");
					$scope.addValidateCuit(false);
					$("#cuit").focus();
				}
				$scope.checkEmpresaByCuit = function () {
					$("#cuit").rules("remove");
					$("#cuit-error").remove();
					$scope.addValidateCuit(false);
					$scope.object.$$stateCuit = 0;
					if ($scope.object.cuit != "" && ($scope.object.cuit != $scope.object.$$lastCuit)) {
						if ($("#cuit").valid()) {
							EmpresaService.existsEmpresaByCuit($scope.object.cuit).then(function (response) {
								if (response.data.exists) {
									$scope.object.$$stateCuit = 2;
								} else {
									$scope.object.$$stateCuit = 1;
								}
								$scope.addValidateCuit(true);
							}, function (response) {
							}).finally(function () {
								$("#cuit").valid();
							});
						}
					} else {
						if (($scope.object.cuit == $scope.object.$$lastCuit) && ($scope.object.cuit != undefined) && ($scope.object.$$lastCuit != undefined)) {
							$scope.object.$$stateCuit = 1;
						}
					}
					$("#cuit").valid();
				}
				$scope.addValidateCuit = function (opt) {
					$("#cuit").rules("add", {
						required: true,
						maxlength: "11",
						minlength: "10",
						validateCuit: opt
					});

				};
			}];
		return {
			restrict: 'E',
			require: ['inputCuit'],
			scope: {
				object: '='
			},
			templateUrl: baseUrlTemplates + 'templates/inputCuit.html',
			controller: controller,
			replace: true,
			link: function ($scope, $element, $attrs) {
				$scope.initDirectiveCuit();
			}
		}
	}]);