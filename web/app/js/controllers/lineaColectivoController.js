app.controller("LineaColectivoListadoController", ["$scope", "LineaColectivoService", "$controller", "utilsService", function ($scope, LineaColectivoService, $controller, utilsService) {

        $controller('baseViewCtrl', {$scope: $scope});
        $controller('mainController', {$scope: $scope});

        $scope.paramPagination.page = 1;
        $scope.paramPagination.sortkey = "nombre";
        $scope.paramPagination.direction = "ASC";

        $scope.filterCriteria = {
            nombre: null
        };

        $scope.fetchResult = function () {
            $scope.startMain();
            var criteria = $scope.filterCriteria;
            LineaColectivoService.search(criteria, $scope.paramPagination)
                    .then(function (response) {
                        if (response.code === 200) {
                            $scope.colectivos = response.data.listado;
                            $scope.setValuePagination(response.data);
                        } else {
                            utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                        }
                    }, function (response) {
                    }).finally(function () {
                $scope.stopMain();
            });
        };

        $scope.clear = function () {
            $scope.filterCriteria.nombre = null;
            $scope.fetchResult();
        };

        function iniciar() {
            $scope.fetchResult();
        }
        ;

        /*************************************************/
        $scope.init = function (title) {
            $scope.title = title;
            iniciar();
            $("#jsForm").validate({
                ignore: []
            });
        };

    }]);

app.controller("LineaColectivoBaseController", ["$scope", "$controller", "LineaColectivoService", "utilsService", "$timeout", function ($scope, $controller, LineaColectivoService, utilsService, $timeout) {
        $controller('baseViewCtrl', {$scope: $scope});
        $controller('mainController', {$scope: $scope});

        $scope.languaje = {
            inputTooShort: function () {
                return "Ingrese mas de 2 caracteres...";
            },
            searching: function () {
                return "Buscando…";
            },
            noResults: function () {
                return "No se encontraron resultados";
            }
        };

        $scope.initLineaColectivo = function (data) {            
            $scope.data = data;

            $("#formLineaColectivo").validate({
                ignore: [],
                messages: {
                    nombre: {
                        required: "Debe completar este campo."
                    },
                    empresa: {
                        required: "Debe completar este campo."
                    }
                },
                rules: {
                    nombre: {
                        required: true
                    },
                    empresa: {
                        required: true
                    }
                }
            });
            $timeout(function () {
                $scope.reset();
                $scope.loadSearchEmpresas("#empresas", null);
                $("#empresas").on("select2:select", function () {
                    $("#empresas").valid();
                });
                jQuery.validator.addMethod("validateEmpresa", function (value, element) {
                    var tempEmp = $("#empresas").select2('data');
                    return tempEmp != null && tempEmp.length > 0;
                }, "Debe seleccionar una empresa");
                $scope.$broadcast('finishInitLineaColectivo');
            });
        };

        $scope.reset = function () {
            $scope.colectivo = {
                nombre: null,
                descripcion: null
            };
            $('#empresas').val(null).trigger('change');
            $scope.empresa = {
                seleccionada: null
            };
        };

        $scope.setLineaColectivo = function (colectivo) {
            $scope.colectivo.id = colectivo.id;
            $scope.colectivo.nombre = colectivo.nombre;
            $scope.colectivo.descripcion = colectivo.descripcion;
            var tempSelectionEmp = [];
            tempSelectionEmp.push({id: colectivo.empresa.id, nombre:  colectivo.empresa.nombre, selected: true});
            $('#empresas').find('option').remove();
            $("#empresas").select2('destroy');
            $timeout(function () {
                $scope.loadSearchEmpresas("#empresas", tempSelectionEmp);
            });
        };

        $scope.getLineaColectivo = function (id) {
            $scope.startMain();
            LineaColectivoService.getLineaColectivoById(id).then(function (response) {
                $scope.setLineaColectivo(response.data);
            }, function (response) {
                utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
            }).finally(function () {
                $scope.stopMain();
            });
        };

        $scope.loadSearchEmpresas = function (elem, data) {
            $(elem).select2({
                width: '100%',
                theme: "bootstrap4",
                data: data,
                ajax: {
                    url: baseUrlByRol + "ajax/empresas/search",
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            q: params.term
                        }
                        return query;
                    },
                    processResults: function (data) {
                        angular.forEach(data.data, function (item) {
                            item.id = item.id;
                        });
                        return {
                            results: data.data
                        };
                    },
                    cache: true
                },
                placeholder: 'Busque la empresa',
                minimumInputLength: 2,
                language: $scope.languaje,
                templateResult: function (data) {
                    if (data.loading) {
                        return data.text;
                    }
                    try {
                        return data.nombre;
                    } catch (e) {
                        console.log(e);
                    }
                },
                templateSelection: function (data) {
                    if (data.id == "") {
                        return data.text;
                    } else {
                        return data.nombre;
                    }
                },
                escapeMarkup: function (m) {
                    return m;
                }
            });
        };

        $scope.getEmpresas = function () {
            var items = $("#empresas").select2('data');
            return items[0].id;
        };
    }]);

app.controller("NuevaLineaColectivoController", ["$scope", "$controller", "LineaColectivoService", "utilsService", function ($scope, $controller, LineaColectivoService, utilsService) {
        $controller('LineaColectivoBaseController', {$scope: $scope});

        $scope.saveLineaColectivo = function (option) {
            if ($("#formLineaColectivo").valid()) {
                $scope.colectivo.empresa = $scope.getEmpresas();
                $scope.startMain();
                LineaColectivoService.save($scope.colectivo)
                        .then(function (response) {
                            if (response.code == 200) {
                                var dialog = utilsService.openDialogo(
                                        "Se agregó correctamente la linea de colectivo",
                                        {
                                            mensaje: "Se agregó correctamente la linea de colectivo " + response.data.nombre
                                        },
                                null,
                                        baseUrlTemplates + 'templates/dialogoOk.html',
                                        'CloseCtrl',
                                        'lg'
                                        );
                                dialog.result.then(
                                        function (response) {
                                            if (option == 1) {
                                                window.location.href = baseUrlByRol + "colectivos";
                                            }
                                            if (option == 2) {
                                                $scope.reset();
                                            }
                                        });
                            }

                        }, function (response) {
                            utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                        }).finally(function () {
                    $scope.stopMain();
                });
            }
        };

    }]);

app.controller("EditarLineaColectivoController", ["$scope", "$controller", "LineaColectivoService", "utilsService", function ($scope, $controller, LineaColectivoService, utilsService) {
        $controller('LineaColectivoBaseController', {$scope: $scope});

        $scope.$on('finishInitLineaColectivo', function (event) {
            $scope.getLineaColectivo($scope.data);
        });

        $scope.updateLineaColectivo = function (option) {
            if ($("#formLineaColectivo").valid()) {
                $scope.colectivo.empresa = $scope.getEmpresas();                
                $scope.startMain();
                LineaColectivoService.update($scope.colectivo.id, $scope.colectivo)
                        .then(function (response) {
                            if (response.code == 200) {
                                var dialog = utilsService.openDialogo(
                                        "Se actualizó correctamente la linea de colectivo",
                                        {
                                            mensaje: "Se actualizó correctamente la linea de colectivo " + response.data.nombre
                                        },
                                null,
                                        baseUrlTemplates + 'templates/dialogoOk.html',
                                        'CloseCtrl',
                                        'lg'
                                        );
                                dialog.result.then(
                                        function (response) {
                                            if (option == 1) {
                                                window.location.href = baseUrlByRol + "colectivos";
                                            }
                                            if (option == 2) {
                                                window.location.href = baseUrlByRol + "colectivos/nuevo";
                                            }
                                        });
                            }

                        }, function (response) {
                            utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                        }).finally(function () {
                    $scope.stopMain();
                });
            }
        };

    }]);

