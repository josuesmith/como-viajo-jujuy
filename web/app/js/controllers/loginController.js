app.controller("loginController", ["$scope", "$timeout", function ($scope, $timeout) {
    $scope.init = function () {
        $("#formLogin").validate({
            ignore: [],
        });
        $timeout(function () {
            $("#username").focus();
        });
    }
    $scope.login = function () {
        if ($("#formLogin").valid()) {
            $scope.startMain();
            $("#formLogin").submit();
        }
    }
    $scope.logout = function () {
        $scope.startMain();
        $("#formLogout").submit();
    }

}]);