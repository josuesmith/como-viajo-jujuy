app.controller("EmpresaColectivoListadoController", ["$scope", "EmpresaColectivoService", "$controller", "utilsService",
    function ($scope, EmpresaColectivoService, $controller, utilsService) {

        $controller('baseViewCtrl', {$scope: $scope});
        $controller('mainController', {$scope: $scope});

        $scope.paramPagination.page = 1;
        $scope.paramPagination.sortkey = "nombre";
        $scope.paramPagination.direction = "ASC";

        $scope.filterCriteria = {
            nombre: null
        };

        $scope.fetchResult = function () {
            $scope.startMain();
            var criteria = $scope.filterCriteria;
            EmpresaColectivoService.search(criteria, $scope.paramPagination)
                    .then(function (response) {
                        if (response.code === 200) {
                            $scope.empresas = response.data.listado;
                            $scope.setValuePagination(response.data);
                        } else {
                            utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                        }
                    }, function (response) {
                    }).finally(function () {
                $scope.stopMain();
            });
        };

        $scope.clear = function () {
            $scope.filterCriteria.nombre = null;
            $scope.fetchResult();
        };

        function iniciar() {
            $scope.fetchResult();
        }
        ;

        /*************************************************/
        $scope.init = function (title) {
            $scope.title = title;
            iniciar();
            $("#jsForm").validate({
                ignore: []
            });
        };

    }]);

app.controller("BaseEmpresaColectivoController", ["$scope", "EmpresaColectivoService", "$controller", "utilsService", "$timeout",
    function ($scope, EmpresaColectivoService, $controller, utilsService, $timeout) {

        $controller('baseViewCtrl', {$scope: $scope});
        $controller('mainController', {$scope: $scope});

        $scope.initEmpresa = function (data) {
            $scope.data = data;

            //Validación del formulario
            $("#formEmpresa").validate({
                ignore: [],
                messages: {
                    nombre: {
                        required: "Debe completar este campo."
                    },
                    descripcion: {
                        required: "Debe completar este campo."
                    }
                },
                rules: {
                    nombre: {
                        required: true
                    },
                    descripcion: {
                        required: true
                    }
                }
            });

            $timeout(function () {
                $scope.reset();
                $scope.$broadcast('finishInitEmpresaColectivo');
            });
        };

        $scope.reset = function () {
            $scope.empresa = {
                nombre: null,
                descripcion: null
            };
        };

        $scope.setEmpresaColectivo = function (empresa) {
            $scope.empresa.id = empresa.id;
            $scope.empresa.nombre = empresa.nombre;
            $scope.empresa.descripcion = empresa.descripcion;
        };

        $scope.getEmpresaColectivo = function (id) {
            $scope.startMain();
            EmpresaColectivoService.getEmpresaColectivoById(id).then(function (response) {
                $scope.setEmpresaColectivo(response.data);
            }, function (response) {
                utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
            }).finally(function () {
                $scope.stopMain();
            });
        };

    }]);

app.controller("NuevaEmpresaColectivoController", ["$scope", "EmpresaColectivoService", "$controller", "utilsService",
    function ($scope, EmpresaColectivoService, $controller, utilsService) {

        $controller('BaseEmpresaColectivoController', {$scope: $scope});

        $scope.saveEmpresa = function (option) {
            if ($("#formEmpresa").valid()) {
                $scope.startMain();
                EmpresaColectivoService.save($scope.empresa)
                        .then(function (response) {
                            if (response.code == 200) {
                                var dialog = utilsService.openDialogo(
                                        "Se agregó correctamente la empresa de colectivo",
                                        {
                                            mensaje: "Se agregó correctamente la empresa de colectivo " + response.data.nombre
                                        },
                                null,
                                        baseUrlTemplates + 'templates/dialogoOk.html',
                                        'CloseCtrl',
                                        'lg'
                                        );
                                dialog.result.then(
                                        function (response) {
                                            if (option == 1) {
                                                window.location.href = baseUrlByRol + "empresas";
                                            }
                                            if (option == 2) {
                                                $scope.reset();
                                            }
                                        });
                            }

                        }, function (response) {
                            utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                        }).finally(function () {
                    $scope.stopMain();
                });
            }
        };


    }]);

app.controller("EditarEmpresaColectivoController", ["$scope", "EmpresaColectivoService", "$controller", "utilsService",
    function ($scope, EmpresaColectivoService, $controller, utilsService) {

        $controller('BaseEmpresaColectivoController', {$scope: $scope});
        
        $scope.$on('finishInitEmpresaColectivo', function (event) {
            $scope.getEmpresaColectivo($scope.data);            
        });

        $scope.updateEmpresa = function (option) {
            if ($("#formEmpresa").valid()) {
                $scope.startMain();
                EmpresaColectivoService.update($scope.empresa.id, $scope.empresa)
                        .then(function (response) {
                            if (response.code == 200) {
                                var dialog = utilsService.openDialogo(
                                        "Se actualizó correctamente la empresa de colectivo",
                                        {
                                            mensaje: "Se actualizó correctamente la empresa de colectivo " + response.data.nombre
                                        },
                                null,
                                        baseUrlTemplates + 'templates/dialogoOk.html',
                                        'CloseCtrl',
                                        'lg'
                                        );
                                dialog.result.then(
                                        function (response) {
                                            if (option == 1) {
                                                window.location.href = baseUrlByRol + "empresas";
                                            }
                                            if (option == 2) {
                                                window.location.href = baseUrlByRol + "empresas/nueva";
                                            }
                                        });
                            }

                        }, function (response) {
                            utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                        }).finally(function () {
                    $scope.stopMain();
                });
            }
        };


    }]);


