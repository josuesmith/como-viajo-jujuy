app.controller("BaseEditarMisDatosClaveController", ["$scope", "EmpresaService", "baseUrl", "$controller",
	function ($scope, EmpresaService, baseUrl, $controller) {
		$controller('baseViewCtrl', {$scope: $scope});
		$controller('mainController', {$scope: $scope});

		$scope.clear = function () {
			$scope.usuario.password = null;
			$scope.usuario.password1 = null;
		};


		$scope.initCambioClave = function (data) {

			$scope.usuario = {
				password: null,
				password1: null
			};

			$("#formCambioClave").validate({
				ignore: [],
				message: {
					password: {
						required: "Debe completar este campo",
						minlength: "La contraseña debe tener 6 o más caracteres.",
						maxlength: "La contraseña debe tener 8 o menos caracteres."
					},
					password1: {
						required: "Debe completar este campo.",
						equalTo: "No coinciden las contraseñas."
					}
				},
				rules: {
					password: {
						required: true,
						minlength: 6,
						maxlength: 8
					},
					password1: {
						required: true,
						equalTo: "#password"
					}
				}
			});
		};
	}]);

app.controller("AdminEditarMisDatosClaveController", ["$scope", "EmpresaService", "$controller", "utilsService", "baseUrl",
	function ($scope, EmpresaService, $controller, utilsService, baseUrl) {

		$controller('BaseEditarMisDatosClaveController', {$scope: $scope});

		$scope.updateClave = function (idUsuario) {
			if ($("#formCambioClave").valid()) {
				$scope.startMain();
				$scope.usuario.id = idUsuario;
				EmpresaService.updateClaveAdmin($scope.usuario).then(function (response) {
					if (response.code == 200) {
						var dialog = utilsService.openDialogo(
								"Se actualizó la contraseña exitosamente.",
								{mensaje: "Se actualizó la contraseña del usuario " + response.data.usuario + ". Debe ingresar al sistema con la nueva contraseña."},
								null,
								baseUrlTemplates + 'templates/dialogoOk.html',
								'CloseCtrl',
								'lg'
								);
						dialog.result.then(function () {
							window.location.href = baseUrl + "logout";
						});
					} else {
						utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
					}
				}, function (response) {
				}).finally(function () {
					$scope.stopMain();
				});
			}

		};

	}]);  