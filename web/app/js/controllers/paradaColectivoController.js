app.controller("ParadaColectivoListadoController", ["$scope", "ParadaColectivoService", "$controller", "utilsService",
    function ($scope, ParadaColectivoService, $controller, utilsService) {

        $controller('baseViewCtrl', {$scope: $scope});
        $controller('mainController', {$scope: $scope});

        $scope.paramPagination.page = 1;
        $scope.paramPagination.sortkey = "nombre";
        $scope.paramPagination.direction = "ASC";

        $scope.filterCriteria = {
            nombre: null
        };

        $scope.fetchResult = function () {
            $scope.startMain();
            var criteria = $scope.filterCriteria;
            ParadaColectivoService.search(criteria, $scope.paramPagination)
                    .then(function (response) {
                        if (response.code === 200) {
                            $scope.paradas = response.data.listado;
                            $scope.setValuePagination(response.data);
                        } else {
                            utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                        }
                    }, function (response) {
                    }).finally(function () {
                $scope.stopMain();
            });
        };

        $scope.clear = function () {
            $scope.filterCriteria.nombre = null;
            $scope.fetchResult();
        };

        function iniciar() {
            $scope.fetchResult();
        }
        ;

        /*************************************************/
        $scope.init = function (title) {
            $scope.title = title;
            iniciar();
            $("#jsForm").validate({
                ignore: []
            });
        };

    }]);

app.controller("BaseParadaColectivoController", ["$scope", "ParadaColectivoService", "$controller", "utilsService", "$timeout",
    function ($scope, ParadaColectivoService, $controller, utilsService, $timeout) {

        $controller('baseViewCtrl', {$scope: $scope});
        $controller('mainController', {$scope: $scope});

        $scope.languaje = {
            inputTooShort: function () {
                return "Ingrese mas de 1 caracter...";
            },
            searching: function () {
                return "Buscando…";
            },
            noResults: function () {
                return "No se encontraron resultados";
            }
        };

        $scope.initParada = function (data) {
            $scope.data = data;
            $scope.aLineas = lineas;

            //Validación del formulario
            $("#formParadaColectivo").validate({
                ignore: [],
                messages: {
                    nombre: {
                        required: "Debe completar este campo."
                    },
                    linea: {
                        required: "Debe completar este campo."
                    },
                    latitud: {
                        required: "Debe completar este campo.",
                        number: "Debe ingresar números unicamente."
                    },
                    longitud: {
                        required: "Debe completar este campo.",
                        number: "Debe ingresar números unicamente."
                    },
                    sentido: {
                        required: "Debe completar este campo."
                    }
                },
                rules: {
                    nombre: {
                        required: true
                    },
                    linea: {
                        required: true
                    },
                    latitud: {
                        required: true,
                        number: true
                    },
                    longitud: {
                        required: true,
                        number: true
                    },
                    sentido: {
                        required: true
                    }
                }
            });

            $timeout(function () {
                $scope.reset();
                $scope.loadSearchLineas("#lineas", null);
                $("#lineas").on("select2:select", function () {
                    $("#lineas").valid();
                });
                jQuery.validator.addMethod("validateLinea", function (value, element) {
                    var tempLin = $("#lineas").select2('data');
                    return tempLin != null && tempLin.length > 0;
                }, "Debe seleccionar una linea de colectivo");
                $scope.$broadcast('finishInitParadaColectivo');
            });
        };

        $scope.reset = function () {
            $scope.parada = {
                nombre: null,
                latitud: null,
                longitud: null
            };
            $('#lineas').val(null).trigger('change');           
        };

        $scope.setParadaColectivo = function (parada) {
            $scope.parada.id = parada.id;
            $scope.parada.nombre = parada.nombre;
            $scope.parada.latitud = parada.latitud;
            $scope.parada.longitud = parada.longitud;
            $scope.parada.sentido = parada.sentido;
            
             var tempSelectionLin = [];
            tempSelectionLin.push({id: parada.linea.id, nombre:  parada.linea.nombre, empresa: parada.linea.empresa, selected: true});
            $('#lineas').find('option').remove();
            $("#lineas").select2('destroy');
            $timeout(function () {
                $scope.loadSearchLineas("#lineas", tempSelectionLin);
            });            
        };

        $scope.getParadaColectivo = function (id) {
            $scope.startMain();
            ParadaColectivoService.getParadaColectivoById(id).then(function (response) {
                $scope.setParadaColectivo(response.data);
            }, function (response) {
                utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
            }).finally(function () {
                $scope.stopMain();
            });
        };
        
        $scope.loadSearchLineas = function (elem, data) {
            $(elem).select2({
                width: '100%',
                theme: "bootstrap4",
                data: data,
                ajax: {
                    url: baseUrlByRol + "ajax/lineas/search",
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            q: params.term
                        }
                        return query;
                    },
                    processResults: function (data) {
                        angular.forEach(data.data, function (item) {
                            item.id = item.id;
                        });
                        return {
                            results: data.data
                        };
                    },
                    cache: true
                },
                placeholder: 'Busque la linea de colectivo',
                minimumInputLength: 1,
                language: $scope.languaje,
                templateResult: function (data) {                    
                    if (data.loading) {
                        return data.text;
                    }
                    try {                        
                        return data.nombre + " -  " + data.empresa.nombre ;
                    } catch (e) {
                        console.log(e);
                    }
                },
                templateSelection: function (data) {
                    if (data.id == "") {
                        return data.text;
                    } else {                        
                        return data.nombre + " - " + data.empresa.nombre ;
                    }
                },
                escapeMarkup: function (m) {
                    return m;
                }
            });
        };
        
        $scope.getLineasColectivos = function () {
            var items = $("#lineas").select2('data');
            return items[0].id;
        };


    }]);

app.controller("NuevaParadaColectivoController", ["$scope", "ParadaColectivoService", "$controller", "utilsService",
    function ($scope, ParadaColectivoService, $controller, utilsService) {

        $controller('BaseParadaColectivoController', {$scope: $scope});

        $scope.saveParadaColectivo = function (option) {
            if ($("#formParadaColectivo").valid()) {
                $scope.parada.linea = $scope.getLineasColectivos();
                $scope.startMain();
                ParadaColectivoService.save($scope.parada)
                        .then(function (response) {
                            if (response.code == 200) {
                                var dialog = utilsService.openDialogo(
                                        "Se agregó correctamente la parada de colectivo",
                                        {
                                            mensaje: "Se agregó correctamente la parada de colectivo " + response.data.nombre
                                        },
                                null,
                                        baseUrlTemplates + 'templates/dialogoOk.html',
                                        'CloseCtrl',
                                        'lg'
                                        );
                                dialog.result.then(
                                        function (response) {
                                            if (option == 1) {
                                                window.location.href = baseUrlByRol + "paradas";
                                            }
                                            if (option == 2) {
                                                $scope.reset();
                                            }
                                        });
                            }

                        }, function (response) {
                            utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                        }).finally(function () {
                    $scope.stopMain();
                });
            }
        };


    }]);

app.controller("EditarParadaColectivoController", ["$scope", "ParadaColectivoService", "$controller", "utilsService",
    function ($scope, ParadaColectivoService, $controller, utilsService) {

        $controller('BaseParadaColectivoController', {$scope: $scope});
        $scope.$on('finishInitParadaColectivo', function (event) {
            $scope.getParadaColectivo($scope.data);
        });

        $scope.updateParadaColectivo = function (option) {
            if ($("#formParadaColectivo").valid()) {
                $scope.parada.linea = $scope.getLineasColectivos();
                $scope.startMain();
                ParadaColectivoService.update($scope.parada.id, $scope.parada)
                        .then(function (response) {
                            if (response.code == 200) {
                                var dialog = utilsService.openDialogo(
                                        "Se actualizó correctamente la parada de colectivo",
                                        {
                                            mensaje: "Se actualizó correctamente la parada de colectivo " + response.data.nombre
                                        },
                                null,
                                        baseUrlTemplates + 'templates/dialogoOk.html',
                                        'CloseCtrl',
                                        'lg'
                                        );
                                dialog.result.then(
                                        function (response) {
                                            if (option == 1) {
                                                window.location.href = baseUrlByRol + "paradas";
                                            }
                                            if (option == 2) {
                                               window.location.href = baseUrlByRol + "paradas/nueva";
                                            }
                                        });
                            }

                        }, function (response) {
                            utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                        }).finally(function () {
                    $scope.stopMain();
                });
            }
        };


    }]);