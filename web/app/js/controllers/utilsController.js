app.controller('baseViewCtrl', ['$scope', function ($scope) {
    $scope.status = {
        isCustomHeaderOpen: true,
        isFirstOpen: true,
        isFirstDisabled: false
    };
    $scope.searchText = "";
    //inicializo las variables para utilizar la función SORT
    $scope.paramPagination = {
        q: "",
        reverse: false, //if true make it false and vice versa
        sortkey: "",
        direction: "ASC",
        page: 1, //por defecto en 1
        itemsPerPage: 0,
        totalItems: 0,
        maxSize: 5,
        bigTotalItems: 175,
        bigCurrentPage: 1,
        filtroNombre: ""
    }
    //funcion que ordena por Nombre, o por Apellido o por Usuario
    $scope.sort = function (keyname, callBackFunction) {
        $scope.paramPagination.reverse = !$scope.paramPagination.reverse; //if true make it false and vice versa
        $scope.paramPagination.sortkey = keyname;   //set the sortKey to the param passed
        if ($scope.paramPagination.reverse) {
            $scope.paramPagination.direction = "DESC";
        } else {
            $scope.paramPagination.direction = "ASC";
        }
        if (angular.isFunction($scope[callBackFunction])) {
            $scope[callBackFunction]();
        }
    };
    $scope.pageChanged = function (callBackFunction) {
        if (angular.isFunction($scope[callBackFunction])) {
            $scope[callBackFunction]();
        }
    };
    $scope.setValuePagination = function (datos) {
        $scope.paramPagination.itemsPerPage = datos.page_size;
        $scope.paramPagination.totalItems = datos.total_results;
        $scope.paramPagination.page = datos.current_page;
        $scope.paramPagination.maxSize = 5;
        $scope.paramPagination.bigTotalItems = 175;
        $scope.paramPagination.bigCurrentPage = 1;
        $scope.paramPagination.filtroNombre = "";
        // $scope.gotToScroll("#contenidoPrincipal");// para que vaya siempre al inicio  y no se quede al final cuando haya scroll
    }

}]);


app.controller('ConfirmacionCtrl', ['$scope', 'datos', 'titulo', '$uibModalInstance', '$window',  function ($scope, datos, titulo, $uibModalInstance, $window) {

    $scope.datos = datos;
    $scope.titulo = titulo;

    $scope.cerrar = function () {
        $uibModalInstance.dismiss('Cancelado');
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Cancelado');
    };
    $scope.confirm = function () {
        $uibModalInstance.close('Ok');
    };


}]);

app.controller('CloseCtrl', ['$scope', 'datos', 'titulo', '$uibModalInstance', '$window',  function ($scope, datos, titulo, $uibModalInstance, $window) {

    $scope.datos = datos;
    $scope.titulo = titulo;

    $scope.confirm = function () {
        $uibModalInstance.close();
    };
}]);

app.controller('DialogoCtrl', ['$scope', 'datos', 'titulo', 'utilsService', '$uibModalInstance', function ($scope, datos, titulo, utilsService, $uibModalInstance) {
    $scope.datos = datos;
    $scope.titulo = titulo;

    $scope.cerrar = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Cancel');
    };

    $scope.confirm = function () {
        $uibModalInstance.close('Ok');
    };

    $scope.inicio = function () {
    };

}]);

app.controller('DialogoCreditoCtrl', ['$scope', 'datos', 'titulo', 'utilsService', '$uibModalInstance', 'CreditoService', function ($scope, datos, titulo, utilsService, $uibModalInstance, CreditoService) {
    $scope.datos = datos;
    $scope.titulo = titulo;
    
    $scope.cerrar = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Cancel');
    };

    $scope.confirm = function () {
        $uibModalInstance.close('Ok');
    };

    $scope.inicio = function () {
    };
    
    $scope.descargarInscriptoEstado = function(){
        var data = {
            aulaId: $scope.datos.aulaId
        }
        CreditoService.descargarCreditossEstado(data);
    }
}]);