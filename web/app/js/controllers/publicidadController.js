app.controller("PublicidadListadoController", ["$scope", "PublicidadService", "$controller", "utilsService",
    function ($scope, PublicidadService, $controller, utilsService) {

        $controller('baseViewCtrl', {$scope: $scope});
        $controller('mainController', {$scope: $scope});

        $scope.paramPagination.page = 1;
        $scope.paramPagination.sortkey = "tituloPublicidad";
        $scope.paramPagination.direction = "ASC";

        $scope.filterCriteria = {
            tituloPublicidad: null
        };

        $scope.fetchResult = function () {
            $scope.startMain();
            var criteria = $scope.filterCriteria;
            PublicidadService.search(criteria, $scope.paramPagination)
                    .then(function (response) {
                        if (response.code === 200) {
                            $scope.publicidades = response.data.listado;
                            $scope.setValuePagination(response.data);
                        } else {
                            utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                        }
                    }, function (response) {
                    }).finally(function () {
                $scope.stopMain();
            });
        };

        $scope.clear = function () {
            $scope.filterCriteria.tituloPublicidad = null;
            $scope.fetchResult();
        };

        function iniciar() {
            $scope.fetchResult();
        }
        ;

        /*************************************************/
        $scope.init = function (title) {
            $scope.title = title;
            iniciar();
            $("#jsForm").validate({
                ignore: []
            });
        };

    }]);

app.controller("BasePublicidadController", ["$scope", "PublicidadService", "$controller", "utilsService", "$timeout",
    function ($scope, PublicidadService, $controller, utilsService, $timeout) {
        $controller('baseViewCtrl', {$scope: $scope});
        $controller('mainController', {$scope: $scope});

        //codigo que visualiza las imagenes
        function readURLImagen(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#imagen').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        //codigo que visualiza los videos
        function readURLVideo(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.video').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        //codigo que visualiza los audios
        function readURLAudio(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#audio').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        //codigo que obtiene la extension del archivo
        function getExtencionArchivo(file) {
            var nombre = file.name;
            var ext = nombre.split(".")[1];
            return ext;
        }

        //codigo para que se actualice el nombre del archivo en el input
        $('#archivo').on('input change', function (e) {
            var archivo = $('#archivo')[0].files[0];
            if (archivo === undefined) {
                $('label.custom-file-label').html('Seleccionar Archivo...');
            } else {
                $('label.custom-file-label').html(archivo.name);
                var ext = getExtencionArchivo(archivo);
                if (ext == 'jpg' || ext == 'png' || ext == 'jpeg') {
                    $scope.video = false;
                    $scope.imagen = true;
                    $scope.audio = false;
                    readURLImagen(this);
                } else if (ext == 'mp4' || ext == 'avi') {
                    $scope.video = true;
                    $scope.imagen = false;
                    $scope.audio = false;
                    readURLVideo(this);
                } else if (ext == 'mp3' || ext == 'wav') {
                    $scope.video = false;
                    $scope.imagen = false;
                    $scope.audio = true;
                    readURLAudio(this);
                }
            }
            $scope.$apply();
        });

        $scope.initPublicidad = function (data) {
            $scope.data = data;
            $scope.imagen = false;
            $scope.video = false;
            $scope.audio = false;

            $.validator.addMethod('filesize', function (value, element, param) {
                return this.optional(element) || (element.files[0].size <= param);
            });

            //Validación del formulario
            $("#upload_file_form").validate({
                ignore: [],
                messages: {
                    titulo: {
                        required: "Debe completar este campo."
                    },
                    descripcion: {
                        required: "Debe completar este campo."
                    },
                    archivo: {
                        filesize: "El archivo no debe superar los 4 MB."
                    },
                    tipo: {
                        required: "Debe completar este campo."
                    },
                    estado: {
                        required: "Debe completar este campo."
                    },
                    duracion: {
                        required: "Debe completar este campo.",
                        min: "Debe ingresar una duración mayor a 0."
                    },
                    url: {
                     url: "Debe ingresar una URL válida."   
                    }
                },
                rules: {
                    nombre: {
                        required: true
                    },
                    descripcion: {
                        required: true
                    },
                    archivo: {
                        filesize: 4000000
                    },
                    tipo: {
                        required: true
                    },
                    estado: {
                        required: true
                    },
                    duracion: {
                        required: true,
                        min: 1
                    },
                    url: {
                        url: true
                    }
                }
            });
        };

        $scope.reset = function () {
            $scope.publicidad = {
                titulo: null,
                descripcion: null,
                duracion: 0,
                url: null
            };
            $scope.estado = {
                seleccionado: null
            };
            $scope.tipo = {
                seleccionado: null
            };
            $("#archivo").val('');
            $('label.custom-file-label').html('Seleccionar Archivo...');
            $scope.imagen = false;
            $scope.video = false;
            $scope.audio = false;
        };

        $scope.aEstados = [{"id": 0, "nombre": "Inactivo"}, {"id": 1, "nombre": "Activo"}];
        $scope.aTipos = [{"id": 0, "nombre": "Pantalla pequeña"}, {"id": 1, "nombre": "Pantalla grande"}];

        $timeout(function () {
            $scope.reset();
            $scope.$broadcast('finishInitPublicidad');
        });

        $scope.reloadArchivo = function (publicidad) {
            var urlFile = baseUrl.replace("app_dev.php/", "");
            urlFile = urlFile + "uploads/multimedia/" + publicidad.nombre_archivo;
            if (publicidad.tipo_archivo == 'image/jpg' || publicidad.tipo_archivo == 'image/png' || publicidad.tipo_archivo == 'image/jpeg') 
            {
                $scope.imagen = true;
                $(function () {
                   $("#imagen").attr("src", urlFile);
                });
                
            } 
            if (publicidad.tipo_archivo == 'video/mp4' || publicidad.tipo_archivo == 'video/avi')
            {
                $scope.video = true;
                $(function () {
                   $(".video").attr("src", urlFile);
                });
                
            } 
            if (publicidad.tipo_archivo == 'audio/mp3' || publicidad.tipo_archivo == 'audio/wav')
            {
                $scope.audio = true;
                 $(function () {
                   $("#audio").attr("src", urlFile);
                });
            }
        };

        $scope.setPublicidad = function (publicidad) {
            $scope.publicidad.id = publicidad.id;
            $scope.publicidad.titulo = publicidad.titulo_publicidad;
            $scope.publicidad.descripcion = publicidad.descripcion_publicidad;
            $scope.publicidad.duracion = publicidad.duracion_publicidad;
            $scope.publicidad.url = publicidad.url_publicidad;
            $scope.tipo.seleccionado = (publicidad.tipo_publicidad) ? 1 : 0;
            $scope.estado.seleccionado = (publicidad.estado) ? 1 : 0;
            if (publicidad.nombre_archivo != null) {
                $('label.custom-file-label').html(publicidad.nombre_archivo);
                $scope.reloadArchivo(publicidad);
            }
        };

        $scope.getPublicidad = function (id) {
            $scope.startMain();
            PublicidadService.getPublicidadById(id).then(function (response) {
                $scope.setPublicidad(response.data);
            }, function (response) {
                utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
            }).finally(function () {
                $scope.stopMain();
            });
        };

    }]);

app.controller("NuevaPublicidadController", ["$scope", "PublicidadService", "$controller", "utilsService",
    function ($scope, PublicidadService, $controller, utilsService) {

        $controller('BasePublicidadController', {$scope: $scope});

        $scope.savePublicidad = function (option) {
            if ($("#upload_file_form").valid()) {
                $scope.startMain();
                $scope.publicidad.estado = $scope.estado.seleccionado;
                $scope.publicidad.tipo = $scope.tipo.seleccionado;

                var form = $('#upload_file_form')[0];
                var formData = new FormData(form);

                if ($('#archivo')[0].files !== undefined && $('#archivo')[0].files !== null) {
                    var archivo = $('#archivo')[0].files[0];
                    formData.append('archivo', archivo);
                }

                PublicidadService.save(formData)
                        .then(function (response) {
                            if (response.code == 200) {
                                var dialog = utilsService.openDialogo(
                                        "Se agregó correctamente la publicidad",
                                        {
                                            mensaje: "Se agregó correctamente la publicidad " + response.data.titulo_publicidad
                                        },
                                null,
                                        baseUrlTemplates + 'templates/dialogoOk.html',
                                        'CloseCtrl',
                                        'lg'
                                        );
                                dialog.result.then(
                                        function (response) {
                                            if (option == 1) {
                                                window.location.href = baseUrlByRol + "publicidades";
                                            }
                                            if (option == 2) {
                                                $scope.reset();
                                            }
                                        });
                            } else {
                                utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                                $scope.reset();
                            }

                        }, function (response) {
                            utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                        }).finally(function () {
                    $scope.stopMain();
                });
            }
        };

    }]);

app.controller("EditarPublicidadController", ["$scope", "PublicidadService", "$controller", "utilsService",
    function ($scope, PublicidadService, $controller, utilsService) {

        $controller('BasePublicidadController', {$scope: $scope});

        $scope.$on('finishInitPublicidad', function (event) {
            $scope.getPublicidad($scope.data);
        });

        $scope.updatePublicidad = function (option) {
            if ($("#upload_file_form").valid()) {
                $scope.startMain();
                $scope.publicidad.estado = $scope.estado.seleccionado;
                $scope.publicidad.tipo = $scope.tipo.seleccionado;

                var form = $('#upload_file_form')[0];
                var formData = new FormData(form);

                if ($('#archivo')[0].files !== undefined && $('#archivo')[0].files !== null) {
                    var archivo = $('#archivo')[0].files[0];
                    formData.append('archivo', archivo);
                }                

                PublicidadService.update($scope.publicidad.id, formData)
                        .then(function (response) {
                            if (response.code == 200) {
                                var dialog = utilsService.openDialogo(
                                        "Se actualizó correctamente la publicidad",
                                        {
                                            mensaje: "Se actualizó correctamente la publicidad " + response.data.titulo_publicidad
                                        },
                                null,
                                        baseUrlTemplates + 'templates/dialogoOk.html',
                                        'CloseCtrl',
                                        'lg'
                                        );
                                dialog.result.then(
                                        function (response) {
                                            if (option == 1) {
                                                window.location.href = baseUrlByRol + "publicidades";
                                            }
                                            if (option == 2) {
                                                window.location.href = baseUrlByRol + "publicidades/nueva";
                                            }
                                        });
                            } else {
                                utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                                $scope.reset();
                            }

                        }, function (response) {
                            utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                        }).finally(function () {
                    $scope.stopMain();
                });
            }
        };

    }]);