app.controller("mainController", ["$scope", "$timeout", "utilsService", function ($scope, $timeout, utilsService) {
    $scope.startMain = function () {
        $(".page-content").addClass("working");
    };
    $scope.startMainDetalle = function () {
        $(".tablaDetalle").addClass("working");
    };
    $scope.mainData = {
    }
    $scope.stopMain = function () {
        $(".page-content").removeClass("working");
    };
     $scope.stopMainDetalle = function () {
        $(".tablaDetalle").removeClass("working");
    };
    // funciones globales para jquery validate
    jQuery.validator.setDefaults({
        onfocusout: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        }
    });
}]);