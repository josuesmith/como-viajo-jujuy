app.controller("UsuarioListadoController", ["$scope", "UsuarioService", "$controller", "utilsService", function ($scope, UsuarioService, $controller, utilsService) {

        $controller('baseViewCtrl', {$scope: $scope});
        $controller('mainController', {$scope: $scope});


        $scope.paramPagination.page = 1;
        $scope.paramPagination.sortkey = "usuario";
        $scope.paramPagination.direction = "ASC";

        $scope.filterCriteria = {
            usuario: null
        };

        $scope.fetchResult = function () {
            $scope.startMain();
            var criteria = $scope.filterCriteria;
            UsuarioService.search(criteria, $scope.paramPagination)
                    .then(function (response) {
                        if (response.code === 200) {
                            $scope.usuarios = response.data.listado;
                            $scope.setValuePagination(response.data);
                        } else {
                            utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                        }
                    }, function (response) {
                    }).finally(function () {
                $scope.stopMain();
            });
        };

        $scope.refreshGrid = function () {
            $scope.fetchResult();
        };

        $scope.clear = function () {
            $scope.filterCriteria.usuario = null;
            $scope.fetchResult();
        };

        function iniciar() {
            $scope.fetchResult();
        };

        /*************************************************/
        $scope.init = function (title, data) {
            $scope.title = title;
            iniciar();
            $("#jsForm").validate({
                ignore: []
            });
        };

    }]);

app.controller("BaseUsuarioController", ["$scope", "UsuarioService", "$controller", "utilsService", "$timeout",
    function ($scope, UsuarioService, $controller, utilsService, $timeout) {
        $controller('baseViewCtrl', {$scope: $scope});
        $controller('mainController', {$scope: $scope});

        $scope.initUsuario = function (data, roles) {
            $("#errorDni").hide();
            $scope.disableInputDni = false;
            $scope.existeCuil = true;
            $scope.busquedaCuil = true;
            $scope.existeAsociacion = true;
            $scope.data = data;
            $scope.aRoles = roles;            

            $.validator.addMethod("validDate", function (value, element) {
                return this.optional(element) || /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/.test(value);
            }, "Ingrese una fecha con el formato válido dd-mm-aaaa");            

            $("#formUsuario").validate({
                ignore: [],
                messages: {
                    dni: {
                        maxlength: "Debe contener 8 o menos caracteres.",
                        minlength: "Debe contener 7 o más caracteres",
                        digits: "Debe contener solo números enteros."
                    },
                    cuil: {
                        maxlength: "Debe contener 11 o menos caracteres.",
                        minlength: "Debe contener 10 o más caracteres",
                        digits: "Debe contener solo numeros enteros."
                    },
                    email: {
                        required: "Debe completar este campo.",
                        email: "Debe ingresar una dirección de email valida."
                    },
                    fecha_nacimiento: {
                        validDate: "Ingrese una fecha con el formato válido dd-mm-aaaa"
                    },                    
                    username: {
                        required: "Debe completar este campo."
                    },
                    password: {
                        required: "Debe completar este campo.",
                        minlength: "La contraseña debe tener 6 o más caracteres.",
                        maxlength: "La contraseña debe tener 8 o menos caracteres."
                    },
                    password1: {
                        equalTo: "No coinciden las contraseñas."
                    },                   
                    rol: {
                        required: "Debe completar este campo."
                    }
                },
                rules: {
                    dni: {
                        maxlength: 8,
                        minlength: 7,
                        digits: true
                    },
                    cuil: {
                        maxlength: 11,
                        minlength: 10,
                        digits: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    fecha_nacimiento: {
                        validDate: true
                    },
                    empresa: {
                        required: true
                    },
                    username: {
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 6,
                        maxlength: 8
                    },
                    password1: {
                        equalTo: "#password"
                    },
                    rol: {
                        required: true
                    }
                }
            });
            
            $("#formUsuarioModificacion").validate({
                ignore: [],
                messages: {
                    dni: {
                        maxlength: "Debe contener 8 o menos caracteres.",
                        minlength: "Debe contener 7 o más caracteres",
                        digits: "Debe contener solo números enteros."
                    },
                    cuil: {
                        maxlength: "Debe contener 11 o menos caracteres.",
                        minlength: "Debe contener 10 o más caracteres",
                        digits: "Debe contener solo numeros enteros."
                    },
                    email: {
                        required: "Debe completar este campo.",
                        email: "Debe ingresar una dirección de email valida."
                    },
                    fecha_nacimiento: {
                        validDate: "Ingrese una fecha con el formato válido dd-mm-aaaa"
                    },                    
                    username: {
                        required: "Debe completar este campo."
                    },
                    password: {
                        minlength: "La contraseña debe tener 6 o más caracteres.",
                        maxlength: "La contraseña debe tener 8 o menos caracteres."
                    },
                    password1: {
                        equalTo: "No coinciden las contraseñas."
                    },                   
                    rol: {
                        required: "Debe completar este campo."
                    }
                },
                rules: {
                    dni: {
                        maxlength: 8,
                        minlength: 7,
                        digits: true
                    },
                    cuil: {
                        maxlength: 11,
                        minlength: 10,
                        digits: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    fecha_nacimiento: {
                        validDate: true
                    },
                    empresa: {
                        required: true
                    },
                    username: {
                        required: true
                    },
                    password: {
                        minlength: 6,
                        maxlength: 8
                    },
                    password1: {
                        equalTo: "#password"
                    },
                    rol: {
                        required: true
                    }
                }
            });

            $timeout(function () {
                $scope.reset();
                $scope.$broadcast('finishInitUsuario');
            });
            isNumberKey = function (evt) {
                if (evt != null) {
                    var charCode = (evt.which) ? evt.which : event.keyCode
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                        return false;
                    return true;
                }
            };

            $scope.aSexos = [{id: "M", nombre: "Masculino"}, {id: "F", nombre: "Femenino"}];
        };

        function addZero(n) {
            if (n < 10) {
                n = "0" + n;
            }
            return n;
        }
        ;

        $scope.formatFecha = function (fecha) {
            var d = fecha.getDate();
            var m = fecha.getMonth() + 1;
            var a = fecha.getFullYear();
            return addZero(d) + "-" + addZero(m) + "-" + a;
        };

        $scope.reset = function () {
            $scope.persona = {
                id: null,
                nombre: null,
                apellido: null,                
                dni: null,
                cuil: null,
                fecha_nacimiento: null,
                email: null
            };
            $scope.usuario = {
                username: null,
                password: null,
                password1: null,
                estado: true
            };
            $scope.rol = {
                seleccionado: null
            };          
            $("#cuil").focus();
            $scope.busquedaCuil = true;
        };

        $scope.setPersona = function (persona) {
            $scope.persona.id = persona.id;
            $scope.persona.nombre = persona.nombre;
            $scope.persona.apellido = persona.apellido;            
            $scope.persona.dni = persona.dni;
            $scope.persona.cuil = persona.cuil;
            //Formateo la fecha de nacimiento a mostrar
            var fecha = null;
            if (persona.fecha_nacimiento != null) {
                var date = new Date(persona.fecha_nacimiento);
                fecha = $scope.formatFecha(date);
            }
            $scope.persona.fecha_nacimiento = fecha;
            $scope.persona.email = persona.email;                        
        };

        $scope.setUsuario = function (usuario) {
            $scope.usuario.id = usuario.id;
            $scope.usuario.username = usuario.usuario;
            $scope.rol.seleccionado = usuario.rol[0].id;
            $scope.usuario.estado = usuario.estado;
        };       

        $scope.getUsuario = function (id) {
            $scope.startMain();
            UsuarioService.getUsuarioById(id).then(function (response) {
                $scope.setPersona(response.data.persona);
                $scope.setUsuario(response.data);
            }, function (response) {
                utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
            }).finally(function () {
                $scope.stopMain();
            });
        };

        $("#fecha_nacimiento").datepicker({
            dateFormat: "dd-mm-yy"
        });

    }]);

app.controller("NuevoUsuarioController", ["$scope", "UsuarioService", "$controller", "utilsService", "baseUrl",
    function ($scope, UsuarioService, $controller, utilsService, baseUrl) {

        $controller('BaseUsuarioController', {$scope: $scope});
        
        $scope.edicion = false;

        $scope.buscarPersona = function () {
            if ($scope.persona.dni != null && ($scope.persona.dni.length >= 7 && $scope.persona.dni.length <= 8)) {
                $("#errorDni").hide();
                $scope.startMain();
                UsuarioService.existsPersonaByDni($scope.persona.dni).then(function (response) {
                    if (response.code == 200) {
                        if (response.data == null || response.data.usuario == null) {
                            $scope.busquedaCuil = false;
                            if (response.data != null) {                              
                                $scope.setPersona(response.data);
                            }
                        } else {
                            $scope.reset();
                            $scope.busquedaCuil = true;
                            utilsService.openDialogo("Atención", {mensaje: "El D.N.I : " + response.data.dni + " tiene asociado un usuario actualmente."}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                        }

                    }
                }, function (response) {

                }).finally(function () {
                    $scope.stopMain();
                });
            } else {
                $("#errorDni").show();
            }
        };

        $scope.saveUsuario = function (option) {
            if ($("#formUsuario").valid()) {
                $scope.usuario.rol = $scope.rol.seleccionado;                                
                $scope.object = {
                    persona: $scope.persona,
                    usuario: $scope.usuario
                };
                $scope.startMain();
                UsuarioService.save($scope.object).then(function (response) {
                    if (response.code == 200) {
                        var dialog = utilsService.openDialogo(
                                "Se agrego correctamente el usuario",
                                {
                                    mensaje: "Se agrego correctamente el usuario " + response.data.usuario
                                },
                        null,
                                baseUrlTemplates + 'templates/dialogoOk.html',
                                'CloseCtrl',
                                'lg'
                                );
                        dialog.result.then(
                                function (response) {
                                    if (option == 1) {
                                        window.location.href = baseUrlByRol + "usuarios";
                                    }
                                    if (option == 2) {
                                        $scope.reset();
                                    }
                                });
                    }
                }, function (response) {
                    utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                }).finally(function () {
                    $scope.stopMain();
                });
            }
        };
    }]);

app.controller("EditarUsuarioController", ["$scope", "UsuarioService", "$controller", "utilsService",
    function ($scope, UsuarioService, $controller, utilsService) {

        $controller('BaseUsuarioController', {$scope: $scope});
        
        $scope.edicion = true;
        $scope.$on('finishInitUsuario', function (event) {
            $scope.getUsuario($scope.data);
            $scope.busquedaCuil = false;
            $scope.disableInputDni = true;
        });
      
        $scope.updateUsuario = function (option) {
            if ($("#formUsuarioModificacion").valid()) {
                $scope.usuario.rol = $scope.rol.seleccionado;               
                $scope.object = {
                    persona: $scope.persona,
                    usuario: $scope.usuario
                };
                $scope.startMain();
                UsuarioService.update($scope.usuario.id, $scope.object).then(function (response) {
                    if (response.code == 200) {
                        var dialog = utilsService.openDialogo(
                                "Se modificó correctamente el usuario",
                                {mensaje: "Se modificó correctamente el usuario " + response.data.usuario},
                        null,
                                baseUrlTemplates + 'templates/dialogoOk.html',
                                'CloseCtrl',
                                'lg'
                                );
                        dialog.result.then(function () {
                            if (option == 1) {
                                window.location.href = baseUrlByRol + "usuarios";
                            }
                            if (option == 2) {
                                window.location.href = baseUrlByRol + "usuarios/nuevo";
                            }
                        });

                    } else {
                        utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                    }
                }, function (response) {

                }).finally(function () {
                    $scope.stopMain();
                });
            }
        };


    }]);

app.controller("GenerarClaveUsuarioController", ["$scope", "UsuarioService", "$controller", "utilsService",
    function ($scope, UsuarioService, $controller, utilsService) {

        $controller('baseViewCtrl', {$scope: $scope});
        $controller('mainController', {$scope: $scope});

        $scope.$on('finishInitUsuario', function (event) {
            $scope.getUsuario($scope.data);
        });

        $scope.saveClaveUsuario = function () {
            if ($("#formGenerarClaveUsuario").valid()) {
                $scope.startMain();
                $scope.usuario.id = $scope.dataUser.id;
                UsuarioService.updateClave($scope.usuario.id, $scope.usuario).then(function (response) {
                    if (response.code == 200) {
                        $("#botonClave").css("display", "none");
                        $scope.usuario.password = null;
                        $scope.usuario.password1 = null;
                        var dialog = utilsService.openDialogo(
                                "Generación de clave exitosa.",
                                {mensaje: "Se generó correctamente la contraseña del usuario " + response.data.usuario},
                        null,
                                baseUrlTemplates + 'templates/dialogoOk.html',
                                'CloseCtrl',
                                'lg'
                                );
                        dialog.result.then(function () {
                            window.location.href = baseUrl + "login";
                        });
                    } else {
                        utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                    }
                }, function (response) {
                }).finally(function () {
                    $scope.stopMain();
                });
            }
        };


        $scope.initUsuario = function (data) {

            $scope.dataUser = data;

            $scope.usuario = {
                id: null,
                password: null,
                password1: null
            };

            $("#formGenerarClaveUsuario").validate({
                ignore: [],
                messages: {
                    password: {
                        required: "Debe completar este campo",
                        minlength: "La contraseña debe tener 6 o más caracteres.",
                        maxlength: "La contraseña debe tener 8 o menos caracteres."
                    },
                    password1: {
                        required: "Debe completar este campo.",
                        equalTo: "No coinciden las contraseñas."
                    }
                },
                rules: {
                    password: {
                        required: true,
                        minlength: 6,
                        maxlength: 8
                    },
                    password1: {
                        required: true,
                        equalTo: "#password"
                    }
                }
            });

        };


    }]);

app.controller("RecuperarClaveUsuarioController", ["$scope", "UsuarioService", "$controller", "utilsService",
    function ($scope, UsuarioService, $controller, utilsService) {

        $controller('baseViewCtrl', {$scope: $scope});
        $controller('mainController', {$scope: $scope});

        isNumberKey = function (evt) {
            if (evt != null) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
        };

        $scope.recuperarClaveUsuario = function () {
            if ($("#formRecuperarClaveUsuario").valid()) {
                $scope.startMain();
                UsuarioService.recuperarClave($scope.usuario).then(function (response) {
                    if (response.code == 200) {
                        $("#botonClave").css("display", "none");
                        $scope.usuario.cuil = null;
                        var dialog = utilsService.openDialogo(
                                "Se envió un correo eléctronico exitosamente.",
                                {mensaje: "Se envió un correo eléctronico a " + response.data.persona.email + " para gestionar la nueva clave."},
                        null,
                                baseUrlTemplates + 'templates/dialogoOk.html',
                                'CloseCtrl',
                                'lg'
                                );
                        dialog.result.then(function () {
                            window.location.href = baseUrl + "login";
                        });
                    } else {
                        utilsService.openDialogo("Atención", {mensaje: response.message}, null, baseUrlTemplates + 'templates/dialogoWarning.html', 'CloseCtrl', 'md');
                    }
                }, function (response) {
                }).finally(function () {
                    $scope.stopMain();
                });

            }
        };

        $scope.initUsuario = function () {

            $scope.usuario = {
                cuil: null
            };

            $("#formRecuperarClaveUsuario").validate({
                ignore: [],
                messages: {
                    cuil: {
                        required: "Debe completar este campo",
                        minlength: "La contraseña debe tener 10 o más caracteres.",
                        maxlength: "La contraseña debe tener 11 o menos caracteres."
                    }
                },
                rules: {
                    cuil: {
                        required: true,
                        minlength: 10,
                        maxlength: 11
                    }
                }
            });
        };

    }]);