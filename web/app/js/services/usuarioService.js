app.service('UsuarioService', ['ajaxService', function (ajaxService) {

        this.search = function (data, paramPagination) {
            return ajaxService.post(baseUrlByRol + "ajax/usuarios/search?&sort=" + paramPagination.sortkey + "&direction=" + paramPagination.direction + "&page=" + paramPagination.page, data);
        };

        this.existsPersonaByDni = function (dni) {
            return ajaxService.get(baseUrlByRol + "ajax/usuario/persona/dni/" + dni + "/exists");
        };

        this.save = function (data) {
            return ajaxService.post(baseUrlByRol + "ajax/usuarios", data);
        };

        this.getUsuarioById = function (id) {
            return ajaxService.get(baseUrlByRol + "ajax/usuarios/" + id);
        };

        this.update = function (id, data) {
            return ajaxService.put(baseUrlByRol + "ajax/usuarios/" + id, data);
        };
        
        this.updateClave = function(id, data) {
            return ajaxService.put(baseUrlPublic + "ajax/usuarios/clave", data);
        };
        
        this.recuperarClave = function(data){
            return ajaxService.post(baseUrlPublic + "ajax/usuario/recuperar_clave_email", data);
        };
        
    }]);