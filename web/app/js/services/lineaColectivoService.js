app.service('LineaColectivoService', ['ajaxService', 'utilsService', function (ajaxService, utilsService) {

        this.search = function (data, paramPagination) {
            return ajaxService.post(baseUrlByRol + "ajax/colectivos/search?&sort=" + paramPagination.sortkey + "&direction=" + paramPagination.direction + "&page=" + paramPagination.page, data);
        };

        this.save = function (data) {
            return ajaxService.post(baseUrlByRol + "ajax/colectivos", data);
        };

        this.getLineaColectivoById = function (id) {
            return ajaxService.get(baseUrlByRol + "ajax/colectivos/" + id);
        };

        this.update = function (id, data) {
            return ajaxService.put(baseUrlByRol + "ajax/colectivos/" + id, data);
        };

    }]);


