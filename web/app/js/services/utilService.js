app.service('utilsService', ['$uibModal', function ($uibModal) {
    this.openDialogo = function (titulo, data, scope, template, controler, size) {
        $(".mainLoadAjax").addClass("working");
        var realSize = "lg";
        if (size === "xlg") {
            realSize = "xlg";
            size = "lg";
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: template,
            controller: controler,
            scope: scope,
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                titulo: function () {
                    return titulo;
                },
                datos: function () {
                    return data;
                },
                data: function () {
                    return data;
                },
                visible: true
            }
        });
        modalInstance.opened.then(function () {
            $(".mainLoadAjax").removeClass("working");
        });
        if (realSize === "xlg") {
            modalInstance.rendered.then(function () {
                $(".modal-dialog").addClass('modal-xlg');
            });
        }
        return  modalInstance;
    };
}]);





