app.service('CreditoService', ['ajaxService', 'utilsService', function (ajaxService, utilsService) {

        this.search = function (data, paramPagination) {
            return ajaxService.post(baseUrlByRol + "ajax/credito/search?&sort=" + paramPagination.sortkey + "&direction=" + paramPagination.direction + "&page=" + paramPagination.page, data);
        };

        this.searchCreditoDetalle = function (credito, data, paramPagination) {
            return ajaxService.post(baseUrlByRol + "ajax/credito/" + credito + "/searchDetalle?&sort=" + paramPagination.sortkey + "&direction=" + paramPagination.direction + "&page=" + paramPagination.page, data);
        };
        
          this.searchOrganismoPersonaByCredito = function(data, paramPagination){
                    return ajaxService.post(baseUrlByRol + "ajax/creditos/listado/organismoPersona/search?&sort=" + paramPagination.sortkey + "&direction=" + paramPagination.direction + "&page=" + paramPagination.page, data);
                };

        this.previewCsv = function (data) {
            return ajaxService.postFile(baseUrlByRol + 'previewcsv', data);
        };

        this.setCsv = function (data) {
            return ajaxService.postFile(baseUrlByRol + 'uploadcsv', data);
        };

        this.descargarPdf = function (data) {
            $(".page-content").addClass("working");
            $('html, body').animate({
                scrollTop: $("body").offset().top
            }, 100);
            var xhr = new XMLHttpRequest();
            xhr.open('POST', baseUrlByRol + "ajax/credito/detalle/descarga", true);
            xhr.responseType = 'arraybuffer';            
            xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
            xhr.onload = function (e, r, t, y, u) {
                $(".page-content").removeClass("working");
                if (this.status == 200) {                       
                    var blob = new Blob([this.response], {type: "application/pdf"});                    
                    download(blob, "Detalle-Credito.pdf", "file/pdf");
                }
            };
            xhr.send(JSON.stringify(data));
        };

    }]);