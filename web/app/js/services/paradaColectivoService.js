app.service('ParadaColectivoService', ['ajaxService', 'utilsService', function (ajaxService, utilsService) {

        this.search = function (data, paramPagination) {
            return ajaxService.post(baseUrlByRol + "ajax/paradas/search?&sort=" + paramPagination.sortkey + "&direction=" + paramPagination.direction + "&page=" + paramPagination.page, data);
        };

        this.save = function (data) {
            return ajaxService.post(baseUrlByRol + "ajax/paradas", data);
        };

        this.getParadaColectivoById = function (id) {
            return ajaxService.get(baseUrlByRol + "ajax/paradas/" + id);
        };

        this.update = function (id, data) {
            return ajaxService.put(baseUrlByRol + "ajax/paradas/" + id, data);
        };

    }]);


