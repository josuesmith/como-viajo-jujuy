app.service('EmpresaColectivoService', ['ajaxService', 'utilsService', function (ajaxService, utilsService) {

        this.search = function (data, paramPagination) {
            return ajaxService.post(baseUrlByRol + "ajax/empresas/search?&sort=" + paramPagination.sortkey + "&direction=" + paramPagination.direction + "&page=" + paramPagination.page, data);
        };

        this.save = function (data) {
            return ajaxService.post(baseUrlByRol + "ajax/empresas", data);
        };

        this.getEmpresaColectivoById = function (id) {
            return ajaxService.get(baseUrlByRol + "ajax/empresas/" + id);
        };

        this.update = function (id, data) {
            return ajaxService.put(baseUrlByRol + "ajax/empresas/" + id, data);
        };

    }]);


