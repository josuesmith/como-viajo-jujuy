app.service('ReclamoService', ['ajaxService', function (ajaxService) {

		this.adminSearch = function (data, paramPagination) {
			return ajaxService.post(baseUrlAdmin + "ajax/reclamos/search?&sort=" + paramPagination.sortkey + "&direction=" + paramPagination.direction + "&page=" + paramPagination.page, data);
		};
                this.haciendaSearch = function (data, paramPagination) {
			return ajaxService.post(baseUrlHacienda + "ajax/reclamos/search?&sort=" + paramPagination.sortkey + "&direction=" + paramPagination.direction + "&page=" + paramPagination.page, data);
		};
		this.adminGetReclamoById = function (id) {
			return ajaxService.get(baseUrlByRol + "ajax/reclamos/" + id);
		};
                this.orgBaseSearch = function (data, paramPagination) {
			return ajaxService.post(baseUrlEmpresaBase + "ajax/reclamos/search?&sort=" + paramPagination.sortkey + "&direction=" + paramPagination.direction + "&page=" + paramPagination.page, data);
		};
		this.empresaSearch = function (data, paramPagination) {
			return ajaxService.post(baseUrlByRol + "ajax/reclamos/search?&sort=" + paramPagination.sortkey + "&direction=" + paramPagination.direction + "&page=" + paramPagination.page, data);
		};
		this.empresaGetReclamoById = function (id) {
			return ajaxService.get(baseUrlByRol + "ajax/reclamos/" + id);
		};                
                this.getEstados = function () {
			return ajaxService.get(baseUrlByRol + "ajax/estados");
		};
                this.saveEstado = function (id, data) {
			return ajaxService.put(baseUrlByRol + "ajax/reclamo/" + id + "/changeEstado",  data);
		};

	}]);