app.service('ajaxService', ['$q', '$http', function ($q, $http) {
    this.get = function (url) {
        var defered = $q.defer();
        var promise = defered.promise;
        $http({
            headers: {'Content-Type': 'application/json', 'X-Requested-With': 'XMLHttpRequest'},
            method: 'GET',
            url: url
        }).then(function (response) {
            defered.resolve(response.data);
        }).catch(function (response) {
            if (response.status == 401) {
                redirect();
            } else {
                defered.reject(response.data);
            }
        }).finally(function () {
        });
        return promise;
    };
    this.post = function (url, data) {
        var defered = $q.defer();
        var promise = defered.promise;
        $http({
            url: url,
            method: 'POST',
            data: data,
            headers: {'Content-Type': 'application/json', 'X-Requested-With': 'XMLHttpRequest'}
        }).then(function (response) {
            defered.resolve(response.data);
        }).catch(function (response) {
            if (response.status == 401) {
                redirect();
            } else {
                defered.reject(response.data);
            }
        }).finally(function () {
        });
        return promise;
    };
    this.postFile = function (url, data) {
        var defered = $q.defer();
        var promise = defered.promise;
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {                
                if (jQuery.type(response) == "string") {
                    var response = JSON.parse(response);
                } else {
                   var response =  response; 
                }                             
                defered.resolve(response);
            },
            error:function (response) {
                defered.reject(response);
            }
        });
        return promise;
    };
    this.put = function (url, data) {
        var defered = $q.defer();
        var promise = defered.promise;
        $http({
            url: url,
            dataType: 'json',
            method: 'PUT',
            data: data,
            headers: {'Content-Type': 'application/json', 'X-Requested-With': 'XMLHttpRequest'}
        }).then(function (response) {
            defered.resolve(response.data);
        }).catch(function (response) {
            if (response.status == 401) {
                redirect();
            } else {
                defered.reject(response.data);
            }
        }).finally(function () {
        });
        return promise;
    };
    this.delete = function (url, data) {
        var defered = $q.defer();
        var promise = defered.promise;
        $http({
            url: url,
            method: 'DELETE',
            headers: {'Content-Type': 'application/json', 'X-Requested-With': 'XMLHttpRequest'},
            data: data
        }).then(function (response) {
            defered.resolve(response.data);
        }).catch(function (response) {
            if (response.status == 401) {
                redirect();
            } else {
                defered.reject(response.data);
            }
        }).finally(function () {
        });
        return promise;
    };
}]);


