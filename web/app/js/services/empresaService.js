app.service('EmpresaService', ['ajaxService', function (ajaxService) {

        this.adminSearch = function (data, paramPagination) {
            return ajaxService.post(baseUrlAdmin + "ajax/empresas/search?&sort=" + paramPagination.sortkey + "&direction=" + paramPagination.direction + "&page=" + paramPagination.page, data);
        };
        this.adminSavePersonaJuridica = function (data) {
            return ajaxService.post(baseUrlAdmin + "ajax/empresas", data);
        };
        this.adminUpdate = function (id, data) {
            return ajaxService.put(baseUrlAdmin + "ajax/empresas/" + id, data);
        };
        this.adminGetEmpresaById = function (id) {
            return ajaxService.get(baseUrlAdmin + "ajax/empresas/" + id);
        };
        this.haciendaSearch = function (data, paramPagination) {
            return ajaxService.post(baseUrlHacienda + "ajax/empresas/search?&sort=" + paramPagination.sortkey + "&direction=" + paramPagination.direction + "&page=" + paramPagination.page, data);
        };
        this.haciendaSavePersonaJuridica = function (data) {
            return ajaxService.post(baseUrlHacienda + "ajax/empresas", data);
        };
        this.haciendaGetEmpresaById = function (id) {
            return ajaxService.get(baseUrlHacienda + "ajax/empresas/" + id);
        };
        this.haciendaUpdate = function (id, data) {
            return ajaxService.put(baseUrlHacienda + "ajax/empresas/" + id, data);
        };
        this.empresaSearch = function (data, paramPagination) {
            return ajaxService.post(baseUrlEmpresaConc + "ajax/empresas/search?&sort=" + paramPagination.sortkey + "&direction=" + paramPagination.direction + "&page=" + paramPagination.page, data);
        };
        this.empresaConcSavePersonaJuridica = function (data) {
            return ajaxService.post(baseUrlEmpresaConc + "ajax/empresas", data);
        };
        this.empresaConcUpdate = function (id, data) {
            return ajaxService.put(baseUrlEmpresaConc + "ajax/empresas/" + id, data);
        };
        this.empresaConcUpdateMy = function (data) {
            return ajaxService.put(baseUrlEmpresaConc + "ajax/empresas-my", data);
        };
        this.empresaBaseUpdateMy = function (data) {
            return ajaxService.put(baseUrlEmpresaBase + "ajax/empresas-my", data);
        };
        this.empresaConcGetEmpresa = function (id) {
            return ajaxService.get(baseUrlEmpresaConc + "ajax/empresas/" + id);
        };
        this.existsEmpresaByCuit = function (cuit) {
            return ajaxService.get(baseUrlCommon + "ajax/empresas/cuit/" + cuit + "/exists");
        };

        this.updateClaveConc = function (data) {
            return ajaxService.put(baseUrlEmpresaConc + "ajax/empresas-my/usuario-clave", data);
        };

        this.updateClaveBase = function (data) {
            return ajaxService.put(baseUrlEmpresaBase + "ajax/empresas-my/usuario-clave", data);
        };

        this.updateClaveAdmin = function (data) {
            return ajaxService.put(baseUrlAdmin + "ajax/usuario-clave", data);
        };

        this.updateClaveHacienda = function (data) {
            return ajaxService.put(baseUrlHacienda + "ajax/usuario-clave", data);
        };

    }]);