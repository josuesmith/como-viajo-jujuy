app.service('PublicidadService', ['ajaxService', 'utilsService', function (ajaxService, utilsService) {

        this.search = function (data, paramPagination) {
            return ajaxService.post(baseUrlByRol + "ajax/publicidades/search?&sort=" + paramPagination.sortkey + "&direction=" + paramPagination.direction + "&page=" + paramPagination.page, data);
        };

        this.save = function (data) {
            return ajaxService.postFile(baseUrlByRol + "ajax/publicidades", data);
        };

        this.getPublicidadById = function (id) {
            return ajaxService.get(baseUrlByRol + "ajax/publicidades/" + id);
        };

        this.update = function (id, data) {
            return ajaxService.postFile(baseUrlByRol + "ajax/publicidades/edicion/" + id, data);
        };               

    }]);


