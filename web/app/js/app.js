var app = angular.module("app", ['ui.bootstrap','directives.custom']);
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});

app.constant("baseUrl", baseUrl);