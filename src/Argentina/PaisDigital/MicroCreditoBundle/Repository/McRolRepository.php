<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Argentina\PaisDigital\MicroCreditoBundle\Pagination\ListadoPaginar;

class McRolRepository extends EntityRepository {
    
    public function getRolesOrgs(){
        $qb = $this->createQueryBuilder('r');
        $qb->andWhere($qb->expr()->in('r.id', '2,3'));
        $results = $qb->select('r')->getQuery()->getResult();
        return $results;
    }     
}