<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Argentina\PaisDigital\MicroCreditoBundle\Pagination\ListadoPaginar;

class McPersonaRepository extends EntityRepository {

	public function save($obj) {
		try {
			$em = $this->getEntityManager();
			$em->getConnection()->beginTransaction();                       
			$em->persist($obj);
			$em->flush();
			$em->getConnection()->commit();
			return $obj;
		} catch (Exception $e) {
			$em->getConnection()->rollback();
			throw new \Symfony\Component\HttpKernel\Exception\HttpException("Ocurrio un error con la transaccion.");
		}
	}

	public function search(string $value) {
		$query = $this->createQueryBuilder('p');
		$query->where($query->expr()->like('p.apellido', "'%" . $value . "%'"));
		$query->orWhere($query->expr()->like('p.nombre', "'%" . $value . "%'"));
		$query->orWhere($query->expr()->like('p.dni', "'%" . $value . "%'"));
		$query->orWhere($query->expr()->like('p.cuil', "'%" . $value . "%'"));
		return $query->select('p')->getQuery()->getResult();
	}               

}
