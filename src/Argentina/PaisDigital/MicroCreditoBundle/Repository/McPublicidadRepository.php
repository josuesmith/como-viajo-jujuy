<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Argentina\PaisDigital\MicroCreditoBundle\Entity\McPublicidad;
use Argentina\PaisDigital\MicroCreditoBundle\Pagination\ListadoPaginar;

class McPublicidadRepository extends EntityRepository {

    public function search($first, $max, $sortField, $sortDirection, $searchParam) {
        extract($searchParam);

        $qb = $this->createQueryBuilder('p');

        if (!empty($tituloPublicidad)) {
            $qb->andWhere($qb->expr()->like('p.tituloPublicidad', "'" . $tituloPublicidad . "'"));
        }

        $qb->addOrderBy('p.tituloPublicidad', 'DESC');

        $numElementos = $qb->select('COUNT(p)')->getQuery()->getSingleScalarResult();
        $qb->setFirstResult($first)->setMaxResults($max);
        $results = $qb->select('p')->getQuery()->getResult();
        $listadoPaginar = new ListadoPaginar($results, $numElementos);
        return $listadoPaginar;
    }

    public function savePublicidad(McPublicidad $publicidad) {
        try {
            $em = $this->getEntityManager();
            $em->getConnection()->beginTransaction();
            $em->persist($publicidad);
            $em->flush();
            $em->getConnection()->commit();
            return $publicidad;
        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new HttpException(500, "Ocurrio un error con la transaccion.");
        }
    }

    public function update(McPublicidad $publicidadBD) {
        try {
            $em = $this->getEntityManager();
            $em->getConnection()->beginTransaction();
            $em->persist($publicidadBD);
            $em->flush();
            $em->getConnection()->commit();
            return $publicidadBD;
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            throw new \Symfony\Component\HttpKernel\Exception\HttpException(500, "Ocurrio un error con la transaccion.");
        }
    }
}
    