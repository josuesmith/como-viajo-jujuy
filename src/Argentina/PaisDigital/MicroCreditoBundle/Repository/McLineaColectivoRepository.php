<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Argentina\PaisDigital\MicroCreditoBundle\Entity\McLineaColectivo;
use Doctrine\ORM\Query;
use Argentina\PaisDigital\MicroCreditoBundle\Pagination\ListadoPaginar;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Pagination;
use Doctrine\ORM\Query\Expr\Join;

class McLineaColectivoRepository extends EntityRepository {

    public function search($first, $max, $sortField, $sortDirection, $searchParam) {
        extract($searchParam);

        $qb = $this->createQueryBuilder('lc');

        if (!empty($nombre)) {
            $qb->andWhere($qb->expr()->like('lc.nombre', "'%" . $nombre . "%'"));
        }

        $qb->addOrderBy('lc.nombre', 'DESC');

        $numElementos = $qb->select('COUNT(lc)')->getQuery()->getSingleScalarResult();
        $qb->setFirstResult($first)->setMaxResults($max);
        $results = $qb->select('lc')->getQuery()->getResult();
        $listadoPaginar = new ListadoPaginar($results, $numElementos);
        return $listadoPaginar;
    }

    public function saveLineaColectivo(McLineaColectivo $colectivo) {
        try {
            $em = $this->getEntityManager();
            $em->getConnection()->beginTransaction();
            $em->persist($colectivo);
            $em->flush();
            $em->getConnection()->commit();
            return $colectivo;
        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new \Symfony\Component\HttpKernel\Exception\HttpException(500, "Ocurrio un error con la transaccion.");
        }
    }

    public function update(McLineaColectivo $colectivoDB) {
        try {
            $em = $this->getEntityManager();
            $em->getConnection()->beginTransaction();
            $em->persist($colectivoDB);
            $em->flush();
            $em->getConnection()->commit();
            return $colectivoDB;
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            throw new \Symfony\Component\HttpKernel\Exception\HttpException(500, "Ocurrio un error con la transaccion.");
        }
    }
    
    public function searchLineaColectivo(string $value) {
        $query = $this->createQueryBuilder('l');
        $query->where($query->expr()->like('l.nombre', "'%" . $value . "%'"));        
        return $query->select('l')->getQuery()->getResult();
    }

}
