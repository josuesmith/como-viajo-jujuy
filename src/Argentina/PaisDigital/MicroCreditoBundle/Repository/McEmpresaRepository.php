<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Repository;

use Argentina\PaisDigital\MicroCreditoBundle\Pagination\ListadoPaginar;
use Doctrine\ORM\EntityRepository;
use Argentina\PaisDigital\MicroCreditoBundle\Entity\McEmpresa;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;

class McEmpresaRepository extends EntityRepository {

    public function search($first, $max, $sortField, $sortDirection, $searchParam) {
        extract($searchParam);

        $qb = $this->createQueryBuilder('e');

        if (!empty($nombre)) {
            $qb->andWhere($qb->expr()->like('e.nombre', "'" . $nombre . "'"));
        }

        $qb->addOrderBy('e.nombre', 'DESC');

        $numElementos = $qb->select('COUNT(e)')->getQuery()->getSingleScalarResult();
        $qb->setFirstResult($first)->setMaxResults($max);
        $results = $qb->select('e')->getQuery()->getResult();
        $listadoPaginar = new ListadoPaginar($results, $numElementos);
        return $listadoPaginar;
    }

    public function searchEmpresa(string $value) {
        $query = $this->createQueryBuilder('e');
        $query->where($query->expr()->like('e.nombre', "'%" . $value . "%'"));
        return $query->select('e')->getQuery()->getResult();
    }
    
      public function saveEmpresaColectivo(McEmpresa $empresa) {
        try {
            $em = $this->getEntityManager();
            $em->getConnection()->beginTransaction();
            $em->persist($empresa);
            $em->flush();
            $em->getConnection()->commit();
            return $empresa;
        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new HttpException(500, "Ocurrio un error con la transaccion.");
        }
    }
    
     public function update(McEmpresa $empresaDB) {
        try {
            $em = $this->getEntityManager();
            $em->getConnection()->beginTransaction();
            $em->persist($empresaDB);
            $em->flush();
            $em->getConnection()->commit();
            return $empresaDB;
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            throw new \Symfony\Component\HttpKernel\Exception\HttpException(500, "Ocurrio un error con la transaccion.");
        }
    }


}
