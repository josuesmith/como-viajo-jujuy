<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Argentina\PaisDigital\MicroCreditoBundle\Pagination\ListadoPaginar;

class McUsuarioRepository extends EntityRepository {

    public function search($first, $max, $sortField, $sortDirection, $searchParam) {
        extract($searchParam);

        if (isset($orgconc) && !empty($orgconc)) {
            //Subquery que me trae todas las org bases que pertenecen a la org concentradora
            $subqb = $this->getEntityManager()->createQueryBuilder()
                    ->select('pj.id')
                    ->from('ArgentinaPaisDigitalMicroCreditoBundle:McPersonaJuridica', 'pj')
                    ->innerJoin('pj.organizaciones', 'o')
                    ->andWhere('o.id = :orgconc')->setParameter('orgconc', $orgconc)
                    ->getDQL();
        }

// Query que obtiene los usuarios de la org concentradora y de las org bases de la misma.
        $qb = $this->createQueryBuilder('u');
        if (isset($orgconc) && !empty($orgconc)) {
            $qb->innerJoin('ArgentinaPaisDigitalMicroCreditoBundle:McPersonaFisica', 'mpf', 'WITH', 'u.persona = mpf.id')
                    ->innerJoin('mpf.empresa', 'p')
                    ->where($qb->expr()->orX(
                                    $qb->expr()->eq('p.id', ':orgconc'), $qb->expr()->in('p.id', $subqb)
                            )
                    )
                    ->setParameter('orgconc', $orgconc);
        }
        if (!empty($usuario)) {
            $qb->andWhere($qb->expr()->like('u.usuario', "'%" . $usuario . "%'"));
        }

        if (!empty($sortField)) {
            $sortField = in_array($sortField, array('id', 'usuario', 'rol')) ? $sortField : 'usuario';
            $sortDirection = ($sortDirection == 'DESC') ? 'DESC' : 'ASC';
            $qb->orderBy('u.' . $sortField, $sortDirection);
        }

        $numElementos = $qb->select('COUNT(u)')->getQuery()->getSingleScalarResult();
        $qb->setFirstResult($first)->setMaxResults($max);
        $results = $qb->select('u')->getQuery()->getResult();
        $listadoPaginar = new ListadoPaginar($results, $numElementos);
        return $listadoPaginar;
    }

    public function save($obj) {
        try {
            $em = $this->getEntityManager();
            $em->getConnection()->beginTransaction();
            $em->persist($obj);
            $em->flush();
            $em->getConnection()->commit();
            return $obj;
        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new \Symfony\Component\HttpKernel\Exception\HttpException("Ocurrio un error con la transaccion.");
        }
    }

    public function getUsername($username, $id) {
        $qb = $this->createQueryBuilder('u');
        $qb->andWhere($qb->expr()->like('u.usuario', "'%" . $username . "%'"));
        if (!is_null($id)) {
            $qb->andWhere('u.id != :id')->setParameter('id', $id);
        }
        $results = $qb->select('u')->getQuery()->getResult();
        return $results;
    }

    public function getUsuariosByArrayPersonasId($arrayPersonas){
        $arrayPersonasIdString = implode(",", $arrayPersonas);
        $qb = $this->createQueryBuilder('r');
        $qb->andWhere($qb->expr()->in('r.persona', $arrayPersonasIdString));
        $results = $qb->select('r')->getQuery()->getResult();
        return $results;
    }
}
