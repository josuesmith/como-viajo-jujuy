<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Repository;

use Argentina\PaisDigital\MicroCreditoBundle\Entity\McParadaColectivo;
use Argentina\PaisDigital\MicroCreditoBundle\Pagination\ListadoPaginar;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;

class McParadaColectivoRepository extends EntityRepository {

    public function search($first, $max, $sortField, $sortDirection, $searchParam) {
        extract($searchParam);

        $qb = $this->createQueryBuilder('pc');

        if (!empty($nombre)) {
            $qb->andWhere($qb->expr()->like('pc.nombre', "'" . $nombre . "'"));
        }

        $qb->addOrderBy('pc.nombre', 'DESC');

        $numElementos = $qb->select('COUNT(pc)')->getQuery()->getSingleScalarResult();
        $qb->setFirstResult($first)->setMaxResults($max);
        $results = $qb->select('pc')->getQuery()->getResult();
        $listadoPaginar = new ListadoPaginar($results, $numElementos);
        return $listadoPaginar;
    }

    public function searchParadaColectivoCercana($parametros, $box, $earthRadio, $band) {
        extract($parametros);

        if ($band) {
            $sql = "SELECT pc.id, pc.nombre AS Parada, pc.latitud, pc.longitud, id_linea, (" . $earthRadio . " * ACOS( COS( RADIANS(" . $latitudDesde . ") ) * COS(RADIANS( pc.latitud ) ) * COS(RADIANS( pc.longitud ) - RADIANS(" . $longitudDesde . ") ) + SIN( RADIANS(" . $latitudDesde . ") ) * SIN(RADIANS( pc.latitud ) ) )) AS distancia, ";
        } else {
            $sql = "SELECT pc.id, pc.nombre AS Parada, pc.latitud, pc.longitud, id_linea, (" . $earthRadio . " * ACOS( COS( RADIANS(" . $latitudHasta . ") ) * COS(RADIANS( pc.latitud ) ) * COS(RADIANS( pc.longitud ) - RADIANS(" . $longitudHasta . ") ) + SIN( RADIANS(" . $latitudHasta . ") ) * SIN(RADIANS( pc.latitud ) ) )) AS distancia, ";
        }
        $sql .= "lc.nombre AS Linea, lc.descripcion AS 'Linea Descripcion', me.id AS id_empresa, me.nombre AS Empresa "
                . "FROM mc_paradas_colectivos pc "
                . "INNER JOIN mc_lineas_colectivos lc ON lc.id = pc.id_linea "
                . "INNER JOIN mc_empresas me ON me.id = lc.id_empresa "
                . "WHERE (pc.latitud BETWEEN " . $box['min_lat'] . " AND " . $box['max_lat'] . ") AND (pc.longitud BETWEEN " . $box['min_lng'] . " AND " . $box['max_lng'] . ") "
                . "HAVING distancia < 1 "
                . "ORDER BY distancia ASC "
                . "LIMIT 50 ";

        $em = $this->getEntityManager();
        $con = $em->getConnection();
        $query = $con->prepare($sql);
        $query->execute();
        $results = $query->fetchAll();
        return $results;
    }

    public function saveParadaColectivo(McParadaColectivo $parada) {
        try {
            $em = $this->getEntityManager();
            $em->getConnection()->beginTransaction();
            $em->persist($parada);
            $em->flush();
            $em->getConnection()->commit();
            return $parada;
        } catch (Exception $e) {
            $em->getConnection()->rollback();
            throw new HttpException(500, "Ocurrio un error con la transaccion.");
        }
    }

    public function update(McParadaColectivo $paradaDB) {
        try {
            $em = $this->getEntityManager();
            $em->getConnection()->beginTransaction();
            $em->persist($paradaDB);
            $em->flush();
            $em->getConnection()->commit();
            return $paradaDB;
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            throw new \Symfony\Component\HttpKernel\Exception\HttpException(500, "Ocurrio un error con la transaccion.");
        }
    }

    public function getRecorrido($desde, $hasta, $linea) {
        $sql = "SELECT * "
                . "FROM mc_paradas_colectivos pc "
                . "WHERE pc.id_linea = " . $linea . " AND pc.id >= " . $desde . " AND pc.id <= " . $hasta;        

        $em = $this->getEntityManager();
        $con = $em->getConnection();
        $query = $con->prepare($sql);
        $query->execute();
        $results = $query->fetchAll();
        return $results;
    }
    
    public function searchParadaIntermedia ($desde, $hasta){
        $sql = "SELECT mp.id AS idDesde, mpb.id AS idHasta, mp.id_linea AS LineaDesde, mpb.id_linea AS LineaHasta, 111.111 *
  DEGREES(ACOS(LEAST(1.0, COS(RADIANS(mp.latitud))
         * COS(RADIANS(mpb.latitud))
         * COS(RADIANS(mp.longitud - mpb.longitud))
         + SIN(RADIANS(mp.latitud))
         * SIN(RADIANS(mpb.latitud))))) AS distance_in_km
    FROM mc_paradas_colectivos AS mp
    JOIN mc_paradas_colectivos AS mpb ON mp.id <> mpb.id
    WHERE mp.id_linea = " . $desde["id_linea"] . " AND mpb.id_linea = " . $hasta["id_linea"] . " 
    HAVING distance_in_km < 1.0 
    ORDER BY distance_in_km ASC";    
       

        $em = $this->getEntityManager();
        $con = $em->getConnection();
        $query = $con->prepare($sql);
        $query->execute();
        $results = $query->fetchAll();
        return $results;
    }
    
    //Metodo sql que busca el punto mas cercano a uno dado por parametro.
    public function getParadasCercanasADestino($parada, $parametros){
         $sql = "SELECT *, SQRT (POW(69.1 * (pc.latitud - " . $parametros["latitudHasta"] . "), 2) + POW(69.1 * (" . $parametros["longitudHasta"] . " - pc.longitud) * COS(pc.latitud / 57.3), 2)) AS distance "
                . "FROM mc_paradas_colectivos pc "
                . "WHERE pc.id_linea = " . $parada["id_linea"] . " "
                . "HAVING distance < 0.5 "
                . "ORDER BY distance ";                                         

        $em = $this->getEntityManager();
        $con = $em->getConnection();
        $query = $con->prepare($sql);
        $query->execute();
        $results = $query->fetchAll();
        return $results;
    }
    
        public function getParadasCercanasAOrigen($parada, $parametros){
         $sql = "SELECT *, SQRT (POW(69.1 * (pc.latitud - " . $parametros["latitudDesde"] . "), 2) + POW(69.1 * (" . $parametros["longitudDesde"] . " - pc.longitud) * COS(pc.latitud / 57.3), 2)) AS distance "
                . "FROM mc_paradas_colectivos pc "
                . "WHERE pc.id_linea = " . $parada["id_linea"] . " "
                . "HAVING distance < 0.5 "
                . "ORDER BY distance ";                                     

        $em = $this->getEntityManager();
        $con = $em->getConnection();
        $query = $con->prepare($sql);
        $query->execute();
        $results = $query->fetchAll();
        return $results;
    }

}
