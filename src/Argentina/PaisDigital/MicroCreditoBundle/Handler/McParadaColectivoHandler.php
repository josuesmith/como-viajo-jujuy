<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Handler;

use Argentina\PaisDigital\MicroCreditoBundle\Entity\McParadaColectivo;
use Argentina\PaisDigital\MicroCreditoBundle\Handler\McLineaColectivoHandler;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\CodesWebServices;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Constants;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Mobile_Detect;

class McParadaColectivoHandler extends BaseHandler {

    public function __construct(ContainerInterface $container, EntityManagerInterface $entityManager, McLineaColectivoHandler $service) {
        $this->container = $container;
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(McParadaColectivo::class);
        $this->service = $service;
    }

    public function getById($id) {
        return $this->repository->findOneById($id);
    }

    public function search($offset, $limit, $sortField, $sortDirection, $searchParam) {
        $lp = $this->repository->search($offset, $limit, $sortField, $sortDirection, $searchParam);
        $lineaColectivos = $this->toarray($lp->getListado(), 'parada');
        $lp->setListado($lineaColectivos);
        return $lp;
    }

    public function getArrayUnicoIndirectos($array) {
        $temp_array = array();
        if (COUNT($array) > 0) {
            $i = 0;
            $key_linea = array();
            $key_transbordo = array();
            foreach ($array AS &$val) {
                if (!in_array($val["linea"]["id_linea"], $key_linea) || !in_array($val["linea_transbordo"]["id_linea"], $key_transbordo)) {
                    $key_linea[$i] = $val["linea"]["id_linea"];
                    $key_transbordo[$i] = $val["linea_transbordo"]["id_linea"];
                    $temp_array[$i] = $val;
                    $i++;
                }
            }
        }
        return $temp_array;
    }

    private function getRecorrido($idParadaDesde, $idParadaHasta, $idLinea) {
        $recorrido = $this->repository->getRecorrido($idParadaDesde, $idParadaHasta, $idLinea);
        return $this->toarray($recorrido, "recorrido");
    }

    public function searchLinea($d, $a) {
        $temp = false;
        foreach ($d AS &$d1) {
            if ($d1["linea"]["id_linea"] == $a["id_linea"]) {
                $temp = true;
            }
        }
        return $temp;
    }
    
       public function getMenorRecorrido($d, $a) {
        $aux = [];        
            if ($d["id"] < $a["id"]) {
                if (COUNT($aux) == 0) {
                    $aux = $this->getRecorrido($d["id"], $a["id"], $d["id_linea"]);
                } else {
                    $auxRecorrido = $this->getRecorrido($d["id"], $a["id"], $d["id_linea"]);
                    if (COUNT($aux) > COUNT($auxRecorrido)) {
                        $aux = $auxRecorrido;
                    }
                }
            }        
        return $aux;
    }

    public function getLineaColectivosDirectos($lineaDesde, $lineaHasta) {
        $arrayAux = array();
        $i = 0;
        foreach ($lineaDesde AS &$a1) {
            foreach ($lineaHasta AS &$a2) {
                if ($a1["id_linea"] == $a2["id_linea"]) {
                    $recorrido = $this->getMenorRecorrido($a1, $a2);
                    if (!$this->searchLinea($arrayAux, $a1) && COUNT($recorrido) > 0) {
                        $arrayAux[$i]["linea"] = $a1;
                        $arrayAux[$i]["linea"]["recorrido"] = $recorrido;
                        $i++;
                    } else {
                        if (COUNT($recorrido) > 0){
                            $arrayAux = $this->replaceArrayAux($arrayAux, $a1, $recorrido);                            
                            break;
                        }
                    }
                }
            }
        }
        return $arrayAux;
    }

    public function getParadaIntermedia($d, $a) {
        return $this->repository->searchParadaIntermedia($d, $a);
    }

    public function getRecorridoTransbordo($desde, $hasta, $recorrido) {
        $band = false;
        for ($i = 0; $i < COUNT($recorrido); $i++) {
            if ($recorrido[$i]["LineaDesde"] == $desde["id_linea"] && $recorrido[$i]["idDesde"] > $desde["id"] && (($recorrido[$i]["idDesde"] - $desde["id"]) < 25) //Comprobamos que el recorrido no sea largo
                    && $recorrido[$i]["LineaHasta"] == $hasta["id_linea"] && $recorrido[$i]["idHasta"] < $hasta["id"] && (($hasta["id"] - $recorrido[$i]["idHasta"]) < 25)) { //Comprobamos que el recorrido no sea largo
                $band = $recorrido[$i];
                break;
            }
        }
        return $band;
    }

    public function getLineaColectivosIndirectos($lineaDesde, $lineaHasta) {
        $arrayAux = array();
        $z = 0;
        if ((COUNT($lineaDesde) > 0) && (COUNT($lineaHasta) > 0)) {// se comprueba que al menos venga una linea desde y una linea hasta.   
            foreach ($lineaDesde AS &$d) {
                foreach ($lineaHasta AS &$a) {
                    $intermediaAux = $this->getParadaIntermedia($d, $a);
                    $existeRecorrido = $this->getRecorridoTransbordo($d, $a, $intermediaAux);
                    if ($existeRecorrido) {
                        $arrayAux[$z]["linea"] = $d;
                        $arrayAux[$z]["linea"]["recorrido"] = $this->getRecorrido($d["id"], $existeRecorrido["idDesde"], $d["id_linea"]);
                        $arrayAux[$z]["linea_transbordo"] = $a;
                        $arrayAux[$z]["linea_transbordo"]["recorrido_transbordo"] = $this->getRecorrido($existeRecorrido["idHasta"], $a["id"], $a["id_linea"]);
                        $z++;
                    }
                }
            }
        } else {
            $arrayAux = [];
        }
        return $arrayAux;
    }

    public function getLimites($lat, $lng, $distance, $earthRadius) {
        $return = array();

        $cardinalCoords = array(
            'north' => '0',
            'south' => '180',
            'east' => '90',
            'west' => '270'
        );
        $rLat = deg2rad($lat);
        $rLng = deg2rad($lng);
        $rAngDist = $distance / $earthRadius;
        foreach ($cardinalCoords as $name => $angle) {
            $rAngle = deg2rad($angle);
            $rLatB = asin(sin($rLat) * cos($rAngDist) + cos($rLat) * sin($rAngDist) * cos($rAngle));
            $rLonB = $rLng + atan2(sin($rAngle) * sin($rAngDist) * cos($rLat), cos($rAngDist) - sin($rLat) * sin($rLatB));
            $return[$name] = array('lat' => (float) rad2deg($rLatB),
                'lng' => (float) rad2deg($rLonB));
        }
        return array(
            'min_lat' => $return['south']['lat'],
            'max_lat' => $return['north']['lat'],
            'min_lng' => $return['west']['lng'],
            'max_lng' => $return['east']['lng']
        );
    }

    public function getDistanciaOrigenDestino($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius) {
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }

    //Función que busca la parada mas cercana a la lat y longitud enviados.
    public function getParadasCercanasADestino($desde, $parametros) {
        return $this->repository->getParadasCercanasADestino($desde, $parametros);
    }

    //Función que busca la parada mas cercana a la lat y longitud enviados.
    public function getParadasCercanasAOrigen($hasta, $parametros) {
        return $this->repository->getParadasCercanasAOrigen($hasta, $parametros);
    }

    public function getMenorRecorridoOrigen($d, $paradas) {
        $aux = [];
        foreach ($paradas AS &$p) {
            if ($d["id"] < $p["id"]) {
                if (COUNT($aux) == 0) {
                    $aux = $this->getRecorrido($d["id"], $p["id"], $d["id_linea"]);
                } else {
                    $auxRecorrido = $this->getRecorrido($d["id"], $p["id"], $d["id_linea"]);
                    if (COUNT($aux) > COUNT($auxRecorrido)) {
                        $aux = $auxRecorrido;
                    }
                }
            }
        }
        return $aux;
    }
    
    public function getMenorRecorridoDestino($a, $paradas) {
        $aux = [];
        foreach ($paradas AS &$p) {
            if ($p["id"] < $a["id"]) {
                if (COUNT($aux) == 0) {
                    $aux = $this->getRecorrido($p["id"], $a["id"], $a["id_linea"]);
                } else {
                    $auxRecorrido = $this->getRecorrido($p["id"], $a["id"], $a["id_linea"]);
                    if (COUNT($aux) > COUNT($auxRecorrido)) {
                        $aux = $auxRecorrido;
                    }
                }
            }
        }
        return $aux;
    }

    public function replaceArrayAux($d, $parada, $recorrido) {
        for ($i = 0; $i < COUNT($d); $i++) {
            if ($d[$i]["linea"]["id_linea"] == $parada["id_linea"] && COUNT($d[$i]["linea"]["recorrido"]) > COUNT($recorrido)) {
                if (COUNT($d[$i]["linea"]["recorrido"]) - COUNT($recorrido) > 1) {
                    $d[$i]["linea"] = $parada;
                    $d[$i]["linea"]["recorrido"] = $recorrido;
                }
            }
        }
        return $d;
    }

    //Función que busca las paradas mas cercanas al destino, en función de las paradas Origen.
    public function getLineaColectivosDirectosByOrigen($lineaDesde, $parametros) {
        $arrayAux = array();
        $i = 0;
        foreach ($lineaDesde AS &$d1) {
            $paradas = $this->getParadasCercanasADestino($d1, $parametros);
            $recorrido = $this->getMenorRecorridoOrigen($d1, $paradas);
            if (!$this->searchLinea($arrayAux, $d1) && COUNT($recorrido) > 0) {
                $arrayAux[$i]["linea"] = $d1;
                $arrayAux[$i]["linea"]["recorrido"] = $recorrido;
                $i++;
            } else {
                if (COUNT($recorrido) > 0) {
                    $arrayAux = $this->replaceArrayAux($arrayAux, $d1, $recorrido);
                    break;
                }
            }
        }
        return $arrayAux;
    }

    //Función que busca las paradas mas cercanas al Origen, en función de las paradas Destino.
    public function getLineaColectivosDirectosByDestino($lineaHasta, $parametros) {
        $arrayAux = array();
        $i = 0;
        foreach ($lineaHasta AS &$a1) {
            $paradas = $this->getParadasCercanasAOrigen($a1, $parametros);
            $recorrido = $this->getMenorRecorridoDestino($a1, $paradas);

            if (!$this->searchLinea($arrayAux, $a1) && COUNT($recorrido) > 0) {
                $arrayAux[$i]["linea"] = $a1;
                $arrayAux[$i]["linea"]["recorrido"] = $recorrido;
                $i++;
            } else {
                if (COUNT($recorrido) > 0) {
                    $arrayAux = $this->replaceArrayAux($arrayAux, $a1, $recorrido);
                    break;
                }
            }
        }
        return $arrayAux;
    }

    public function searchParadaColectivoCercana($request) {
        $restResponse = new Response();
        $parametros = $request->request->all();
        $detect = new Mobile_Detect;
        try {
            if (true) { //$detect->isMobile()
                $distancia = $this->getDistanciaOrigenDestino($parametros["latitudDesde"], $parametros["longitudDesde"], $parametros["latitudHasta"], $parametros["longitudHasta"], Constants::EARTHRADIUS);
                if ($distancia > 0.2) { // Si la distancia desde el Origen hasta en Destino es menor a 200 metros, no busca recorridos.
                    $paradasCercanasDesde = $this->getLimites($parametros["latitudDesde"], $parametros["longitudDesde"], $parametros["radio"], Constants::EARTHRADIUS);
                    $paradasDesde = $this->repository->searchParadaColectivoCercana($parametros, $paradasCercanasDesde, Constants::EARTHRADIUS, true);
                    
                    $paradasCercanasHasta = $this->getLimites($parametros["latitudHasta"], $parametros["longitudHasta"], $parametros["radio"], Constants::EARTHRADIUS);
                    $paradasHasta = $this->repository->searchParadaColectivoCercana($parametros, $paradasCercanasHasta, Constants::EARTHRADIUS, false);                    
                   
                    // Sección donde se busca los recorrido directos.
                    $paradasTotal["directos"] = $this->getLineaColectivosDirectos($paradasDesde, $paradasHasta);
                    if (COUNT($paradasTotal["directos"]) == 0) {
                        $paradasTotal["directos"] = $this->getLineaColectivosDirectosByOrigen($paradasDesde, $parametros);
                    }
                    if (COUNT($paradasTotal["directos"]) == 0) {
                        $paradasTotal["directos"] = $this->getLineaColectivosDirectosByDestino($paradasHasta, $parametros);
                    }
                    // FIn de la sección donde se busca los recorrido directos.

                    $paradasTotal["indirectos"] = $this->getLineaColectivosIndirectos($paradasDesde, $paradasHasta);

                    if (COUNT($paradasTotal["directos"]) <= 0) {
                        $paradasTotal["directos"] = [];
                    }
                    if (COUNT($paradasTotal["indirectos"]) <= 0) {
                        $paradasTotal["indirectos"] = [];
                    } else {
                        foreach ($paradasTotal["indirectos"] AS $key => $element) { //Si no trae el transbordo o la linea no muestra en el json
                            if (!isset($element["linea_transbordo"]) || !isset($element["linea"])) {
                                unset($paradasTotal["indirectos"][$key]);
                            }
                        }
                        $paradasTotal["indirectos"] = $this->getArrayUnicoIndirectos($paradasTotal["indirectos"]);
                    }

                    if (COUNT($paradasTotal["indirectos"]) > 0 || COUNT($paradasTotal["directos"]) > 0) {
                        $restResponse->setData($paradasTotal);
                        $restResponse->setCode(CodesWebServices::OK);
                        $restResponse->setMessage("Ok");
                    } else {
                        $restResponse->setCode(CodesWebServices::ERROR_LINEA_COLECTIVO);
                        $restResponse->setMessage("No se encontraron Lineas de colectivos para dicho recorrido.");
                    }
                } else {
                    $restResponse->setCode(CodesWebServices::ERROR_LINEA_COLECTIVO);
                    $restResponse->setMessage("Entre el origen y el destino debe haber más de 200 metros de distancia.");
                }
            } else {
                $restResponse->setCode(CodesWebServices::ERROR);
                $restResponse->setMessage("No tiene permiso para acceder al sistema.");
            }
        } catch (Exception $e) {
            $restResponse->setCode(CodesWebServices::ERROR);
            $restResponse->setMessage($e->getMessage());
        }

        $serializedEntity = $this->container->get('serializer')->serialize($restResponse, 'json');
        return $serializedEntity;

        }

        public function getParadaColectivoFromRequest(Request $request, int $id = null):McParadaColectivo {
        $nombre = $request->request->get('nombre');
        $latitud = $request->request->get('latitud');
        $longitud = $request->request->get('longitud');
        $linea = $this->service->getById($request->request->get('linea'));

        if (is_null($id)) {
            $parada = new McParadaColectivo();
        } else {
            $parada = $this->repository->findOneById($id);
        }
        $parada->setNombre($nombre);
        $parada->setLatitud($latitud);
        $parada->setLongitud($longitud);
        $parada->setLinea($linea);
        return $parada;
    }

    public function saveParadaColectivo(McParadaColectivo $parada) {
        $this->repository->saveParadaColectivo($parada);
        $parada = $this->toarray($parada, "parada");
        return $parada;
    }

    public function updateParadaColectivo(McParadaColectivo $parada, int $idParadaColectivo) {
        $parada = $this->repository->update($parada);
        $parada = $this->toarray($parada, "parada");
        return $parada;
    }

}
