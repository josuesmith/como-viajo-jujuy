<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Handler;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouterInterface;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Constants;

class BaseHandler {

    
	protected $userContext = null;
	protected $context = null;
	protected $tokenStorage = null;
	protected $router;

	function __construct(TokenStorageInterface $tokenStorage, RouterInterface $router, ContainerInterface $container, EntityManagerInterface $entityManager) {
                                $this->om = $entityManager;
                                $this->container = $container;
		$this->tokenStorage = $tokenStorage;
		$this->router = $router;
	}

	public function toarray($object, $group = null) {
		$this->serializer = SerializerBuilder::create()->build();
		$serializerContext = SerializationContext::create();
		if (!is_null($group)) {
			$serializerContext->setGroups(array($group));
		}
		$serializerContext->setSerializeNull(true);

		return $this->serializer->toArray($object, $serializerContext);
	}

	public function isRolAdmin() {
		$this->userContext = $this->tokenStorage->getToken()->getUser();
		$roles = $this->userContext->getRol();
		$isAdmin = false;
		foreach ($roles as $r) {
			if ($r->getNombre() === Constants::ROLE_ADMIN) {
				$isAdmin = true;
			}
		}
		return $isAdmin;
	}

	public function isRolEmpresa() {
		$this->userContext = $this->tokenStorage->getToken()->getUser();
		$roles = $this->userContext->getRol();
		$isEmpresa = false;
		foreach ($roles as $r) {
			if ($r->getNombre() === Constants::ROLE_ORGANIZACION_CONC) {
				$isEmpresa = true;
			}
		}
		return $isEmpresa;
	}

}
