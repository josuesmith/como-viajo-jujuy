<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Handler;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MailService
 *
 * @author pchacon
 */
class MailHandler extends \Symfony\Bundle\FrameworkBundle\Controller\Controller {

    protected $mailer;
    protected $container;

    public function __construct(\Swift_Mailer $mailer, ContainerInterface $container = null) {
        $this->mailer = $mailer;
        $this->container = $container;
    }

    public function sendMail($mailFrom, $mailTo, $mensaje, $subject) {
        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($mailFrom)
                ->setTo($mailTo)
                ->setContentType("text/html")
                ->setBody(
                '<p>' . $mensaje . '</p>' .
                '<br/>' .
                '<p>Saludos Cordiales.</p>' .
                '<p></p>', 'text/html'
        );

        $this->mailer->send($message);
    }

}
