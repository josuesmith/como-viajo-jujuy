<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Argentina\PaisDigital\MicroCreditoBundle\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AccessDeniedHandler extends BaseHandler implements AccessDeniedHandlerInterface {

	public function handle(Request $request, AccessDeniedException $accessDeniedException) {
		if ($this->isRolAdmin()) {
			return new RedirectResponse($this->router->generate('admin_home'));
		}
		if ($this->isRolEmpresa()) {
//			return new RedirectResponse($this->router->generate('empresaconc_home'));
			return new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException("No tiene los permisos suficientes para realizar esta acción.");
		}
	}

}
