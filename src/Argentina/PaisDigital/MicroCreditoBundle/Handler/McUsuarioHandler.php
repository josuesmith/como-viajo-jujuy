<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Handler;

use Argentina\PaisDigital\MicroCreditoBundle\Entity\McRol;
use Argentina\PaisDigital\MicroCreditoBundle\Entity\McUsuario;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Constants;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\EmailsMessages;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Encrypt;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

class McUsuarioHandler extends BaseHandler {

    public function __construct(ContainerInterface $container, EntityManagerInterface $entityManager) {
        $this->container = $container;
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(McUsuario::class);
    }

    public function search($offset, $limit, $sortField, $sortDirection, $searchParam) {
        $lp = $this->repository->search($offset, $limit, $sortField, $sortDirection, $searchParam);
        $users = $this->toarray($lp->getListado(), 'user');
        $lp->setListado($users);
        return $lp;
    }

    public function getUsuarioByPersona($idPersona) {
        return $this->repository->findOneByPersona($idPersona);
    }

    public function getRoles() {
        $roles = $this->entityManager->getRepository(McRol::class)->findAll();
        return $this->toarray($roles, 'user');
    }

    public function getRolesOrgs() {
        $roles = $this->entityManager->getRepository(McRol::class)->getRolesOrgs();
        return $this->toarray($roles, 'user');
    }

    public function getMenuFromRol(string $rol) {
        $rol = $this->om->getRepository('ArgentinaPaisDigitalTalentoBundle:TdRol')->findOneByNombre($rol);
        $submenu = $rol->getMenu()->getVdSubMenu();
        return $this->toarray($submenu, 'menu');
    }

    public function getById($userId) {
        $user = $this->repository->findOneById($userId);
        return $this->toarray($user, 'user');
    }

    private function getCodigoVerificacion($id, $fecha) {
        $mensaje = $id . '_' . $fecha;
        $clave = $this->container->getParameter('clave_encriptacion_generar_clave');
        $codigo = Encrypt::encrypt($mensaje, $clave);
        return $codigo;
    }    

    private function enviarMailRecuperarClave($persona, $usuario, $request) {
        $emailFrom = $this->container->getParameter('mailer_user');
        $urlFront = strstr($request->headers->get('referer'), 'user', true);
        $emailTo = $persona->getEmail();
        $subject = 'Como Viajo Jujuy - Recuperación de contraseña';
        $apellidoNombre = $persona->getApellido() . " " . $persona->getNombre();
        $fechaEnvioMail = new DateTime();
        $fechaVerificacion = $fechaEnvioMail->getTimestamp();
        $codigo = $this->getCodigoVerificacion($usuario->getId(), $fechaVerificacion);
        $linkVerficar = $urlFront . Constants::URL_RECUPERAR_CLAVE;
        $linkVerificacion = $linkVerficar . $codigo;
        $mensaje = EmailsMessages::mensajeRecuperarClaveEmail($apellidoNombre, $usuario->getUsuario(), $linkVerificacion, $urlFront);
        $this->container->get(MailHandler::class)->sendMail($emailFrom, $emailTo, $mensaje, $subject);
    }

    public function save($data, $id = null, $request) {
        $persona_entity = $this->container->get(McPersonaHandler::class)->savePersona($data["persona"]);
        $user_entity = $this->saveUsuario($data["usuario"], $persona_entity, $id);
        $result = $this->repository->save($user_entity);
        return $this->toarray($result, "user");
    }       

    public function saveUsuario($usuario, $persona_entity, $id) {
        $createUser = false;
        $encoder = $this->container->get("security.password_encoder");
        $username = $usuario["username"];
        $exisiteUsername = $this->repository->getUsername($username, $id);

        if (COUNT($exisiteUsername) == 0) {
            if (is_null($id)) {
                $user = new McUsuario();
                $user->setFechaCreacion(new DateTime());
                $createUser = true;
            } else {
                $user = $this->repository->findOneById($id);
            }
            $user->setUsuario($username);
            $user->setSalt("");  
            $user->setEstado(true);
            if ($createUser) {
                $password = $usuario["password"];
                $encoded = $encoder->encodePassword($user, $password);
                $user->setPassword($encoded);                
            } else {
                // si es null quiere decir q al ser modificcion no quiso midificar la contraseña
                if (trim($usuario["password"]) != "" && $usuario["password"] != null) {
                    $encoded = $encoder->encodePassword($user, $usuario["password"]);
                    $user->setPassword($encoded);
                }
                $user->setFechaActualizacion(new DateTime());
            }
            $user->setPersona($persona_entity);
            $rol = $this->container->get(McRolHandler::class)->getRolById($usuario["rol"]);

            /*             * ***roles anteriores**** */
            $roles = $user->getRoles();
            foreach ($roles as $r) {
                $aux = $this->container->get(McRolHandler::class)->getRolByNombre($r);
                $user->removeRol($aux);
            }
            /*             * *********************** */

            $user->addRol($rol);
            return $user;
        } else {
            throw new HttpException(409, "Ya existe el usuario con el con el NOMBRE DE USUARIO: " . $username);
        }
    }

    public function getDataPublicUser($clave_encriptada, $band) {
        $clave = $this->container->getParameter('clave_encriptacion_generar_clave');
        $codigo = Encrypt::decrypt($clave_encriptada, $clave);

        // Obtengo el id y la fecha del usuario.
        $data = explode('_', $codigo);

        if (COUNT($data) != 2) {
            throw new HttpException(409, "El link al que quiere ingresar no es válido.");
        }

        $timestamp = (int) $data[1];
        $dt = new DateTime();
        $dt->setTimestamp($timestamp);

        $horas = $this->container->getParameter('horas_validar_link_generar_clave');
        if ($horas > 0) { // Si el parametro de horas, es mayor que 0, deberá comprobar que la fecha este dentro del rango.
            $inter = 'PT' . $horas . 'H';
            $dt->add(new DateInterval($inter));

            $dtNow = new DateTime();
            if ($dtNow > $dt) {
                throw new HttpException(409, "El código del link de generación de clave ya no es válido");
            } else {
                $usuario = $this->getById($data[0]);
            }
        } else { //Si el parametro horas es negativo, no verificara un rango de fecha.
            $usuario = $this->getById($data[0]);
        }

        if ($band) { //Cuando $band es true, se esta dando de alta un usuario. Si es falso, es porque el usuario quiere recuperar su clave.
            if (!$usuario["primer_ingreso"]) {
                throw new HttpException(409, "El código del link de generación de clave ya no es válido");
            }
        }

        return $usuario;
    }

    public function updateClave($data) {
        $encoder = $this->container->get("security.password_encoder");
        $usuario = $this->repository->findOneById($data["id"]);        
        $encoded = $encoder->encodePassword($usuario, $data["password"]);
        $usuario->setPassword($encoded);        
        $usuario->setFechaActualizacion(new DateTime());
        $result = $this->repository->save($usuario);             

        return $this->toarray($result, "user");
    }

    public function recuperarClaveEmail($request, $cuil) {   
        
        $persona = $this->container->get(McPersonaHandler::class)->getPersonaByCuil($cuil);
        
        if (!is_null($persona)) {
            $usuario = $this->getUsuarioByPersona($persona->getId());
        } else {
            throw new HttpException(409, "El cuil ingresado no pertenece a una persona registrada en el sistema.");
        }
        if (!is_null($usuario)) {
            $this->enviarMailRecuperarClave($persona, $usuario, $request);
        } else {
            throw new HttpException(409, "El cuil ingresado no esta vinculado a un usuario.");
        }

        $usuario = $this->toarray($usuario, "user");
        return $usuario;
    }

}
