<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Handler;

use Argentina\PaisDigital\MicroCreditoBundle\Entity\McPublicidad;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\CodesWebServices;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Request;
use Mobile_Detect;

class McPublicidadHandler extends BaseHandler {

    public function __construct(ContainerInterface $container, EntityManagerInterface $entityManager) {
        $this->container = $container;
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(McPublicidad::class);
    }

    public function getAllPublicidades() {
        $detect = new Mobile_Detect;
        $restResponse = new Response();
        $publicidades = array();         
        try {
            if ($detect->isMobile()) {
                $publicidades["publicidades"] = $this->repository->findBy(array("estado" => true));
                $publicidades["publicidades"] = $this->toarray($publicidades["publicidades"], "publicidad");
                if (COUNT($publicidades["publicidades"]) > 0) {
                    foreach($publicidades["publicidades"] AS &$publicidad){                                        
                    $publicidad["ruta_archivo"] = $this->container->getParameter('path_image_video') . $publicidad["nombre_archivo"]; 
                    }
                    $restResponse->setData($publicidades);
                    $restResponse->setCode(CodesWebServices::OK);
                    $restResponse->setMessage("Ok");
                } else {
                    $restResponse->setCode(CodesWebServices::OK);
                    $restResponse->setMessage("No se encontraron publicidades.");
                }
            } else {
                $restResponse->setCode(CodesWebServices::ERROR);
                $restResponse->setMessage("No tiene permiso para acceder al sistema.");
            }
        } catch (Exeption $e) {
            $restResponse->setCode(CodesWebServices::ERROR);
            $restResponse->setMessage($e->getMessage());
        }

        $serializedEntity = $this->container->get('serializer')->serialize($restResponse, 'json');
        return $serializedEntity;
    }

    public function getPublicidadById($id) {
        return $this->repository->findOneById($id);
    }

    public function getTituloPublicidadByNombre($tituloPublicidad) {
        return $this->repository->findOneByTituloPublicidad($tituloPublicidad);
    }

    public function searchPublicidades($offset, $limit, $sortField, $sortDirection, $searchParam) {
        $lp = $this->repository->search($offset, $limit, $sortField, $sortDirection, $searchParam);
        $publicidades = $this->toarray($lp->getListado(), 'publicidad');
        $lp->setListado($publicidades);
        return $lp;
    }

    private function getPathMultimedia($request) {
        $basePath = $this->container->getParameter('kernel.root_dir');
        $basePath = str_replace('/app', '/web', $basePath);

        $basePath = $basePath . $this->container->getParameter('path_image_video');
        $path = "$basePath";
        return $path;
    }

    private function saveArchivo($data, $ruta) {
        if (!file_exists($ruta)) {
            mkdir($ruta, 0777, true);
        }
        $fileType = $data['type'];
        $info = pathinfo($data['name']);
        $ext = $info['extension']; // get the extension of the file
        $filename = trim($info['filename']);
        $temp_name = $data['tmp_name'];
        $path_filename_ext = $ruta . $filename . "." . $ext;
        if (file_exists($path_filename_ext)) {
            throw new HttpException(409, "El nombre del archivo ya existe. ");
        } else {
            move_uploaded_file($temp_name, $path_filename_ext);
            return true;
        }
    }

    private function deleteFile($pathFile) {
        try {
            unlink($pathFile);
        } catch (Exception $e) {
            
        }
        return $pathFile;
    }

    private function deleteAndSaveArchivo($data, $ruta, $publicidad) {
        if (!is_null($publicidad->getRutaArchivo())) {
            //remove el archivo de la carpeta multimedia
            $this->deleteFile($publicidad->getRutaArchivo());
        }
        return $this->saveArchivo($data, $ruta);
        }

        public function getPublicidadFromRequest(&$data, Request $request, int $id = null) :McPublicidad {
        $uploadFile = false;
        $ruta = $this->getPathMultimedia($request);

        $titulo = $request->request->get('titulo');
        $descripcion = $request->request->get('descripcion');
        $duracion = $request->request->get('duracion');
        $urlPublicidad = $request->request->get('url');
        $estado = $request->request->get('estado');
        $tipo = $request->request->get('tipo');
        $estadoPublicidad = explode(":", $estado);
        $tipoPublicidad = explode(":", $tipo);

        if (is_null($id)) {
            $publicidad = new McPublicidad();
            if ($data["name"] != "") {//Se guarda por primera vez un archivo para la publicidad                
                $uploadFile = $this->saveArchivo($data, $ruta);
            }
        } else {
            $publicidad = $this->repository->findOneById($id);
            if ($data["name"] != "") {
                $uploadFile = $this->deleteAndSaveArchivo($data, $ruta, $publicidad);
            }
        }

        $publicidad->setTipoPublicidad((int) $tipoPublicidad[1]);
        $publicidad->setEstado((int) $estadoPublicidad[1]);
        $publicidad->setTituloPublicidad($titulo);
        $publicidad->setDescripcionPublicidad($descripcion);
        $publicidad->setDuracionPublicidad($duracion);
        $publicidad->setUrlPublicidad($urlPublicidad);
        if ($uploadFile) {
            $publicidad->setNombreArchivo($data["name"]);
            $publicidad->setTipoArchivo($data["type"]);
            $publicidad->setRutaArchivo($ruta . $data["name"]);
        }

        return $publicidad;
    }

    public function savePublicidad(McPublicidad $publicidad) {
        $publicidad = $this->repository->savePublicidad($publicidad);
        $publicidad = $this->toarray($publicidad, "publicidad");
        return $publicidad;
    }

    public function updatePublicidad(McPublicidad $publicidad, int $idPublicidad) {
        $publicidad = $this->repository->update($publicidad);
        $publicidad = $this->toarray($publicidad, "publicidad");
        return $publicidad;
    }

}
