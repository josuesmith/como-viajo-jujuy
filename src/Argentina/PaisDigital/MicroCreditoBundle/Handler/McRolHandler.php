<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Handler;

use Argentina\PaisDigital\MicroCreditoBundle\Entity\McRol;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class McRolHandler extends BaseHandler {

    public function __construct(ContainerInterface $container, EntityManagerInterface $entityManager) {
        $this->container = $container;
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(McRol::class);
    }

    public function getAll() {
        return $this->repository->findAll();       
    } 
    
    public function getRolById($id){
        return $this->repository->findOneById($id);
    }
    
    public function getRolByNombre($nombreRol){
         return $this->repository->findOneByNombre($nombreRol);
    }

}
