<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Handler;

use Argentina\PaisDigital\MicroCreditoBundle\Entity\McEmpresa;
use Argentina\PaisDigital\MicroCreditoBundle\Handler\McPersonaHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class McEmpresaHandler extends BaseHandler {

    protected $service;

    public function __construct(ContainerInterface $container, EntityManagerInterface $entityManager, McPersonaHandler $service) {
        $this->container = $container;
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(McEmpresa::class);
        $this->service = $service;
    }

    public function getAll() {
        $empresa = $this->repository->findAll();
        return $this->toarray($empresa, "empresa");
    }

    public function getById($id) {
        return $this->repository->findOneById($id);
    }

    public function searchEmpresas($offset, $limit, $sortField, $sortDirection, $searchParam) {
        $lp = $this->repository->search($offset, $limit, $sortField, $sortDirection, $searchParam);
        $lineaColectivos = $this->toarray($lp->getListado(), 'empresa');
        $lp->setListado($lineaColectivos);
        return $lp;
    }

    /**
     * busca una empresa por nombre nada mas 
     */
    public function search(string $value) {
        $res = $this->repository->searchEmpresa($value);
        $res = $this->toarray($res, "empresa");
        return $res;
        }

        public function getEmpresaColectivoFromRequest(Request $request, int $id = null) :McEmpresa {
        $nombre = $request->request->get('nombre');
        $descripcion = $request->request->get('descripcion');

        if (is_null($id)) {
            $empresa = new McEmpresa();
        } else {
            $empresa = $this->repository->findOneById($id);
        }
        $empresa->setNombre($nombre);
        $empresa->setDescripcion($descripcion);
        return $empresa;
    }

    public function saveEmpresaColectivo(McEmpresa $empresa) {
        $empresa = $this->repository->saveEmpresaColectivo($empresa);
        $empresa = $this->toarray($empresa, "empresa");
        return $empresa;
    }

    public function updateEmpresaColectivo(McEmpresa $empresa, int $idEmpresaColectivo) {
        $empresa = $this->repository->update($empresa);
        $empresa = $this->toarray($empresa, "empresa");
        return $empresa;
    }

}
