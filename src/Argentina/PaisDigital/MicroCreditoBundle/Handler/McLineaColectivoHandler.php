<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Handler;

use Argentina\PaisDigital\MicroCreditoBundle\Entity\McLineaColectivo;
use Argentina\PaisDigital\MicroCreditoBundle\Handler\McEmpresaHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class McLineaColectivoHandler extends BaseHandler {

    public function __construct(ContainerInterface $container, EntityManagerInterface $entityManager, McEmpresaHandler $service) {
        $this->container = $container;
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(McLineaColectivo::class);
        $this->service = $service;
    }

    public function getById($id) {
        return $this->repository->findOneById($id);
    }

    public function getAll() {
        $lineas = $this->repository->findAll();
        $lineas = $this->toarray($lineas, "linea");
        return $lineas;
    }

    public function search($offset, $limit, $sortField, $sortDirection, $searchParam) {
        $lp = $this->repository->search($offset, $limit, $sortField, $sortDirection, $searchParam);
        $lineaColectivos = $this->toarray($lp->getListado(), 'linea');
        $lp->setListado($lineaColectivos);
        return $lp;
        }

        public function getLineaColectivoFromRequest(Request $request, int $id = null):McLineaColectivo {
        $nombre = $request->request->get('nombre');
        $descripcion = $request->request->get('descripcion');
        $empresa = $this->service->getById($request->request->get('empresa'));

        if (is_null($id)) {
            $colectivo = new McLineaColectivo();
        } else {
            $colectivo = $this->repository->findOneById($id);
        }
        $colectivo->setNombre($nombre);
        $colectivo->setDescripcion($descripcion);
        $colectivo->setEmpresa($empresa);
        return $colectivo;
    }

    public function saveLineaColectivo(McLineaColectivo $colectivo) {
        $colectivo = $this->repository->saveLineaColectivo($colectivo);
        $colectivo = $this->toarray($colectivo, "linea");
        return $colectivo;
    }

    public function updateLineaColectivo(McLineaColectivo $colectivo, int $idLineaColectivo) {
        $colectivo = $this->repository->update($colectivo);
        $colectivo = $this->toarray($colectivo, "linea");
        return $colectivo;
    }

    /**
     * busca una linea de colectivo por nombre nada mas 
     */
    public function searchLineaColectivos(string $value) {
        $res = $this->repository->searchLineaColectivo($value);
        $res = $this->toarray($res, "linea");
        return $res;
    }

}
