<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Handler;

use Argentina\PaisDigital\MicroCreditoBundle\Entity\McPersona;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class McPersonaHandler extends BaseHandler {

    public function __construct(ContainerInterface $container, EntityManagerInterface $entityManager) {
        $this->container = $container;
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(McPersona::class);
    }

    public function getByPersona($id) {
        return $this->repository->findOneByPersona($id);
    }

    /**
     * Devuelve una persona por id
     * @param type $id
     * @return type
     */
    public function getPersonaById($id) {
        return $this->repository->findOneById($id);
    }

    public function getPersonaByCuil($cuil) {
        return $this->repository->findOneByCuil($cuil);
    }

    public function getPersonaByDni2($dni) {
        return $this->repository->findOneByDni($dni);
    }

    /**
     * Devuelve un objeto persona por dni
     * @param type $dni
     * @return type
     */
    public function getObjectPersonaByDni($dni) {
        return $this->repository->findOneByDni($dni);
    }

    /**
     * busca una persona por nombre nada mas 
     */
    public function search(string $value) {
        $res = $this->repository->search($value);
        $res = $this->toarray($res, "persona");
        return $res;
    }

    public function getById($id) {
        return $this->repository->findOneById($id);
    }

    public function getPersonaByDni($dni) {
        $persona = $this->repository->findOneByDni($dni);
        $aux = null;
        if (!is_null($persona)) {
            $aux = $this->toarray($persona, "persona");
            $aux["usuario"] = null;            
            if (isset($aux["id"]) && !is_null($aux["id"])) {
                $usuario = $this->container->get("Argentina\PaisDigital\MicroCreditoBundle\Handler\McUsuarioHandler")->getUsuarioByPersona($aux["id"]);
                if (!is_null($usuario)) {
                    $aux["usuario"] = $usuario->getId();
                }
            }
        }
        return $aux;
    }

    public function savePersona($persona) {
        if (is_null($persona["id"])) {
            $p = new McPersona();
            $p->setFechaCreacion(new DateTime());
        } else {
            $p = $this->getById($persona["id"]);
            $p->setFechaActualizacion(new DateTime());
        }
        $p->setDni($persona["dni"]);
        $p->setCuil($persona["cuil"]);
        $p->setNombre($persona["nombre"]);
        $p->setApellido($persona["apellido"]);               
        $p->setEmail($persona["email"]);
        if (isset($persona["fecha_nacimiento"]) && !is_null($persona["fecha_nacimiento"]) && $persona["fecha_nacimiento"] != "") {
            $datetime = new \DateTime($persona["fecha_nacimiento"]);
            $datetime->setTime(12, 00, 00);
            $p->setFechaNacimiento($datetime);
        } else {
            $p->setFechaNacimiento(null);
        }

        return $this->repository->save($p);
    }

}
