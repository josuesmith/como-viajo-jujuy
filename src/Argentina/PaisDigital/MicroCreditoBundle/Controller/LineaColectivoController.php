<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Controller;

use Argentina\PaisDigital\MicroCreditoBundle\Handler\McEmpresaHandler;
use Argentina\PaisDigital\MicroCreditoBundle\Handler\McLineaColectivoHandler;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Codes;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LineaColectivoController extends BaseController {

    /**
     * @Route("/admin/colectivos", name="admin_colectivos_listado")
     * @return type
     */
    public function lineaColectivosListadoAction() {
        $usuario = $this->getUser();
        $this->setTitle("Listado de colectivos | ¿Como Viajo Jujuy?");
        $this->addBreadCrumb("Inicio - Admin", false, "admin_home");
        $this->addBreadCrumb("Linea Colectivos", true);
        $this->data['usuario'] = $usuario;
        $this->data['data'] = null;
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/admin/colectivos/listado.html.twig', $this->data
        );
    }

    /**
     * consulta de linea de colectivos
     *
     * @Route("/admin/ajax/colectivos/search", name="ajax_colectivos_listado", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function searchAction(Request $request) {
        $searchParam = $request->request->all();
        $currentPage = $request->query->get('page');
        $sortField = $request->query->get('sort');
        $sortDirection = $request->query->get('direction');
        $currentPage = null == $currentPage ? 1 : $currentPage;
        $offset = ($currentPage - 1) * $this->getTamanioPagina();
        $limit = $this->getTamanioPagina();
        try {
            /** @var McLineaColectivoHandler $handler */
            $handler = $this->get(McLineaColectivoHandler::class);
            $lp = $handler->search($offset, $limit, $sortField, $sortDirection, $searchParam);
            $lp->setCurrentPage($currentPage);
            $lp->setPageSize($this->getTamanioPagina());
            $this->response->setData($lp);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode(Codes::ERROR);
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

    /**
     * Devuelve una linea de colectivo
     *
     * @Route("/admin/ajax/colectivos/{id}", name="admin_ajax_colectivo_get_by_id", methods={"GET"}, condition="request.isXmlHttpRequest()")
     */
    public function getLineaColectivoByIdAction($id) {
        try {
            $handler = $this->get(McLineaColectivoHandler::class);
            $colectivo = $handler->getById($id);
            $this->response->setData($colectivo);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode(Codes::ERROR);
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

    /**
     * @Route("/admin/colectivos/nuevo", name="admin_linea_nueva")
     * @return type
     */
    public function lineaColectivoNuevoAction() {
        $this->setTitle("Nueva Linea de Colectivos");
        $this->addBreadCrumb("Inicio - Admin", false, "admin_home");
        $this->addBreadCrumb("Colectivos", false, "admin_colectivos_listado");
        $this->addBreadCrumb("Nueva Linea de Colectivo", true);        
        $this->data['data'] = null;        


        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/admin/colectivos/nueva.html.twig', $this->data
        );
    }

    /**
     * @Route("/admin/colectivos/{id}", name="admin_colectivos_modificacion")
     * @return type
     */
    public function lineaColectivoModificacionAction($id) {
        $this->setTitle("Modificar Linea de colectivo");
        $this->addBreadCrumb("Inicio - Admin", false, "admin_home");
        $this->addBreadCrumb("Colectivos", false, "admin_colectivos_listado");
        $this->addBreadCrumb("Modificar Linea de colectivo", true);        
        $this->data['data'] = $id;        
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/admin/colectivos/modificacion.html.twig', $this->data
        );
    }

    /**
     * Guarda una linea de colectivo nueva 
     *
     * @Route("/admin/ajax/colectivos", name="admin_colectivo_save", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function postSaveAction(Request $request) {
        $handler = $this->get(McLineaColectivoHandler::class);
        $colectivo = $handler->getLineaColectivoFromRequest($request);
        $result = $handler->saveLineaColectivo($colectivo);
        $this->response->setData($result);
        $this->response->setCode(Codes::OK);
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

    /**
     * Modifica una linea de colectivo 
     *
     * @Route("/admin/ajax/colectivos/{id}", name="admin_ajax_colectivo_update", methods={"PUT"}, condition="request.isXmlHttpRequest()")
     */
    public function putUpdateAction(Request $request, $id) {
        $handler = $this->get(McLineaColectivoHandler::class);
        $colectivo = $handler->getLineaColectivoFromRequest($request, $id);
        $result = $handler->updateLineaColectivo($colectivo, $id);
        $this->response->setData($result);
        $this->response->setCode(Codes::OK);
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

    /**
     * Busqueda de empresas, metodo get
     *
     * @Route("/admin/ajax/empresas/search", name="ajax_empresas_get_search", methods={"GET"}, condition="request.isXmlHttpRequest()")
     */
    public function getSearchAction(Request $request) {
        try {
            $q = $request->query->get('q');
            $handler = $this->get(McEmpresaHandler::class);
            $result = $handler->search($q);
            $this->response->setData($result);
            $this->response->setCode(Codes::OK);
        } catch (\Exception $e) {
            $this->response->setCode(Codes::ERROR);
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

}
