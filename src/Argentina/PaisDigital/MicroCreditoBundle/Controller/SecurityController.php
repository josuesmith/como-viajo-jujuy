<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Session;
use Argentina\PaisDigital\MicroCreditoBundle\Handler\McPersonaJuridicaHandler;

class SecurityController extends BaseController {

    public function loginAction(AuthenticationUtils $authenticationUtils) {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('@ArgentinaPaisDigitalMicroCredito/auth/login.html.twig', [
                    'last_username' => $lastUsername,
                    'error' => $error,
        ]);
    }

    public function logoutAction() {
        $this->container->get('security.token_storage')->setToken(null);
        return $this->redirect($this->generateUrl('login'));
    }

    public function loginSuccessAction() {
        $session = $this->get("session");
        $user = $this->getUser();
        $isAdmin = $this->isRolAdmin($user->getRoles());
        if ($isAdmin) {
            return $this->redirectToRoute("admin_home");
        }
        $isEmpresa = $this->isRolEmpresaConc($user->getRoles());
        if ($isEmpresa) {
            // para cada session guardamos que empresa es, para despues tenerlo en los servicios
            $persona = $user->getPersona();
            $handler = $this->get(McPersonaJuridicaHandler::class);
            $empresa = $handler->getByPersona($persona->getId());
            $session->set("idEmpresaConc", $empresa->getId());
            $session->set("nombreEmpresa", $empresa->getRazonSocial());
            return $this->redirectToRoute("empresaconc_home");
        }
        $isHacienda = $this->isRolHacienda($user->getRoles());
        if ($isHacienda) {
            return $this->redirectToRoute("hacienda_home");
        }
        $isEmpresaBase = $this->isRolEmpresaBase($user->getRoles());
        if ($isEmpresaBase) {
            $persona = $user->getPersona();
            $handler = $this->get(McPersonaJuridicaHandler::class);
            $empresa = $handler->getByPersona($persona->getId());            
            $session->set("idEmpresaBase", $empresa->getId());
            $session->set("nombreEmpresa", $empresa->getRazonSocial());
            return $this->redirectToRoute("empresabase_home");
        }
    }

}
