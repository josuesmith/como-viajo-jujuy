<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Controller;

use Argentina\PaisDigital\MicroCreditoBundle\Handler\McUsuarioHandler;
use Argentina\PaisDigital\MicroCreditoBundle\Handler\McPersonaHandler;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Codes;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UsuarioController extends BaseController {

    /**
     * @Route("/admin/usuarios", name="admin_usuarios_listado")
     * @return type
     */
    public function usuarioListadoAction() {
        $usuario = $this->getUser();
        $this->setTitle("Listado de usuarios | MicroCrédito");
        $this->addBreadCrumb("Inicio - Admin", false, "admin_home");
        $this->addBreadCrumb("Usuarios", true);
        $this->data['usuario'] = $usuario;
        $this->data['data'] = null;
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/admin/usuarios/listado.html.twig', $this->data
        );
    }

    /**
     * @Route("/admin/usuarios/nuevo", name="admin_usuarios_nuevo")
     * @return type
     */
    public function usuarioNuevoAction() {
        $this->setTitle("Nuevo Usuario");
        $this->addBreadCrumb("Inicio - Admin", false, "admin_home");
        $this->addBreadCrumb("Usuarios", false, "admin_usuarios_listado");
        $this->addBreadCrumb("Nuevo Usuario", true);
        $roles = $this->get(McUsuarioHandler::class)->getRoles();        
        $this->data['data'] = null;
        $this->data['roles'] = $roles;       
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/admin/usuarios/nuevo.html.twig', $this->data
        );
    }

    /**
     * @Route("/admin/usuarios/{id}", name="admin_usuarios_modificacion")
     * @return type
     */
    public function usuarioModificacionAction($id) {
        $this->setTitle("Modificar Usuario");
        $this->addBreadCrumb("Inicio - Admin", false, "admin_home");
        $this->addBreadCrumb("Usuarios", false, "admin_usuarios_listado");
        $this->addBreadCrumb("Modificar Usuario", true);
        $roles = $this->get(McUsuarioHandler::class)->getRoles();        
        $this->data['data'] = $id;
        $this->data['roles'] = $roles;        
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/admin/usuarios/modificacion.html.twig', $this->data
        );
    }

    /**
     * consulta de usuarios
     *
     * @Route("/admin/ajax/usuarios/search", name="ajax_usuario_listado", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function searchAction(Request $request) {
        $searchParam = $request->request->all();
        $currentPage = $request->query->get('page');
        $sortField = $request->query->get('sort');
        $sortDirection = $request->query->get('direction');
        $currentPage = null == $currentPage ? 1 : $currentPage;
        $offset = ($currentPage - 1) * $this->getTamanioPagina();
        $limit = $this->getTamanioPagina();
        try {
            /** @var McUsuarioHandler $handler */
            $handler = $this->get(McUsuarioHandler::class);
            $lp = $handler->search($offset, $limit, $sortField, $sortDirection, $searchParam);
            $lp->setCurrentPage($currentPage);
            $lp->setPageSize($this->getTamanioPagina());
            $this->response->setData($lp);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode(Codes::ERROR);
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

    /**
     * consulta de una persona por cuil
     *
     * @Route("/admin/ajax/usuario/persona/dni/{dni}/exists", name="ajax_usuario_persona_dni_exists", methods={"GET"}, condition="request.isXmlHttpRequest()")
     */
    public function personaExistsAction($dni) {
        try {
            $handler = $this->get(McPersonaHandler::class);
            $persona = $handler->getPersonaByDni($dni);
            $this->response->setData($persona);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode(Codes::ERROR);
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

    /**
     * Guarda un usuario y una persona, si la misma no existe en la BD 
     *
     * @Route("/admin/ajax/usuarios", name="admin_usuario_save", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function postSaveAction(Request $request) {
        $data = $request->request->all();        
        try {
            $result = $this->container->get(McUsuarioHandler::class)->save($data, null,$request);
            $this->response->setData($result);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }

        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

    /**
     * Devuelve un usuario
     *
     * @Route("/admin/ajax/usuarios/{id}", name="admin_ajax_usuario_get_by_id", methods={"GET"}, condition="request.isXmlHttpRequest()")
     */
    public function getUsuarioByIdAction($id) {
        try {
            $handler = $this->get(McUsuarioHandler::class);
            $usuario = $handler->getById($id);
            $this->response->setData($usuario);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode(Codes::ERROR);
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

    /**
     * Modifica un usuario 
     *
     * @Route("/admin/ajax/usuarios/{id}", name="admin_ajax_usuario_update", methods={"PUT"}, condition="request.isXmlHttpRequest()")
     */
    public function putUpdateAction(Request $request, $id) {
        $data = $request->request->all(); 
        $currentUser = $this->getUser();
        try {
            $result = $this->container->get(McUsuarioHandler::class)->save($data, $id, $request, $currentUser);                       
            $this->response->setData($result);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

}
