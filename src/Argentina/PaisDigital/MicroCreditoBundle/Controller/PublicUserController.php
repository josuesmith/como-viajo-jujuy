<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Controller;

use Argentina\PaisDigital\MicroCreditoBundle\Handler\McUsuarioHandler;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Codes;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PublicUserController extends BaseController {

    /**
     * @Route("user/public/generar_clave/{token}", name="public_nueva_clave")
     * @return type
     */
    public function generarClaveAction($token) {
        $this->setTitle("Generación de contraseña");
        $band = true;
        $user = $this->get(McUsuarioHandler::class)->getDataPublicUser($token, $band);
        $this->data["data"] = $user;
        $this->response->setData($this->data);
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/public/usuarios/generar_clave.html.twig', $this->data
        );
    }
    
    /**
     * @Route("user/public/recuperar_clave", name="public_recuperar_clave")
     * @return type
     */
    public function recuperarClaveAction() {
        $this->setTitle("Recuperar contraseña");
        $this->data["data"] = null;
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/public/usuarios/recuperar_clave.html.twig', $this->data
        );
    }
    
     /**
     * @Route("user/public/recuperar_clave_update/{token}", name="public_recuperar_clave_update")
     * @return type
     */
    public function recuperarClaveUpdateAction($token) {
        $this->setTitle("Recuperación de contraseña");
        $band = false;
        $user = $this->get(McUsuarioHandler::class)->getDataPublicUser($token, $band);
        $this->data["data"] = $user;
        $this->response->setData($this->data);
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/public/usuarios/generar_clave.html.twig', $this->data
        );
    }
    

    /**
     * Modifica el password de un usuario 
     *
     * @Route("user/public/ajax/usuarios/clave", name="public_ajax_usuario_clave_update", methods={"PUT"}, condition="request.isXmlHttpRequest()")
     */
    public function putClaveUpdateAction(Request $request) {
        $data = $request->request->all();
        //En este caso no hay usuario logueado, porque esta acción se realiza sin ingreso al sistema.
        try {
            $currentPersona = $this->get(McUsuarioHandler::class)->getById($data["id"]);            
            $currentUser = $this->get(McUsuarioHandler::class)->getUsuarioByPersona($currentPersona["persona"]["id"]);            
            $result = $this->container->get(McUsuarioHandler::class)->updateClave($data, $currentUser);
            $this->response->setData($result);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode($e->getCode());
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }    

    /**
     * @Route("user/public/ajax/usuario/recuperar_clave_email", name="public_ajax_usuario_recuperar_clave_email", methods={"POST"}, condition="request.isXmlHttpRequest()")     
     */
    public function recuperarClaveEmailAction(Request $request) {
        $cuil = $request->request->get('cuil');        
        try {
            $result = $this->container->get(McUsuarioHandler::class)->recuperarClaveEmail($request, $cuil);
            $this->response->setData($result);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
               
    }

}
