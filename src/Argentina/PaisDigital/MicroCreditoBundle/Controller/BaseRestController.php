<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Controller;

use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response as resp;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\RestBundle\Controller\Annotations as Rest;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Response;

/**
 * Description of BaseController
 *
 * @author pchacon
 */
class BaseRestController extends \FOS\RestBundle\Controller\FOSRestController {

	protected $municipio;
	protected $data = array();
	private $tamanioPagina = 5;
	protected $serializer = null;
	protected $response = null;

	function __construct() {
		$this->response = new Response();
		$this->response->setData(null);
		$this->response->setCode(200);
		$this->response->setMessage("");
	}

	public function getResponse(string $message = "Operación correctamente realizada", int $code = resp::HTTP_OK, $data = null) {
		return array("data" => $data, "message" => $message, "code" => $code);
	}

	public function getResponseOk($data = null) {
		return array("data" => $data, "message" => "Operacion correcta", "code" => resp::HTTP_OK);
	}

}
