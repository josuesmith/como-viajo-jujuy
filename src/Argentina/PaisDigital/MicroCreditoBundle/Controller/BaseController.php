<?php

/**
 * Created by PhpStorm.
 * User: coco
 * Date: 24/07/19
 * Time: 09:10
 */

namespace Argentina\PaisDigital\MicroCreditoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Constants;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Response;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Pagination;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session;
use JMS\Serializer\SerializationContext;

class BaseController extends Controller {

	private $tamanioPagina = 5;
	protected $serializer = null;
	protected $response = null;
	protected $data = [];
	protected $pagination;
	protected $session;

	function __construct() {
		$this->response = new Response();
		$this->response->setData(null);
		$this->response->setCode(HttpResponse::HTTP_OK);
		$this->response->setMessage("");
		$this->data['title'] = '';
	}

	/**
	 * @return int
	 */
	public function getTamanioPagina() {
		return $this->tamanioPagina;
	}

	/**
	 * @param int $tamanioPagina
	 */
	public function setTamanioPagina(int $tamanioPagina) {
		$this->tamanioPagina = $tamanioPagina;
	}

	public function toJson($object) {
		$this->serializer = SerializerBuilder::create()->build();
		return $this->serializer->serialize($object, 'json');
	}

	public function setTitle(string $title) {
		$this->data['title'] = $title;
	}

	public function isUserLogged() {
		$user = $this->getUser();
		return $user instanceof \Argentina\PaisDigital\MicroCreditoBundle\Entity\McUsuario;
	}

	/**
	 * Devuelve el usuario loggeado, solo los datos necesarios
	 * @return type
	 */
	public function getUserLoggedData() {
		$user = $this->getUser();
		$serializer = SerializerBuilder::create()->build();
		$serializerContext = SerializationContext::create();
		$serializerContext->setGroups(array("userdata"));
		$serializerContext->setSerializeNull(true);
		return $serializer->toArray($user, $serializerContext);
	}

	/**
	 * Retorna verdadero si el rol de la persona logueada es admin
	 * @param type $roles
	 * @return boolean
	 */
	public function isRolAdmin($roles) {
		$isAdmin = false;
		foreach ($roles as $r) {
			if ($r === Constants::ROLE_ADMIN) {
				$isAdmin = true;
			}
		}
		return $isAdmin;
	}

	/**
	 * Retorna verdadero si el rol de la persona logueada es empresa concentradora
	 * @param type $roles
	 * @return boolean
	 */
	public function isRolEmpresaConc($roles) {
		$isEmpresaConc = false;
		foreach ($roles as $r) {
			if ($r === Constants::ROLE_ORGANIZACION_CONC) {
				$isEmpresaConc = true;
			}
		}
		return $isEmpresaConc;
	}

	/**
	 * Retorna verdadero si el rol de la persona logueada es empresa base
	 * @param type $roles
	 * @return boolean
	 */
	public function isRolEmpresaBase($roles) {
		$isEmpresaBase = false;
		foreach ($roles as $r) {
			if ($r === Constants::ROLE_ORGANIZACION_BASE) {
				$isEmpresaBase = true;
			}
		}
		return $isEmpresaBase;
	}

	/**
	 * Retorna verdadero si el rol de la persona logueada es de hacienda
	 * @param type $roles
	 * @return boolean
	 */
	public function isRolHacienda($roles) {
		$isHacienda = false;
		foreach ($roles as $r) {
			if ($r === Constants::ROLE_HACIENDA) {
				$isHacienda = true;
			}
		}
		return $isHacienda;
	}

	public function isRolUser($roles) {
		$isUser = false;
		foreach ($roles as $r) {
			if ($r === Constants::ROLE_USER) {
				$isUser = true;
			}
		}
		return $isUser;
	}

	/**
	 * Segun los parametros de session guardados cuando se realiza un logueo (login succes) 
	 * devuelve el id de la empresa conc, para el dia de hoy solo puede tener una empresa conc el usuario
	 * @return type
	 */
	public function getIdEmpresaConc() {
		$session = $this->get("session");
		return $session->get("idEmpresaConc", null);
	}

	/**
	 * Segun los parametros de session guardados cuando se realiza un logueo (login succes) 
	 * devuelve el id de la empresa base, para el dia de hoy solo puede tener una empresa conc el usuario
	 * @return type
	 */
	public function getIdEmpresaBase() {
		$session = $this->get("session");
		return $session->get("idEmpresaBase", null);
	}

	/**
	 * Segun los parametros de session guardados cuando se realiza un logueo (login succes) 
	 * devuelve el nombre de la empresa
	 * @return type
	 */
	public function getNombreEmpresa() {
		$session = $this->get("session");
		return $session->get("nombreEmpresa", null);
	}

	public function setBreadCrumb(array $breadcrumbs) {
		$this->data['breadcrumbs'] = $breadcrumbs;
	}

	/**
	 * Agrega breadcrumbs a las vistas
	 * @param string $label label a mostrarse en el breadcrubs
	 * @param bool $active si estara activo, en caso de false corresponde enlace
	 * @param string $route ruta del enlace en caso de que no este activo
	 * @param array $params si la ruta requiere parametros, se lo debe pasar en esete parametro
	 */
	public function addBreadCrumb(string $label, bool $active = false, string $route = "", array $params = array()) {
		if (!isset($this->data['breadcrumbs']) || $this->data['breadcrumbs'] == null || !is_array($this->data['breadcrumbs'])) {
			$this->data['breadcrumbs'] = array();
		}
		array_push($this->data['breadcrumbs'], array("label" => $label,
			"active" => $active,
			"route" => $route,
			"params" => $params
		));
	}

	public function empresaConcCheckSecurity(int $idEmpresa) {
		
	}

	function getPagination() {
		return new Pagination();
	}

}
