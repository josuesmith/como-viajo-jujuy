<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Controller;

use Argentina\PaisDigital\MicroCreditoBundle\Handler\McPublicidadHandler;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Codes;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Argentina\PaisDigital\MicroCreditoBundle\Controller\BaseController;

class PublicidadController extends BaseController {

    /**
     * @Route("/admin/publicidades", name="admin_publicidades_listado")
     * @return type
     */
    public function publicidadesListadoAction() {
        $this->setTitle("Listado de Publicidades | ¿Como Viajo Jujuy?");
        $this->addBreadCrumb("Inicio - Admin", false, "admin_home");
        $this->addBreadCrumb("Publicidades", true);
        $this->data['data'] = null;
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/admin/publicidades/listado.html.twig', $this->data
        );
    }

    /**
     * consulta de empresas de colectivos
     *
     * @Route("/admin/ajax/publicidades/search", name="ajax_publicidades_listado", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function searchAction(Request $request) {
        $searchParam = $request->request->all();
        $currentPage = $request->query->get('page');
        $sortField = $request->query->get('sort');
        $sortDirection = $request->query->get('direction');
        $currentPage = null == $currentPage ? 1 : $currentPage;
        $offset = ($currentPage - 1) * $this->getTamanioPagina();
        $limit = $this->getTamanioPagina();
        try {
            $handler = $this->get(McPublicidadHandler::class);
            $lp = $handler->searchPublicidades($offset, $limit, $sortField, $sortDirection, $searchParam);
            $lp->setCurrentPage($currentPage);
            $lp->setPageSize($this->getTamanioPagina());
            $this->response->setData($lp);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode(Codes::ERROR);
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

    /**
     * @Route("/admin/publicidades/nueva", name="admin_publicidad_nueva")
     * @return type
     */
    public function publicidadNuevaAction() {
        $this->setTitle("Nueva Publicidad");
        $this->addBreadCrumb("Inicio - Admin", false, "admin_home");
        $this->addBreadCrumb("Publicidades", false, "admin_publicidades_listado");
        $this->addBreadCrumb("Nueva Publicidad", true);
        $this->data['data'] = null;
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/admin/publicidades/nueva.html.twig', $this->data
        );
    }

    /**
     * Guarda una publicida nueva
     *
     * @Route("/admin/ajax/publicidades", name="admin_publicidad_save", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function postSaveAction(Request $request) {
        $parameters = $_FILES['archivo'];
        try {
            $handler = $this->get(McPublicidadHandler::class);
            $publicidad = $handler->getPublicidadFromRequest($parameters, $request);
            $result = $handler->savePublicidad($publicidad);
            $this->response->setData($result);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

    /**
     * @Route("/admin/publicidades/{id}", name="admin_publicidad_modificacion")
     * @return type
     */
    public function publicidadModificacionAction($id) {
        $this->setTitle("Editar Publicidad");
        $this->addBreadCrumb("Inicio - Admin", false, "admin_home");
        $this->addBreadCrumb("Publicidades", false, "admin_publicidades_listado");
        $this->addBreadCrumb("Editar Publicidad", true);
        $this->data['data'] = $id;
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/admin/publicidades/modificacion.html.twig', $this->data
        );
    }

    /**
     * Devuelve una empresa de colectivo
     *
     * @Route("/admin/ajax/publicidades/{id}", name="admin_ajax_publicidad_get_by_id", methods={"GET"}, condition="request.isXmlHttpRequest()")
     */
    public function getPublicidadByIdAction($id) {
        try {
            $handler = $this->get(McPublicidadHandler::class);
            $publicidad = $handler->getPublicidadById($id);
            $this->response->setData($publicidad);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode(Codes::ERROR);
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

    /**
     * Modifica una publicidad
     *
     * @Route("/admin/ajax/publicidades/edicion/{id}", name="admin_ajax_publicidad_update", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function putUpdateAction(Request $request, $id) {
        $parameters = $_FILES['archivo'];        
        try {
            $handler = $this->get(McPublicidadHandler::class);
            $empresa = $handler->getPublicidadFromRequest($parameters, $request, $id);
            $result = $handler->updatePublicidad($empresa, $id);
            $this->response->setData($result);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

}
