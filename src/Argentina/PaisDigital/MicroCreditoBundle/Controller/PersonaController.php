<?php
namespace Argentina\PaisDigital\MicroCreditoBundle\Controller;

use Argentina\PaisDigital\MicroCreditoBundle\Handler\McPersonaFisicaHandler;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Codes;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Argentina\PaisDigital\MicroCreditoBundle\Controller\BaseController;

class PersonaController extends BaseController {

	/**
	 * Busqueda de personas, metodo get
	 *
	 * @Route("/admin/ajax/personas/search", name="admin_ajax_persona_get_search", methods={"GET"}, condition="request.isXmlHttpRequest()")
	 */
	public function getSearchAction(Request $request) {
		try {
			$q=$request->query->get('q');
			$handler = $this->get(McPersonaFisicaHandler::class);
			$result = $handler->search($q);
			$this->response->setData($result);
			$this->response->setCode(Codes::OK);
		} catch (\Exception $e) {
			$this->response->setCode(Codes::ERROR);
			$this->response->setMessage($e->getMessage());
		}
		$serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
		return new Response($serializedEntity);
	}

}
