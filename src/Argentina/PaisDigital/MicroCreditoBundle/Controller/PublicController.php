<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Controller;

use Argentina\PaisDigital\MicroCreditoBundle\Handler\McParadaColectivoHandler;
use Argentina\PaisDigital\MicroCreditoBundle\Handler\McPublicidadHandler;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Exception;

class PublicController extends BaseRestController {	
        
        /**
	 *
	 * @Rest\Post("public/parada/search")     
	 * 
	 */
	public function paradaSearchAction(Request $request) {		
		$handler = $this->get(McParadaColectivoHandler::class);
		$lp = $handler->searchParadaColectivoCercana($request);
		return new Response($lp);
	}
        
        /**
	 *
	 * @Rest\Post("public/publicidades")     
	 * 
	 */
	public function publicidadesAction() {		
		$handler = $this->get(McPublicidadHandler::class);
		$lp = $handler->getAllPublicidades();                
		return new Response($lp);
	}
              

}
