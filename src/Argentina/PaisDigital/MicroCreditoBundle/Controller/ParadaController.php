<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Controller;

use Argentina\PaisDigital\MicroCreditoBundle\Handler\McLineaColectivoHandler;
use Argentina\PaisDigital\MicroCreditoBundle\Handler\McParadaColectivoHandler;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Codes;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ParadaController extends BaseController {

    /**
     * @Route("/admin/paradas", name="admin_paradas_listado")
     * @return type
     */
    public function paradasListadoAction() {
        $this->setTitle("Listado de Paradas | ¿Como Viajo Jujuy?");
        $this->addBreadCrumb("Inicio - Admin", false, "admin_home");
        $this->addBreadCrumb("Paradas de colectivos", true);
        $this->data['data'] = null;
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/admin/paradas/listado.html.twig', $this->data
        );
    }

    /**
     * consulta de paradas de colectivos
     *
     * @Route("/admin/ajax/paradas/search", name="ajax_paradas_listado", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function searchAction(Request $request) {
        $searchParam = $request->request->all();
        $currentPage = $request->query->get('page');
        $sortField = $request->query->get('sort');
        $sortDirection = $request->query->get('direction');
        $currentPage = null == $currentPage ? 1 : $currentPage;
        $offset = ($currentPage - 1) * $this->getTamanioPagina();
        $limit = $this->getTamanioPagina();
        try {
            /** @var McParadaColectivoHandler $handler */
            $handler = $this->get(McParadaColectivoHandler::class);
            $lp = $handler->search($offset, $limit, $sortField, $sortDirection, $searchParam);
            $lp->setCurrentPage($currentPage);
            $lp->setPageSize($this->getTamanioPagina());
            $this->response->setData($lp);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode(Codes::ERROR);
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

    /**
     * @Route("/admin/paradas/nueva", name="admin_parada_nueva")
     * @return type
     */
    public function usuarioNuevoAction() {
        $this->setTitle("Nuevo Parada");
        $this->addBreadCrumb("Inicio - Admin", false, "admin_home");
        $this->addBreadCrumb("Paradas", false, "admin_paradas_listado");
        $this->addBreadCrumb("Nuevo Parada", true);        
        $this->data['data'] = null;        
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/admin/paradas/nueva.html.twig', $this->data
        );
    }

    /**
     * Guarda una parada de colectivo nueva 
     *
     * @Route("/admin/ajax/paradas", name="admin_parada_save", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function postSaveAction(Request $request) {
        $handler = $this->get(McParadaColectivoHandler::class);
        $parada = $handler->getParadaColectivoFromRequest($request);
        $result = $handler->saveParadaColectivo($parada);
        $this->response->setData($result);
        $this->response->setCode(Codes::OK);
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

    /**
     * @Route("/admin/paradas/{id}", name="admin_paradas_modificacion")
     * @return type
     */
    public function paradaColectivoModificacionAction($id) {
        $this->setTitle("Modificar Parada de colectivo");
        $this->addBreadCrumb("Inicio - Admin", false, "admin_home");
        $this->addBreadCrumb("Paradas", false, "admin_paradas_listado");
        $this->addBreadCrumb("Modificar Parada de colectivo", true);        
        $this->data['data'] = $id;        
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/admin/paradas/modificacion.html.twig', $this->data
        );
    }

    /**
     * Devuelve una parada de colectivo
     *
     * @Route("/admin/ajax/paradas/{id}", name="admin_ajax_parada_get_by_id", methods={"GET"}, condition="request.isXmlHttpRequest()")
     */
    public function getParadaColectivoByIdAction($id) {
        try {
            $handler = $this->get(McParadaColectivoHandler::class);
            $parada = $handler->getById($id);
            $this->response->setData($parada);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode(Codes::ERROR);
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

    /**
     * Modifica una parada de colectivo 
     *
     * @Route("/admin/ajax/paradas/{id}", name="admin_ajax_parada_update", methods={"PUT"}, condition="request.isXmlHttpRequest()")
     */
    public function putUpdateAction(Request $request, $id) {
        $handler = $this->get(McParadaColectivoHandler::class);
        $parada = $handler->getParadaColectivoFromRequest($request, $id);
        $result = $handler->updateParadaColectivo($parada, $id);
        $this->response->setData($result);
        $this->response->setCode(Codes::OK);
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }
    
    /**
     * Busqueda de lineas de colectivo, metodo get
     *
     * @Route("/admin/ajax/lineas/search", name="ajax_lineas_get_search", methods={"GET"}, condition="request.isXmlHttpRequest()")
     */
    public function getSearchAction(Request $request) {
        try {
            $q = $request->query->get('q');
            $handler = $this->get(McLineaColectivoHandler::class);
            $result = $handler->searchLineaColectivos($q);                     
            $this->response->setData($result);
            $this->response->setCode(Codes::OK);
        } catch (\Exception $e) {
            $this->response->setCode(Codes::ERROR);
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

}
