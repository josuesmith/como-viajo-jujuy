<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Argentina\PaisDigital\MicroCreditoBundle\Handler\McUsuarioHandler;
use Argentina\PaisDigital\MicroCreditoBundle\Handler\McBusquedaLogHandler;
use Argentina\PaisDigital\MicroCreditoBundle\Handler\McPersonaJuridicaHandler;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Codes;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends BaseController {

    /**
     * @Route("/admin/inicio", name="admin_home")
     * @return type
     */
    public function adminHomeAction() {
        $this->setTitle("Inicio | ¿Cómo viajo?");
        $this->addBreadCrumb("Inicio - Admin", true);
        $this->data['usuario'] = array("id" => 99999
        );
        $response = $this->render("@ArgentinaPaisDigitalMicroCredito/home.html.twig", $this->data);
        return $response;
    }

    /**
     * @Route("/mis-datos", name="admin_my")
     * @return type
     */
    public function myAction() {
        $this->setTitle("Perfil | ¿Cómo viajo?");
        $this->addBreadCrumb("Inicio - Admin", false, "admin_home");
        $this->addBreadCrumb("Perfil", true);
        // traemos la empresa para devolver en la misma llamada
        $usuario = $this->getUserLoggedData();
        $this->data['data'] = array(
            "usuario" => $usuario
        );
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/Default/usuarios/my.html.twig', $this->data
        );
    }

    /**
     * Modifica la contraseña del usuario administrador
     * @Route("/modificar-mis-datos-clave", name="admin_admin_modificacion_misdatos_clave")
     * @return type
     */
    public function adminModificacionMisDatosClaveAction() {
        $this->setTitle("Modificación conraseña");
        $this->addBreadCrumb("Inicio - Admin", false, "admin_home");
        $this->addBreadCrumb("Perfil", false, "admin_my");
        $this->addBreadCrumb("Modificar contraseña", true);
        $usuario = $this->getUser();
        $this->data["usuario"] = $usuario;
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/Default/usuarios/modificacioncontraseña.html.twig', $this->data
        );
    }

    /**
     * Modifica el password de un usuario 
     *
     * @Route("admin/ajax/usuario-clave", name="admin_ajax_usuario_clave", methods={"PUT"}, condition="request.isXmlHttpRequest()")
     */
    public function putClaveUpdateAction(Request $request) {
        $data = $request->request->all();        
        try {
            $result = $this->container->get(McUsuarioHandler::class)->updateClave($data);
            $this->response->setData($result);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode($e->getStatusCode());
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

}
