<?php 

namespace Argentina\PaisDigital\MicroCreditoBundle\Controller;

use Argentina\PaisDigital\MicroCreditoBundle\Handler\McEmpresaHandler;
use Argentina\PaisDigital\MicroCreditoBundle\Utils\Codes;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmpresaController extends BaseController {

    /**
     * @Route("/admin/empresas", name="admin_empresas_listado")
     * @return type
     */
    public function empresasListadoAction() {
        $this->setTitle("Listado de Empresas | ¿Como Viajo Jujuy?");
        $this->addBreadCrumb("Inicio - Admin", false, "admin_home");
        $this->addBreadCrumb("Empresas", true);
        $this->data['data'] = null;
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/admin/empresas/listado.html.twig', $this->data
        );
    }

    /**
     * consulta de empresas de colectivos
     *
     * @Route("/admin/ajax/empresas/search", name="ajax_empresas_listado", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function searchAction(Request $request) {
        $searchParam = $request->request->all();
        $currentPage = $request->query->get('page');
        $sortField = $request->query->get('sort');
        $sortDirection = $request->query->get('direction');
        $currentPage = null == $currentPage ? 1 : $currentPage;
        $offset = ($currentPage - 1) * $this->getTamanioPagina();
        $limit = $this->getTamanioPagina();
        try {
            /** @var McEmpresaHandler $handler */
            $handler = $this->get(McEmpresaHandler::class);
            $lp = $handler->searchEmpresas($offset, $limit, $sortField, $sortDirection, $searchParam);
            $lp->setCurrentPage($currentPage);
            $lp->setPageSize($this->getTamanioPagina());
            $this->response->setData($lp);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode(Codes::ERROR);
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

    /**
     * @Route("/admin/empresas/nueva", name="admin_empresa_nueva")
     * @return type
     */
    public function empresaNuevaAction() {
        $this->setTitle("Nuevo Empresa");
        $this->addBreadCrumb("Inicio - Admin", false, "admin_home");
        $this->addBreadCrumb("Empresas", false, "admin_empresas_listado");
        $this->addBreadCrumb("Nuevo Empresa", true);        
        $this->data['data'] = null;        
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/admin/empresas/nueva.html.twig', $this->data
        );
    }

    /**
     * Guarda una empresa de colectivo nueva 
     *
     * @Route("/admin/ajax/empresas", name="admin_empresa_save", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function postSaveAction(Request $request) {
        $handler = $this->get(McEmpresaHandler::class);
        $empresa = $handler->getEmpresaColectivoFromRequest($request);
        $result = $handler->saveEmpresaColectivo($empresa);
        $this->response->setData($result);
        $this->response->setCode(Codes::OK);
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

    /**
     * @Route("/admin/empresas/{id}", name="admin_empresas_modificacion")
     * @return type
     */
    public function empresaColectivoModificacionAction($id) {
        $this->setTitle("Modificar Empresa de colectivo");
        $this->addBreadCrumb("Inicio - Admin", false, "admin_home");
        $this->addBreadCrumb("Empresas", false, "admin_empresas_listado");
        $this->addBreadCrumb("Modificar Empresa de colectivo", true);        
        $this->data['data'] = $id;        
        return $this->render(
                        '@ArgentinaPaisDigitalMicroCredito/admin/empresas/modificacion.html.twig', $this->data
        );
    }

    /**
     * Devuelve una empresa de colectivo
     *
     * @Route("/admin/ajax/empresas/{id}", name="admin_ajax_empresa_get_by_id", methods={"GET"}, condition="request.isXmlHttpRequest()")
     */
    public function getEmpresaColectivoByIdAction($id) {
        try {
            $handler = $this->get(McEmpresaHandler::class);
            $empresa = $handler->getById($id);
            $this->response->setData($empresa);
            $this->response->setCode(Codes::OK);
        } catch (Exception $e) {
            $this->response->setCode(Codes::ERROR);
            $this->response->setMessage($e->getMessage());
        }
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }

    /**
     * Modifica una parada de colectivo 
     *
     * @Route("/admin/ajax/empresas/{id}", name="admin_ajax_empresa_update", methods={"PUT"}, condition="request.isXmlHttpRequest()")
     */
    public function putUpdateAction(Request $request, $id) {
        $handler = $this->get(McEmpresaHandler::class);
        $empresa = $handler->getEmpresaColectivoFromRequest($request, $id);
        $result = $handler->updateEmpresaColectivo($empresa, $id);
        $this->response->setData($result);
        $this->response->setCode(Codes::OK);
        $serializedEntity = $this->container->get('serializer')->serialize($this->response, 'json');
        return new Response($serializedEntity);
    }        

}

