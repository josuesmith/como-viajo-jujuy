<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Pagination;

class ListadoPaginar {

    private $listado;

    private $currentPage;
    private $pageSize;
    private $totalResults;
    private $sortField;
    private $sortDirection;

    function __construct(array $results = null, $numElementos = null) {
        $this->setListado($results);
        $this->setTotalResults($numElementos);
    }

    public function getCurrentPage() {
        return $this->currentPage;
    }

    public function setCurrentPage($currentPage) {
        $this->currentPage = $currentPage;
        return $this;
    }

    public function getPageSize() {
        return $this->pageSize;
    }

    public function setPageSize($pageSize) {
        $this->pageSize = $pageSize;
        return $this;
    }

    public function getTotalResults() {
        return $this->totalResults;
    }

    public function setTotalResults($totalResults) {
        $this->totalResults = null === $totalResults ? null : (int) $totalResults;
        return $this;
    }

    public function getSortField() {
        return $this->sortField;
    }

    public function setSortField($sortField) {
        $this->sortField = $sortField;
        return $this;
    }

    public function getSortDirection() {
        return $this->sortDirection;
    }

    public function setSortDirection($sortDirection) {
        $this->sortDirection = $sortDirection;
        return $this;
    }

    public function getListado() {
        return $this->listado;
    }

    public function setListado($listado) {
        $this->listado = $listado;
        return $this;
    }
}