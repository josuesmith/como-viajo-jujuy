<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Dto;

use JMS\Serializer\Annotation\Type;

class ReclamoRequestDto {

	/**
	 * @Type("string")
	 */
	private $jwt;

	/**
	 * @Type("Argentina\PaisDigital\MicroCreditoBundle\Dto\ReclamoDto")
	 */
	private $reclamo;

	function getJwt() {
		return $this->jwt;
	}

	function getReclamo(): ReclamoDto {
		return $this->reclamo;
	}

	function setJwt($jwt) {
		$this->jwt = $jwt;
	}

	function setReclamo(ReclamoDto $reclamo) {
		$this->reclamo = $reclamo;
	}

}
