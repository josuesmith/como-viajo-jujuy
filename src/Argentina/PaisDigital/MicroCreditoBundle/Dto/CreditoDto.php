<?php
namespace Argentina\PaisDigital\MicroCreditoBundle\Dto;

use Argentina\PaisDigital\MicroCreditoBundle\Utils\Constants;

class CreditoDto{
    private $data;
    private $dataFormat;
    private $dataTruncate;
    private $mensaje;
    private $mensajeDetalle;
    private $codigo;
    private $codigoDetalle;
    private $esDetalle;
   
    function getData() {
        return $this->data;
    }

    function getMensaje() {
        return $this->mensaje;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setMensaje($mensaje) {
        $this->mensaje = $mensaje;
    }
    
    function getCodigo() {
        return $this->codigo;
    }

    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    function getDataFormat() {
        
        return $this->dataFormat;
    }

    function setDataFormat($dataFormat) {
        $this->dataFormat = $dataFormat;
    }
    
    function getDataTruncate() {
        return $this->dataTruncate;
    }

    function getEsDetalle() {
        return $this->esDetalle;
    }

    function setDataTruncate($dataTruncate) {
        $this->dataTruncate = $dataTruncate;
    }

    function setEsDetalle($data) {
        $esDetalle = $data[Constants::REGISTRO_CSV[Constants::CSV_ES_DETALLE]];
        if($esDetalle == 1){
            $this->esDetalle = 1;
        }else{
            $this->esDetalle = 0;
        }
    }
    
    function getCodigoDetalle() {
        return $this->codigoDetalle;
    }

    function setCodigoDetalle($codigoDetalle) {
        $this->codigoDetalle = $codigoDetalle;
    }
    
    function getMensajeDetalle() {
        return $this->mensajeDetalle;
    }

    function setMensajeDetalle($mensajeDetalle) {
        $this->mensajeDetalle = $mensajeDetalle;
    }
}