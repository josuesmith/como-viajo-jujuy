<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Dto;

//use Symfony\Component\Validator\Constraints\Type;
use JMS\Serializer\Annotation\Type;

class ReclamoDto {

	/**
	 * @Type("string")
	 */
	private $email;

	/**
	 * @Type("string")
	 */
	private $codArea;

	/**
	 * @Type("string")
	 */
	private $numero;

	/**
	 * @Type("integer")
	 */
	private $tipo;

	/**
	 * @Type("string")
	 */
	private $descripcion;

	/**
	 * @Type("array")
	 */
	private $id;

	function getEmail() {
		return $this->email;
	}

	function getCodArea() {
		return $this->codArea;
	}

	function getNumero() {
		return $this->numero;
	}

	function getTipo() {
		return $this->tipo;
	}

	function setEmail($email) {
		$this->email = $email;
	}

	function setCodArea($codArea) {
		$this->codArea = $codArea;
	}

	function setNumero($numero) {
		$this->numero = $numero;
	}

	function setTipo($tipo) {
		$this->tipo = $tipo;
	}

	function getDescripcion() {
		return $this->descripcion;
	}

	function setDescripcion($descripcion) {
		$this->descripcion = $descripcion;
	}

	function getId() {
		return $this->id;
	}

	function setId($id) {
		$this->id = $id;
	}

}
