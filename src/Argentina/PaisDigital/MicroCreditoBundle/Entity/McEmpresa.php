<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * McEmpresa
 *
 * @ORM\Table(name="mc_empresas")
 * @ORM\Entity(repositoryClass="Argentina\PaisDigital\MicroCreditoBundle\Repository\McEmpresaRepository")
 */
class McEmpresa {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * 
     * @Groups({"parada", "linea", "empresa"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="string", length=45, nullable=false)
     * @Groups({"parada", "linea", "empresa"})
     */
    private $nombre;
    
    /**
     * @var string
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=false)
     * @Groups({"parada", "linea", "empresa"})
     */
    private $descripcion;
   

    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getDescripcion() {
        return $this->descripcion;
    }   

    function setId($id) {
        $this->id = $id;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }  
}
