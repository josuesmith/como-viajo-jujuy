<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * TdArchivoCsv
 *
 * @ORM\Table(name="mc_archivos_csv")
 * @ORM\Entity(repositoryClass="Argentina\PaisDigital\MicroCreditoBundle\Repository\McArchivoCsvRepository")
 */
class McArchivoCsv
{
    /**
     * @var int
     * @Groups({"log", "csv"})
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Groups({"log", "csv"})
     * @ORM\Column(name="nombre", type="string", length=100, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @Groups({"log", "csv"})
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;

    /**
     * @var string|null
     *
     * @Groups({"log", "csv"})
     * @ORM\Column(name="tamanio", type="string", length=45, nullable=true)
     */
    private $tamanio;

    /**
     * @var string|null
     * @Groups({"log", "csv"})
     * @ORM\Column(name="path", type="string", length=100, nullable=true)
     */
    private $path;

    /**
     * @var bool|null
     * @Groups({"log", "csv"})
     * @ORM\Column(name="procesado", type="boolean", nullable=true)
     */
    private $procesado;

    /**
     * @var int|null
     * @Groups({"log", "csv"})
     * @ORM\Column(name="cantidad_registros", type="integer", nullable=true)
     */
    private $cantidadRegistros;

    /**
     * @var \DateTime
     * @Groups({"log", "csv"})
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaCreacion;

    /**
     * @var \TdUsuario
     *
     * @Groups({"log", "csv"})
     * @ORM\ManyToOne(targetEntity="McUsuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_creacion", referencedColumnName="id", nullable=false)
     * })
     */
    private $usuarioCreacion;
    
    /**
     * @var \DateTime
     * @Groups({"log", "csv"})
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaActualizacion;




    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;
    }



    /**
     * Set descripcion.
     *
     * @param string|null $descripcion
     *
     * @return TdArchivoCsv
     */
    public function setDescripcion($descripcion = null)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string|null
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set tamanio.
     *
     * @param string|null $tamanio
     *
     * @return TdArchivoCsv
     */
    public function setTamanio($tamanio = null)
    {
        $this->tamanio = $tamanio;

        return $this;
    }

    /**
     * Get tamanio.
     *
     * @return string|null
     */
    public function getTamanio()
    {
        return $this->tamanio;
    }

    /**
     * Set path.
     *
     * @param string|null $path
     *
     * @return TdArchivoCsv
     */
    public function setPath($path = null)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path.
     *
     * @return string|null
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set procesado.
     *
     * @param bool|null $procesado
     *
     * @return TdArchivoCsv
     */
    public function setProcesado($procesado = null)
    {
        $this->procesado = $procesado;

        return $this;
    }

    /**
     * Get procesado.
     *
     * @return bool|null
     */
    public function getProcesado()
    {
        return $this->procesado;
    }

    /**
     * Set cantidadRegistros.
     *
     * @param int|null $cantidadRegistros
     *
     * @return TdArchivoCsv
     */
    public function setCantidadRegistros($cantidadRegistros = null)
    {
        $this->cantidadRegistros = $cantidadRegistros;

        return $this;
    }

    /**
     * Get cantidadRegistros.
     *
     * @return int|null
     */
    public function getCantidadRegistros()
    {
        return $this->cantidadRegistros;
    }

    /**
     * Set fechaCreacion.
     *
     * @param \DateTime $fechaCreacion
     *
     * @return TdArchivoCsv
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion.
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set usuarioCreacion.
     *
     * @param \Argentina\PaisDigital\MicroCreditoBundle\Entity\McUsuario|null $usuarioCreacion
     *
     * @return TdArchivoCsv
     */
    public function setUsuarioCreacion(\Argentina\PaisDigital\MicroCreditoBundle\Entity\McUsuario $usuarioCreacion = null)
    {
        $this->usuarioCreacion = $usuarioCreacion;

        return $this;
    }

    /**
     * Get usuarioCreacion.
     *
     * @return \Argentina\PaisDigital\MicroCreditoBundle\Entity\McUsuario|null
     */
    public function getUsuarioCreacion()
    {
        return $this->usuarioCreacion;
    }
    
    function getFechaActualizacion(){
        return $this->fechaActualizacion;
    }

    function setFechaActualizacion( $fechaActualizacion) {
        $this->fechaActualizacion = $fechaActualizacion;
    }
}
