<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\Annotation\Groups;

/**
 * McUsuarios
 *
 * @ORM\Table(name="mc_usuarios", indexes={@ORM\Index(name="fk_mc_usuarios_1_idx", columns={"persona_id"})})
 * @ORM\Entity(repositoryClass="Argentina\PaisDigital\MicroCreditoBundle\Repository\McUsuarioRepository")
 */
class McUsuario implements UserInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"user", "userdata", "busqueda", "userLog"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario", type="string", length=100, nullable=false)
     * 
     * @Groups({"user", "userdata", "busqueda", "userLog", "csv"})
     */
    private $usuario;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=100, nullable=false)
     * 
     * @Groups({"persona"})
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=150, nullable=true)
     * 
     * @Groups({"persona"})
     */
    private $salt = null;

    /**
     * @var \McPersona
     *
     * @ORM\ManyToOne(targetEntity="McPersona")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_persona", referencedColumnName="id")
     * })     
     * @Groups({"user", "userdata"})
     */
    private $persona;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="McRol", inversedBy="usuario")
     * @ORM\JoinTable(name="mc_usuarios_roles",
     *   joinColumns={
     *     @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="rol_id", referencedColumnName="id")
     *   }
     * )
     * @Groups({"user", "busqueda", "userLog"})
     */
    private $rol;

    /**
     * @var bool
     * @Groups({"user", "userdata", "userLog"})
     * @ORM\Column(name="estado", type="boolean", nullable=true, options={"comment"="Determina si un usuario ingreso por primeravez o no, 0:NO, 1:SI"})
     */
    private $estado;
   

    /**
     * @var \DateTime
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     * 
     * @Groups({"user", "userdata"})
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $fechaActualizacion;
    
    private $username;

    /**
     * Constructor
     */
    public function __construct() {
        $this->rol = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     *
     * @return McUsuario
     */
    public function setUsuario($usuario) {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string
     */
    public function getUsuario() {
        return $this->usuario;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return McUsuario
     */
    public function setPassword($password) {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return McUsuario
     */
    public function setSalt($salt) {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt() {
        return $this->salt;
    }

    /**
     * Set persona
     *
     * @param \Argentina\PaisDigital\MicroCreditoBundle\Entity\McPersonaFisica $persona
     *
     * @return McUsuario
     */
    public function setPersona(\Argentina\PaisDigital\MicroCreditoBundle\Entity\McPersona $persona = null) {
        $this->persona = $persona;

        return $this;
    }

    /**
     * Get persona
     *
     * @return \Argentina\PaisDigital\MicroCreditoBundle\Entity\McPersona
     */
    public function getPersona() {
        return $this->persona;
    }

    /**
     * Add rol
     *
     * @param \Argentina\PaisDigital\MicroCreditoBundle\Entity\McRol $rol
     *
     * @return McUsuario
     */
    public function addRol(\Argentina\PaisDigital\MicroCreditoBundle\Entity\McRol $rol) {
        $this->rol[] = $rol;

        return $this;
    }

    /**
     * Remove rol
     *
     * @param \Argentina\PaisDigital\MicroCreditoBundle\Entity\McRol $rol
     */
    public function removeRol(\Argentina\PaisDigital\MicroCreditoBundle\Entity\McRol $rol) {
        $this->rol->removeElement($rol);
    }

    /**
     * Get rol
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRol() {
        return $this->rol;
    }
    
    /**
     * Set estado
     *
     * @param boolean $estado
     *
     * @return McUsuario
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set fechaCreacion.
     *
     * @param \DateTime $fechaCreacion
     *
     * @return McUsuario
     */
    public function setFechaCreacion($fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion.
     *
     * @return \DateTime
     */
    public function getFechaCreacion() {
        return $this->fechaCreacion;
    }

    /**
     * @return \DateTime
     */
    public function getFechaActualizacion() {
        return $this->fechaActualizacion;
    }

    /**
     * @param \DateTime $fechaActualizacion
     */
    public function setFechaActualizacion($fechaActualizacion) {
        $this->fechaActualizacion = $fechaActualizacion;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getRoles() {
        $roles = array();
        foreach ($this->rol as $role) {
            $roles[] = $role->getRole();
        }
        return $roles;
    }

    public function eraseCredentials() {
        
    }

}
