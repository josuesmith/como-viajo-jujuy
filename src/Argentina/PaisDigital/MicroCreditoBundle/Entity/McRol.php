<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

/**
 * McRoles
 *
 * @ORM\Table(name="mc_roles")
 * @ORM\Entity(repositoryClass="Argentina\PaisDigital\MicroCreditoBundle\Repository\McRolRepository")
 */
class McRol  implements RoleHierarchyInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * 
     * @Groups({"user", "busqueda", "userLog"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=45, nullable=false)
     * @Groups({"user", "busqueda", "userLog"})
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     * @Groups({"user", "userLog"})
     */
    private $descripcion = null;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="McUsuario", mappedBy="rol")
     * 
     * @Groups({"user"})
     */
    private $usuario;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->usuario = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return McRol
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return McRol
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Add usuario
     *
     * @param \Argentina\PaisDigital\MicroCreditoBundle\Entity\McUsuario $usuario
     *
     * @return McRol
     */
    public function addUsuario(\Argentina\PaisDigital\MicroCreditoBundle\Entity\McUsuario $usuario)
    {
        $this->usuario[] = $usuario;

        return $this;
    }

    /**
     * Remove usuario
     *
     * @param \Argentina\PaisDigital\MicroCreditoBundle\Entity\McUsuario $usuario
     */
    public function removeUsuario(\Argentina\PaisDigital\MicroCreditoBundle\Entity\McUsuario $usuario)
    {
        $this->usuario->removeElement($usuario);
    }

    /**
     * Get usuario
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    public function getRole() {
        return $this->getNombre();
    }

    public function getReachableRoles(array $roles)
    {
        // TODO: Implement getReachableRoles() method.
    }
}
