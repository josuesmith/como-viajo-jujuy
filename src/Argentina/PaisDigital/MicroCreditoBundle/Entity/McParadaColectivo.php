<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * McUsuarioLog
 *
 * @ORM\Table(name="mc_paradas_colectivos")
 * @ORM\Entity(repositoryClass="Argentina\PaisDigital\MicroCreditoBundle\Repository\McParadaColectivoRepository")
 */
class McParadaColectivo {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * 
     * @Groups({"parada", "recorrido"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="string", length=45, nullable=false)
     * @Groups({"parada", "recorrido"})
     */
    private $nombre;

    /**
     * @var \McLineaColectivo
     * @Groups({"parada"})
     * @ORM\ManyToOne(targetEntity="McLineaColectivo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_linea", referencedColumnName="id")
     * })
     */
    private $linea;

    /**
     * @var string
     *
     * @ORM\Column(name="latitud", type="string", length=45, nullable=false)
     * 
     * @Groups({"parada", "recorrido"})
     */
    private $latitud;

    /**
     * @var string
     *
     * @ORM\Column(name="longitud", type="string", length=45, nullable=false)
     * 
     * @Groups({"parada", "recorrido"})
     */
    private $longitud;   
    

    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }
    
    function getLinea(){
        return $this->linea;
    }

    function getLatitud() {
        return $this->latitud;
    }

    function getLongitud() {
        return $this->longitud;
    }       

    function setId($id) {
        $this->id = $id;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }
    
    function setLinea($linea){
        $this->linea = $linea;
    }

    function setLatitud($latitud) {
        $this->latitud = $latitud;
    }

    function setLongitud($longitud) {
        $this->longitud = $longitud;
    }       

}
