<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * McPersona
 *
 * @ORM\Table(name="mc_personas")
 * @ORM\Entity(repositoryClass="Argentina\PaisDigital\MicroCreditoBundle\Repository\McPersonaRepository")
 */
class McPersona {

	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="bigint", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 * 
	 * @Groups({"credito", "persona", "user", "organizacion", "log", "personaLog", "csv"})
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="apellido", type="string", length=255, nullable=true)
	 * 
	 * @Groups({"credito", "persona", "user", "organizacion", "empresa", "userdata", "personaLog", "listReclamo", "reclamo"})
	 */
	private $apellido;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
	 * 
	 * @Groups({"credito", "persona", "user", "organizacion", "empresa", "userdata", "personaLog", "listReclamo", "reclamo"})
	 */
	private $nombre;	

	/**
	 * @var string
	 *
	 * @ORM\Column(name="dni", type="string", length=45, nullable=false)
	 * 
	 * @Groups({"credito", "persona", "user", "organizacion", "userdata", "personaLog", "listReclamo", "reclamo"})
	 */
	private $dni;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cuil", type="string", length=45, nullable=true)
	 * 
	 * @Groups({"credito", "persona", "user", "organizacion", "userdata", "personaLog", "listReclamo", "reclamo"})
	 */
	private $cuil = null;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="fecha_nacimiento", type="datetime", nullable=true)
	 * 
	 * @Groups({"credito", "persona", "user", "userdata", "personaLog"})
	 */
	private $fechaNacimiento = null;	
		

	/**
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", length=255, nullable=true)
	 * 
	 * @Groups({"credito", "persona", "user", "userdata", "personaLog"})
	 */
	private $email = null;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
	 * 
	 * @Groups({"user"})
	 */
	private $fechaCreacion;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
	 */
	private $fechaActualizacion;		

	/**
	 * Set apellido
	 *
	 * @param string $apellido
	 *
	 * @return McPersona
	 */
	public function setApellido($apellido) {
		$this->apellido = $apellido;

		return $this;
	}

	/**
	 * Get apellido
	 *
	 * @return string
	 */
	public function getApellido() {
		return $this->apellido;
	}

	/**
	 * Set nombre
	 *
	 * @param string $nombre
	 *
	 * @return McPersona
	 */
	public function setNombre($nombre) {
		$this->nombre = $nombre;

		return $this;
	}

	/**
	 * Get nombre
	 *
	 * @return string
	 */
	public function getNombre() {
		return $this->nombre;
	}

	/**
	 * Set dni
	 *
	 * @param string $dni
	 *
	 * @return McPersona
	 */
	public function setDni($dni) {
		$this->dni = $dni;

		return $this;
	}

	/**
	 * Get dni
	 *
	 * @return string
	 */
	public function getDni() {
		return $this->dni;
	}

	/**
	 * Set cuil
	 *
	 * @param string $cuil
	 *
	 * @return McPersona
	 */
	public function setCuil($cuil) {
		$this->cuil = $cuil;

		return $this;
	}

	/**
	 * Get cuil
	 *
	 * @return string
	 */
	public function getCuil() {
		return $this->cuil;
	}

	/**
	 * Set fechaNacimiento
	 *
	 * @param \DateTime $fechaNacimiento
	 *
	 * @return McPersona
	 */
	public function setFechaNacimiento($fechaNacimiento) {
		$this->fechaNacimiento = $fechaNacimiento;

		return $this;
	}

	/**
	 * Get fechaNacimiento
	 *
	 * @return \DateTime
	 */
	public function getFechaNacimiento() {
		return $this->fechaNacimiento;
	}	 	

	/**
	 * Set email
	 *
	 * @param string $email
	 *
	 * @return McPersona
	 */
	public function setEmail($email) {
		$this->email = $email;

		return $this;
	}

	/**
	 * Get email
	 *
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Set fechaCreacion.
	 *
	 * @param \DateTime $fechaCreacion
	 *
	 * @return McPersona
	 */
	public function setFechaCreacion($fechaCreacion) {
		$this->fechaCreacion = $fechaCreacion;

		return $this;
	}

	/**
	 * Get fechaCreacion.
	 *
	 * @return \DateTime
	 */
	public function getFechaCreacion() {
		return $this->fechaCreacion;
	}

	/**
	 * @return \DateTime
	 */
	public function getFechaActualizacion() {
		return $this->fechaActualizacion;
	}

	/**
	 * @param \DateTime $fechaActualizacion
	 */
	public function setFechaActualizacion($fechaActualizacion) {
		$this->fechaActualizacion = $fechaActualizacion;
	}

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id) {
		$this->id = $id;
        }	
}
