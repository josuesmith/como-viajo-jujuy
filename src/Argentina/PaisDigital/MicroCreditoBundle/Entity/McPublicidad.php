<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * McPublicidad
 *
 * @ORM\Table(name="mc_publicidades")
 * @ORM\Entity(repositoryClass="Argentina\PaisDigital\MicroCreditoBundle\Repository\McPublicidadRepository")
 */
class McPublicidad {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * 
     * @Groups({"publicidad"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="titulo_publicidad", type="string", length=100, nullable=false)
     * @Groups({"publicidad"})
     */
    private $tituloPublicidad;

    /**
     * @var string
     * @ORM\Column(name="descripcion_publicidad", type="string", length=200, nullable=false)
     * @Groups({"publicidad"})
     */
    private $descripcionPublicidad;

    /**
     * @var string
     * @ORM\Column(name="nombre_archivo", type="string", length=200, nullable=true)
     * @Groups({"publicidad"})
     */
    private $nombreArchivo;

    /**
     * @var string
     * @ORM\Column(name="ruta_archivo", type="string", length=200, nullable=true)
     * @Groups({"publicidad"})
     */
    private $rutaArchivo;

    /**
     * @var string
     * @ORM\Column(name="tipo_archivo", type="string", length=200, nullable=true)
     * @Groups({"publicidad"})
     */
    private $tipoArchivo;

    /**
     * @var integer
     * @ORM\Column(name="duracion_publicidad", type="integer", nullable=false)
     * @Groups({"publicidad"})
     */
    private $duracionPublicidad;

    /**
     * @var boolean
     * @ORM\Column(name="tipo_publicidad", type="boolean", nullable=false)
     * @Groups({"publicidad"})
     */
    private $tipoPublicidad;

    /**
     * @var boolean
     * @ORM\Column(name="estado", type="boolean", nullable=false)
     * @Groups({"publicidad"})
     */
    private $estado;
    
     /**
     * @var string
     * @ORM\Column(name="url_publicidad", type="string", length=200, nullable=true)
     * @Groups({"publicidad"})
     */
    private $urlPublicidad;

    function getId() {
        return $this->id;
    }

    function getTituloPublicidad() {
        return $this->tituloPublicidad;
    }

    function getDescripcionPublicidad() {
        return $this->descripcionPublicidad;
    }

    function getNombreArchivo() {
        return $this->nombreArchivo;
    }

    function getRutaArchivo() {
        return $this->rutaArchivo;
    }

    function getTipoArchivo() {
        return $this->tipoArchivo;
    }

    function getDuracionPublicidad() {
        return $this->duracionPublicidad;
    }

    function getTipoPublicidad() {
        return $this->tipoPublicidad;
    }

    function getEstado() {
        return $this->estado;
    }
    
    function getUrlPublicidad() {
        return $this->urlPublicidad;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTituloPublicidad($tituloPublicidad) {
        $this->tituloPublicidad = $tituloPublicidad;
    }

    function setDescripcionPublicidad($descripcionPublicidad) {
        $this->descripcionPublicidad = $descripcionPublicidad;
    }

    function setNombreArchivo($nombreArchivo) {
        $this->nombreArchivo = $nombreArchivo;
    }

    function setRutaArchivo($rutaArchivo) {
        $this->rutaArchivo = $rutaArchivo;
    }

    function setTipoArchivo($tipoArchivo) {
        $this->tipoArchivo = $tipoArchivo;
    }

    function setDuracionPublicidad($duracionPublicidad) {
        $this->duracionPublicidad = $duracionPublicidad;
    }

    function setTipoPublicidad($tipoPublicidad) {
        $this->tipoPublicidad = $tipoPublicidad;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }
    
    function setUrlPublicidad($urlPublicidad) {
        $this->urlPublicidad = $urlPublicidad;
    }

}
