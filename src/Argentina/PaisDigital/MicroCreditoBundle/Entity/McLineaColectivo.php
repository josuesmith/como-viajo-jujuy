<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * McLineaColectivo
 *
 * @ORM\Table(name="mc_lineas_colectivos")
 * @ORM\Entity(repositoryClass="Argentina\PaisDigital\MicroCreditoBundle\Repository\McLineaColectivoRepository")
 */
class McLineaColectivo {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * 
     * @Groups({"parada", "linea"})
     */
    private $id;

    /**
     * @var \McEmpresa
     * @Groups({"parada", "linea"})
     * @ORM\ManyToOne(targetEntity="McEmpresa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_empresa", referencedColumnName="id")
     * })     
     */
    private $empresa;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="string", length=45, nullable=false)
     * @Groups({"parada", "linea"})
     */
    private $nombre;

    /**
     * @var string
     *
     * @Groups({"parada", "linea"})
     * 
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;       

    function setId($id) {
        $this->id = $id;
    }

    function getId() {
        return $this->id;
    }

    function setEmpresa($empresa) {
        $this->empresa = $empresa;
    }

    function getEmpresa() {
        return $this->empresa;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function getDescripcion() {
        return $this->descripcion;
    } 

}
