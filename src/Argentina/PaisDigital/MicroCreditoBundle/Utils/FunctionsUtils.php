<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Utils;

class FunctionsUtils {
    public static function esCuilValido($value) {
        if (strlen($value) >= 10) {
            $cuit = str_split($value);
            $totalDni = 0;
            $codigo_valido = true;
            $mult = array(5, 4, 3, 2, 7, 6, 5, 4, 3, 2);
            $total = 0;
            for ($i = 0; $i < count($mult); $i++) {
                $a = $cuit[$i];
                $total += (int) ($cuit[$i]) * $mult[$i];
                if (($i > 1) && ($i < 9)) { //para que en el DNI no se ingresen ceros
                    $totalDni += (int) $cuit[$i] * $mult[$i];
                }
            }
            if ($totalDni == 0) {
                $codigo_valido = false;
            } else {
                $mod = $total % 11;
                if ($mod == 0) {
                    $digito = 0;
                } else {
                    if ($mod == 1) {
                        $digito = 9;
                    } else {
                        $digito = (11 - $mod);
                    }
                }
                $digitoVerificador = (int) $cuit[10];
                $codigo_valido = ($digito == $digitoVerificador);
            }
            return $codigo_valido;
        } else {
            return false;
        }
    }
    
    public static function esDniValido($value) {
        if (strlen($value) >= 7) {
            return true;
        } else {
            return false;
        }
    }
}


    