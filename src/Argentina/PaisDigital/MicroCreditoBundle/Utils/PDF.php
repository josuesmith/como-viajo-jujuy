<?php


namespace Argentina\PaisDigital\MicroCreditoBundle\Utils;


class PDF extends \TCPDF {

    private $image;
    protected $title;

    public function Header() {
//        $logo = $this->getImage();
//        if (file_exists($logo)) {
//            $this->Image($logo, 10, 5, 50, 25, '', '', 'T', false, 300, '', false, false, 0, false, false, false);
//        }
    }

    function getImage() {
        return $this->image;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function getTitle() {
        return $this->title;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    public function setCustomLineGray($val1,$val2){
        $this->Ln($val1);
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(220, 220, 220));
        $this->Line(10, $this->GetY(), 200, $this->GetY(), $style);
        $this->Ln($val2);
    }

    public function setCustomLineBlack($val1,$val2){
        $this->Ln($val1);
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
        $this->Line(10, $this->GetY(), 200, $this->GetY(), $style);
        $this->Ln($val2);
    }

    public function setCustomHeader($path,$posY,$tax = null){
        $this->SetY($posY);
        $this->Image($path, '', '', 60, 25, 'png', '','', true,
            300, '', false, false, 0, false, false, false,"","");
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(20, 20, 20));
        $this->Line(148, 10, 148, 29, $style);
        $this->SetFont('times', '', 12);
        $this->Ln(4);
        $this->MultiCell(30, 6, 'IMPUESTO', 0, 'C',
            0, 0, 160, '', true, '', '', true, 6, 'M', true);
        $this->Ln(6);
        $this->SetFont('times', 'B', 24);
        $this->MultiCell(50, 8, strtoupper($tax['tipoTasa']), 0, 'C',
            0, 0, 150, '', true, '', '', true, 6, 'M', true);
        $this->Ln(8);
        $this->SetFont('times', '', 12);
        $this->SetFillColor(229,239,255);
        $this->MultiCell(50, 6, 'Periodo: <b>'.$tax["periodo"].'</b>', 0, 'C', 1, '',
            150 ,'', true,'',true,'','','',true);
        $this->SetFillColor(255, 255, 255);
    }

    public function setDataSuscriber($tax = null){
        $this->setCustomLineGray(10,2);
        $this->SetFont('times', '', 10);
        $posY = $this->GetY();
//        $this->MultiCell(60, 8, '<b>Nombre completo del titular</b>', 0, 'L',
//            1, 1, '', '', true, '', true, true, 6, 'M', true);
//        $this->MultiCell(60, 8, '<b>Direccion Completa del titular</b>', 0, 'L', 1, 0,
//            '' ,'', true,'',true,'','','',true);
        $this->SetFillColor(229,239,255);
        $this->SetFont('', 'B', '10');
        $this->MultiCell(50, 12, 'Partida '.$tax["contribuyente"], 0, 'C', 1, 1,
            150 ,$posY, true,'',false,'','','M',true);
    }

    private function firstTable($tax){

        $importe1 = '$'.$tax["importe1"];
        $importe2 = '$'.$tax["importe2"];
        $tipoTasa = strtoupper($tax['tipoTasa']);

        $html = <<<EOF
            <style>
                table {
                    border:1px solid #c0c0c0;
                    font-size: 8pt;
                    font-weight: bold;
                }
                td,th {
                    border:none
                }
                .bordeTd {
                    border-top: 0.5px #d2d2d2 !important;
                }
            </style>
            
            <table cellpadding="2" cellspacing="2">
                <thead>
                    <tr>
                        <th class="borderBottom" width="160" colspan="2" align="center" style="font-size: 9pt;"><b>Tributo Anual Total</b></th>
                    </tr>
                </thead>
                <tbody>
                     <tr>
                        <td width="100" align="left" class="bordeTd">Total</td>
                        <td width="60" align="right" class="bordeTd"><b>xxxxxxx</b></td>
                     </tr>
                     <tr>
                        <td width="100" align="left" class="bordeTd">Impuesto</td>
                        <td width="60" align="right" class="bordeTd"><b>xxxxxxx</b></td>
                     </tr>
                     <tr>
                        <td width="100" align="left" class="bordeTd">{$tipoTasa}</td>
                        <td width="60" align="right"class="bordeTd"><b>xxxxxxx</b></td>
                     </tr>
                </tbody>
            </table>
EOF;
        $this->writeHTML($html, false, true, true, false, '');
    }

    private function secondTable($tax){

        $importe1 = '$'.$tax["importe1"];
        $importe2 = '$'.$tax["importe2"];
        $fechaVencimiento1 = $tax["fechaVencimiento1"];
        $fechaVencimiento2 = $tax["fechaVencimiento2"];

        $html = <<<EOF
            <style>
                table {
                    background-color: #e5efff;
                    border:1px solid #c0c0c0;
                    font-size: 8pt;
                    font-weight: bold;
                }
                td,th {
                    border:none
                }
                .bordeTd {
                    border-top: 0.5px white !important;
                }
            </style>
            
            <table cellpadding="4.5" cellspacing="2.18">
                <thead>
                    <tr>
                        <th class="borderBottom" width="192" colspan="3" align="center" style="font-size: 9pt; vertical-align: top"><b>DETALLE DEL PAGO</b></th>
                    </tr>
                </thead>
                <tbody>
                     <tr>
                        <td width="52" align="left" class="">1°Vto.</td>
                        <td width="70" align="center" class="">{$fechaVencimiento1}</td>
                        <td width="70" align="right" class=""><b>{$importe1}</b></td>
                     </tr>
                     <tr>
                        <td width="52" align="left" class="bordeTd">2°Vto.</td>
                        <td width="70" align="center" class="bordeTd">{$fechaVencimiento2}</td>
                        <td width="70" align="right" class="bordeTd"><b>{$importe2}</b></td>
                     </tr>
                </tbody>
            </table>
EOF;
        $this->writeHTML($html, true, false, true, false, '');
    }

    private function thirdTable($tax){
        $html = <<<EOF
            <style>
                table {
                    border:1px solid #c0c0c0;
                    font-size: 8pt;
                }
                td,th {
                    border:none
                }
                .bordeTd {
                    border-top: 0.5px white !important;
                }
            </style>
            
            <table cellpadding="4" cellspacing="8">
                <thead>
                    <tr>
                        <th class="borderBottom" width="148" align="center" style="font-size: 9pt;"><b>Codigo para cajeros automaticos</b></th>
                    </tr>
                </thead>
                <tbody>
                     <tr>
                        <td width="148" align="center" class="" style="font-size: 12px"><b>XXXXXXXXXXXX</b></td>
                     </tr>
                </tbody>
            </table>
EOF;
        $this->writeHTML($html, false, false, true, false, '');
    }

    public function setTables($tax){
        $this->setCustomLineGray(2,2);
        $posX = $this->GetX();  //posicion inicial eje X para referencia de la otra tabla
        $posY = $this->GetY();  //posicion inicial eje Y para referencia de la otra tabla

        $this->SetFont('times', '', 12);
        $this->firstTable($tax);

        $posX += 60;    //posiciones para las tablas
        $this->SetXY($posX,$posY);
        $this->secondTable($tax);

        $posX += 72;    //posiciones para las tablas
        $this->SetXY($posX,$posY);
        $this->thirdTable($tax);
    }

    public function setBarcodeTax($tax,$barcodeType){
        $style = array(
            'position' => 'C',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        $this->Ln(2);
        $codebarPosY = $this->GetY() + 3;
        $this->SetFillColor(229,239,255);
        $this->MultiCell('', 26, '', 0, 'C', 1, 1,
            '' ,'', true,'',false,'','','M',true);

        $posYLineCut = $this->GetY() + 2;
        $this->write1DBarcode($tax["codigoBarras"], $barcodeType, '', $codebarPosY, 150, 20, 0.4, $style, 'N');

        $style = array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => '4,4', 'phase' => 10, 'color' => array(0, 0, 0));
        $this->Line(10, $posYLineCut, 200, $posYLineCut, $style);
    }

    public function setTicket($path,$tax){
        $this->Ln(12);
        $posX = $this->GetX();  //posicion inicial eje X para referencia de la otra tabla
        $posY = $this->GetY();  //posicion inicial eje Y para referencia de la otra tabla

//        $this->Image($path, $posX, $posY, 40, 25, 'jpg', '','', true,
//            300, '', false, false, 0, false, false, false,"","");

        $this->Image($path, $posX, $posY, 45, 20, 'png', '','', true,
            300, '', false, false, 0, false, false, false,"","");

        $posX += 60;    //posiciones para las tablas
        $this->SetXY($posX,$posY);
        $this->secondTable($tax);

        $this->SetFont('times', 'B', 24);
        $this->MultiCell(50, 8, strtoupper($tax['tipoTasa']), 0, 'C',
            0, 0, 150, $posY, true, '', '', true, 6, 'M', true);
        $this->Ln(8);
        $this->SetFont('times', '', 12);
        $this->SetFillColor(229,239,255);
        $this->MultiCell(50, 6, 'Periodo: <b>'.$tax["periodo"].'</b>', 0, 'C', 1, '',
            150 , '', true,'',true,'','','',true);
        $this->Ln(8);
        $this->SetFont('', 'B', '10');
        $this->MultiCell(50, 8, 'Partida '.$tax["contribuyente"], 0, 'C', 1, 1,
            150 ,'', true,'',false,'','','M',true);
//        $this->setCustomLineGray(4,2);

    }

}
