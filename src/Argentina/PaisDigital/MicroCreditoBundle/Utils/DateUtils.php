<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Utils;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Description of DateUtils
 *
 * @author pchacon
 */
class DateUtils{

    public static function validateDate($fecha, $nombreCampo = null) {
        try {
            $date = new \DateTime($fecha);
            $date->format('Y-m-d H:i:s');
        } catch (\Exception $e) {
            if (is_null($nombreCampo))
                throw new BadRequestHttpException("El campo fecha es incorrecto");
            else
                throw new BadRequestHttpException("El campo fecha es incorrecto (" . $nombreCampo . ")");
        }
    }

    public static function getDateFromString($fecha, $nombreCampo = null) {
        try {
            $date = new \DateTime($fecha);
            $date->format('Y-m-d H:i:s');
            return $date;
        } catch (\Exception $e) {
            if (is_null($nombreCampo))
                throw new BadRequestHttpException("El campo fecha es incorrecto");
            else
                throw new BadRequestHttpException("El campo fecha es incorrecto (" . $nombreCampo . ")");
        }
    }

    public static function getIDateFromString($fecha, $nombreCampo = null) {
        try {
            $date = new \DateTime($fecha);
            return strtotime($date->format('Y-m-d'));
        } catch (\Exception $e) {
            if (is_null($nombreCampo))
                throw new BadRequestHttpException("El campo fecha es incorrecto");
            else
                throw new BadRequestHttpException("El campo fecha es incorrecto (" . $nombreCampo . ")");
        }
    }

    /**
     * deuelve la fecha/hora actual mas la cantidad de horas que se indique en el paramentro
     * @param type $horaASumar
     * @return type
     * @throws BadRequestHttpException
     */
    public static function getDateForExpToken($horaASumar) {
        $today = date("Y-m-d H:i:s");
        return strtotime('+'.$horaASumar.' hours', strtotime($today));
    }
    
        /**
     * 
     * @param type $fecha 
     * @param type $format  ej 'Y-m-d H:i:s'
     * @return \String
     * @throws BadRequestHttpException
     */
     public static function getDateFromFormat($fecha, $format) {
        $result = $fecha->format($format);
        return $result;
        
    }

}
