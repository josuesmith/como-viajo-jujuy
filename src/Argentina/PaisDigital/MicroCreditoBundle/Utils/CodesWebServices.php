<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Utils;

use Symfony\Component\HttpFoundation\Response;


class CodesWebServices {
    const OK = 0;  
    const ERROR = 1;
    const ERROR_LINEA_COLECTIVO = 2;
}