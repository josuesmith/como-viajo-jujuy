<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Utils;

class StringUtils {
    public static function clean($string, $stringReplace = null) {
        $unwanted_array = array('Š' => 'S', 'š' => 's', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
            'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U',
            'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c',
            'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o',
            'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y');
        $string = strtr($string, $unwanted_array);
        if (is_null($stringReplace)) {
            $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        } else {
            $string = str_replace(' ', $stringReplace, $string); // Replaces all spaces with hyphens.
        }
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

    public static function cleanForMysql($campo) {
        $replace = "";
        $replaceFinal = "";
        $arrayReplace = array('/', '\\', ':', ';', '!', '@', '#', '$', '%', '^', '*', '(', ')', '_', '+', '=', '|', '{', '}', '[', ']', '"', '<', '>', ',', '?', '~', '`', '&', ' ', '.', '-');
        $i = 0;
        foreach ($arrayReplace as $value) {
            if ($i == 0) {
                $replace = $campo . ",'". $value ."',''";
            } else {
                $replace = ",'". $value ."',''";
            }
            $replaceFinal = "replace(" . $replaceFinal . $replace . ")";
            $i++;
        }
        return $replaceFinal;
    }

    public static function isNullOrEmpty($string) {
        $result = false;
        if($string === NULL) {
            $result =  true;
        }else{
            if($string === '') {
                $result =  true;
            }
        }
        return $result;
    }

    public static function isNotNullOrEmpty($string) {
        $result = false;
        if($string === NULL) {
            $result =  true;
        }else{
            if($string === '') {
                $result =  true;
            }
        }
        return !$result;
    }
}
