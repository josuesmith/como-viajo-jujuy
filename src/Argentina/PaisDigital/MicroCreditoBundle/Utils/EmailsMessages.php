<?php
namespace Argentina\PaisDigital\MicroCreditoBundle\Utils;

class EmailsMessages {        
    public static function mensajeRecuperarClaveEmail($nombreApellido, $username, $linkVerificacion, $urlFront){
        $urlLogo = strstr($urlFront, 'app_dev.php', true);
        $urlLogo .= "app/";
        $output =  '<!doctype html>'.
                    '<html>'.
                        '<head>'.
                            '<meta charset="UTF-8">'.
                            '<title>ComoViajoJujuy</title>'.
                        '</head>'.
                        '<body>'.
                            '<table style="text-align:left; width: 80%; margin: 0px auto; font-size: 16px; border-collapse: collapse; border-spacing: 0; line-height: 2; font-size: 14px; font-family: Helvetica, Arial, Sans-Serif;">'.
                                '<thead>'.
                                    '<tr style=" border-bottom: 1px solid #ccc; margin-bottom: 20px ">'.
                                        '<th style="padding: 18px 0px; text-align:left">'.
                                            '<img src="'.$urlLogo.'" alt="Logo ComoViajoJujuy" style="width:364px; text-align:center"/>'.
                                        '</th>'.
                                    '</tr>'.
                                '</thead>'.
                                '<tbody>'.
                                    '<tr>'.
                                        '<td style="padding-top: 20px">'.
                                            $nombreApellido.',<br>'.
                                            'se ha enviado un correo eléctronico para recuperar la contraseña de Como Viajo Jujuy, tu nombre de usuario es:<br>'.
                                            '<span style="color:#3D4D66; font-weight: bold">'.$username.'</span>'.
                                            '<br><br>'.
                                            'Necesitamos que gestiones tu nueva contraseña, haciendo click en el botón Gestionar Contraseña.<br><br>'.
                                            '<a style="background-color: #3D4D66; color: #fff;  text-transform: uppercase; border: none;padding: 12px; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; font-size: 16px; margin-top: 30px; margin-bottom:80px" '.
                                                'href="'.$linkVerificacion.'" >'.
                                                'GESTIONAR CONTRASEÑA'.
                                            '</a>'.
                                        '</td>'.
                                    '</tr>'.												
                                '</tbody>'.
                            '</table>'.
                        '</body>'.
                    '</html>';
        return $output; 
    }    
}