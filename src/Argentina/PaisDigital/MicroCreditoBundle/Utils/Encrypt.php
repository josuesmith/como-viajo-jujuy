<?php

namespace Argentina\PaisDigital\MicroCreditoBundle\Utils;

/**
 *
 * @author pchacon
 */
class Encrypt {

    public static function encrypt($input, $clave) {         
        $encrypt_method = "aes-256-cbc";
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        $output = openssl_encrypt($input, $encrypt_method, $clave, 0 , $iv);                       
        return strtr(base64_encode($output . '::' . $iv), '+/=', '._-');
    }

    public static function decrypt($input, $clave) {
        $encrypt_method = "aes-256-cbc";
        list($encrypted_data, $iv) =  explode('::', base64_decode(strtr($input, '._-', '+/=')), 2);
        return openssl_decrypt($encrypted_data, $encrypt_method, $clave, 0, $iv);
    }

}
