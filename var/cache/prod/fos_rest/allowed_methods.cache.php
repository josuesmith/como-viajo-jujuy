<?php return array (
  'admin_ajax_usuario_clave' => 
  array (
    0 => 'PUT',
  ),
  'admin_ajax_busquedas__search' => 
  array (
    0 => 'POST',
  ),
  'ajax_usuario_listado' => 
  array (
    0 => 'POST',
  ),
  'ajax_usuario_persona_dni_exists' => 
  array (
    0 => 'GET',
  ),
  'admin_usuario_save' => 
  array (
    0 => 'POST',
  ),
  'admin_ajax_usuario_get_by_id' => 
  array (
    0 => 'GET',
    1 => 'PUT',
  ),
  'admin_ajax_usuario_update' => 
  array (
    0 => 'GET',
    1 => 'PUT',
  ),
  'admin_ajax_credito_detalle' => 
  array (
    0 => 'POST',
  ),
  'admin_previewcsv' => 
  array (
    0 => 'POST',
  ),
  'admin_uploadcsv' => 
  array (
    0 => 'POST',
  ),
  'admin_ajax_creditos_listado_organismoPersona_search' => 
  array (
    0 => 'POST',
  ),
  'admin_ajax_empresa_save' => 
  array (
    0 => 'POST',
  ),
  'admin_ajax_empresa_update' => 
  array (
    0 => 'PUT',
    1 => 'GET',
  ),
  'admin_ajax_empresa_get_by_id' => 
  array (
    0 => 'PUT',
    1 => 'GET',
  ),
  'admin_ajax_empresa_listado' => 
  array (
    0 => 'POST',
  ),
  'downloadcreditoscsv' => 
  array (
    0 => 'GET',
  ),
  'admin_ajax_empresas_get_search' => 
  array (
    0 => 'GET',
  ),
  'admin_ajax_reclamos_listado' => 
  array (
    0 => 'POST',
  ),
  'admin_ajax_reclamos_get_id' => 
  array (
    0 => 'GET',
  ),
  'admin_ajax_archivocsv_listado_search' => 
  array (
    0 => 'POST',
  ),
  'admin_ajax_persona_get_search' => 
  array (
    0 => 'GET',
  ),
  'ajax_empresa_get_exists_by_cuit' => 
  array (
    0 => 'GET',
  ),
  'ajax_persona_get_search' => 
  array (
    0 => 'GET',
  ),
  'empresaconc_ajax_empresa_listado' => 
  array (
    0 => 'POST',
  ),
  'empresaconc_ajax_empresa_save' => 
  array (
    0 => 'POST',
  ),
  'empresaconc_ajax_empresa_get_by_id' => 
  array (
    0 => 'GET',
    1 => 'PUT',
  ),
  'empresaconc_ajax_empresa_update' => 
  array (
    0 => 'GET',
    1 => 'PUT',
  ),
  'empresaconc_ajax_empresa_update_my' => 
  array (
    0 => 'PUT',
  ),
  'empresaconc_ajax_usuario_clave' => 
  array (
    0 => 'PUT',
  ),
  'empresaconc_ajax_credito_detalle' => 
  array (
    0 => 'POST',
  ),
  'empresaconc_previewcsv' => 
  array (
    0 => 'POST',
  ),
  'empresaconc_uploadcsv' => 
  array (
    0 => 'POST',
  ),
  'empresaconc_ajax_creditos_listado_organismoPersona_search' => 
  array (
    0 => 'POST',
  ),
  'empresaconc_ajax_usuario_listado' => 
  array (
    0 => 'POST',
  ),
  'empresaconc_ajax_usuario_persona_dni_exists' => 
  array (
    0 => 'GET',
  ),
  'empresaconc_usuario_save' => 
  array (
    0 => 'POST',
  ),
  'empresaconc_ajax_usuario_get_by_id' => 
  array (
    0 => 'GET',
    1 => 'PUT',
  ),
  'empresaconc_ajax_usuario_update' => 
  array (
    0 => 'GET',
    1 => 'PUT',
  ),
  'empresaconc_ajax_archivocsv_listado_search' => 
  array (
    0 => 'POST',
  ),
  'hacienda_ajax_usuario_clave' => 
  array (
    0 => 'PUT',
  ),
  'hacienda_ajax_busquedas__search' => 
  array (
    0 => 'POST',
  ),
  'hacienda_ajax_empresa_listado' => 
  array (
    0 => 'POST',
  ),
  'hacienda_ajax_empresa_save' => 
  array (
    0 => 'POST',
  ),
  'hacienda_ajax_empresa_get_by_id' => 
  array (
    0 => 'GET',
    1 => 'PUT',
  ),
  'hacienda_ajax_empresa_update' => 
  array (
    0 => 'GET',
    1 => 'PUT',
  ),
  'hacienda_ajax_empresas_get_search' => 
  array (
    0 => 'GET',
  ),
  'ajax_credito_detalle' => 
  array (
    0 => 'POST',
  ),
  'hacienda_previewcsv' => 
  array (
    0 => 'POST',
  ),
  'hacienda_uploadcsv' => 
  array (
    0 => 'POST',
  ),
  'ajax_creditos_listado_organismoPersona_search' => 
  array (
    0 => 'POST',
  ),
  'hacienda_ajax_archivocsv_listado_search' => 
  array (
    0 => 'POST',
  ),
  'empresabase_ajax_empresa_update_my' => 
  array (
    0 => 'PUT',
  ),
  'empresabase_ajax_usuario_clave' => 
  array (
    0 => 'PUT',
  ),
  'empresabase_ajax_creditos_listado_organismoPersona_search' => 
  array (
    0 => 'POST',
  ),
  'empresabase_ajax_credito_detalle' => 
  array (
    0 => 'POST',
  ),
  'empresabase_previewcsv' => 
  array (
    0 => 'POST',
  ),
  'empresabase_uploadcsv' => 
  array (
    0 => 'POST',
  ),
  'empresabase_ajax_archivocsv_listado_search' => 
  array (
    0 => 'POST',
  ),
  'argentina_paisdigital_microcredito_public_creditosearch' => 
  array (
    0 => 'POST',
  ),
  'argentina_paisdigital_microcredito_public_creditosearchcuil' => 
  array (
    0 => 'POST',
  ),
  'argentina_paisdigital_microcredito_public_postreclamos' => 
  array (
    0 => 'POST',
  ),
  'public_ajax_usuario_clave_update' => 
  array (
    0 => 'PUT',
  ),
  'public_ajax_usuario_recuperar_clave_email' => 
  array (
    0 => 'POST',
  ),
);