<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = [];
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        if (0 === strpos($pathinfo, '/login')) {
            // login
            if ('/login' === $pathinfo) {
                return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\SecurityController::loginAction',  '_route' => 'login',);
            }

            // login_success
            if ('/login_success' === $pathinfo) {
                return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\SecurityController::loginSuccessAction',  '_route' => 'login_success',);
            }

        }

        // logout
        if ('/logout' === $pathinfo) {
            return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'logout',);
        }

        if (0 === strpos($pathinfo, '/a')) {
            if (0 === strpos($pathinfo, '/admin')) {
                // admin_home
                if ('/admin/inicio' === $pathinfo) {
                    return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\HomeController::adminHomeAction',  '_route' => 'admin_home',);
                }

                if (0 === strpos($pathinfo, '/admin/ajax')) {
                    if (0 === strpos($pathinfo, '/admin/ajax/usuario')) {
                        // admin_ajax_usuario_clave
                        if ('/admin/ajax/usuario-clave' === $pathinfo && $request->isXmlHttpRequest()) {
                            $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\HomeController::putClaveUpdateAction',  '_route' => 'admin_ajax_usuario_clave',);
                            if (!in_array($requestMethod, ['PUT'])) {
                                $allow = array_merge($allow, ['PUT']);
                                goto not_admin_ajax_usuario_clave;
                            }

                            return $ret;
                        }
                        not_admin_ajax_usuario_clave:

                        if (0 === strpos($pathinfo, '/admin/ajax/usuarios')) {
                            // ajax_usuario_listado
                            if ('/admin/ajax/usuarios/search' === $pathinfo && $request->isXmlHttpRequest()) {
                                $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\UsuarioController::searchAction',  '_route' => 'ajax_usuario_listado',);
                                if (!in_array($requestMethod, ['POST'])) {
                                    $allow = array_merge($allow, ['POST']);
                                    goto not_ajax_usuario_listado;
                                }

                                return $ret;
                            }
                            not_ajax_usuario_listado:

                            // admin_usuario_save
                            if ('/admin/ajax/usuarios' === $pathinfo && $request->isXmlHttpRequest()) {
                                $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\UsuarioController::postSaveAction',  '_route' => 'admin_usuario_save',);
                                if (!in_array($requestMethod, ['POST'])) {
                                    $allow = array_merge($allow, ['POST']);
                                    goto not_admin_usuario_save;
                                }

                                return $ret;
                            }
                            not_admin_usuario_save:

                            // admin_ajax_usuario_get_by_id
                            if (preg_match('#^/admin/ajax/usuarios/(?P<id>[^/]++)$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
                                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_ajax_usuario_get_by_id']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\UsuarioController::getUsuarioByIdAction',));
                                if (!in_array($canonicalMethod, ['GET'])) {
                                    $allow = array_merge($allow, ['GET']);
                                    goto not_admin_ajax_usuario_get_by_id;
                                }

                                return $ret;
                            }
                            not_admin_ajax_usuario_get_by_id:

                            // admin_ajax_usuario_update
                            if (preg_match('#^/admin/ajax/usuarios/(?P<id>[^/]++)$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
                                $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_ajax_usuario_update']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\UsuarioController::putUpdateAction',));
                                if (!in_array($requestMethod, ['PUT'])) {
                                    $allow = array_merge($allow, ['PUT']);
                                    goto not_admin_ajax_usuario_update;
                                }

                                return $ret;
                            }
                            not_admin_ajax_usuario_update:

                        }

                        // ajax_usuario_persona_dni_exists
                        if (0 === strpos($pathinfo, '/admin/ajax/usuario/persona/dni') && preg_match('#^/admin/ajax/usuario/persona/dni/(?P<dni>[^/]++)/exists$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ajax_usuario_persona_dni_exists']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\UsuarioController::personaExistsAction',));
                            if (!in_array($canonicalMethod, ['GET'])) {
                                $allow = array_merge($allow, ['GET']);
                                goto not_ajax_usuario_persona_dni_exists;
                            }

                            return $ret;
                        }
                        not_ajax_usuario_persona_dni_exists:

                    }

                    // admin_ajax_busquedas__search
                    if ('/admin/ajax/busquedas/search' === $pathinfo && $request->isXmlHttpRequest()) {
                        $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\HomeController::busquedasSearchAction',  '_route' => 'admin_ajax_busquedas__search',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_admin_ajax_busquedas__search;
                        }

                        return $ret;
                    }
                    not_admin_ajax_busquedas__search:

                    // admin_ajax_busquedas_descargar
                    if ('/admin/ajax/busquedas/descargar' === $pathinfo) {
                        return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\HomeController::descargaAction',  '_route' => 'admin_ajax_busquedas_descargar',);
                    }

                    if (0 === strpos($pathinfo, '/admin/ajax/credito')) {
                        // admin_ajax_credito_detalle
                        if (preg_match('#^/admin/ajax/credito/(?P<credito>[^/]++)/searchDetalle$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_ajax_credito_detalle']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\CreditoController::searchDetalleCreditoAction',));
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_admin_ajax_credito_detalle;
                            }

                            return $ret;
                        }
                        not_admin_ajax_credito_detalle:

                        // admin_ajax_credito_detalle_descarga
                        if ('/admin/ajax/credito/detalle/descarga' === $pathinfo) {
                            return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\CreditoController::descargaAction',  '_route' => 'admin_ajax_credito_detalle_descarga',);
                        }

                        // admin_ajax_creditos_listado_organismoPersona_search
                        if ('/admin/ajax/creditos/listado/organismoPersona/search' === $pathinfo && $request->isXmlHttpRequest()) {
                            $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\CreditoController::searchOrganismoPersonaByCreditoAction',  '_route' => 'admin_ajax_creditos_listado_organismoPersona_search',);
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_admin_ajax_creditos_listado_organismoPersona_search;
                            }

                            return $ret;
                        }
                        not_admin_ajax_creditos_listado_organismoPersona_search:

                    }

                    elseif (0 === strpos($pathinfo, '/admin/ajax/empresas')) {
                        // admin_ajax_empresa_save
                        if ('/admin/ajax/empresas' === $pathinfo && $request->isXmlHttpRequest()) {
                            $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaController::postSaveAction',  '_route' => 'admin_ajax_empresa_save',);
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_admin_ajax_empresa_save;
                            }

                            return $ret;
                        }
                        not_admin_ajax_empresa_save:

                        // admin_ajax_empresa_update
                        if (preg_match('#^/admin/ajax/empresas/(?P<id>[^/]++)$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_ajax_empresa_update']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaController::putUpdateAction',));
                            if (!in_array($requestMethod, ['PUT'])) {
                                $allow = array_merge($allow, ['PUT']);
                                goto not_admin_ajax_empresa_update;
                            }

                            return $ret;
                        }
                        not_admin_ajax_empresa_update:

                        // admin_ajax_empresa_get_by_id
                        if (preg_match('#^/admin/ajax/empresas/(?P<id>[^/]++)$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_ajax_empresa_get_by_id']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaController::getEmpresaByIdAction',));
                            if (!in_array($canonicalMethod, ['GET'])) {
                                $allow = array_merge($allow, ['GET']);
                                goto not_admin_ajax_empresa_get_by_id;
                            }

                            return $ret;
                        }
                        not_admin_ajax_empresa_get_by_id:

                        // admin_ajax_empresa_listado
                        if ('/admin/ajax/empresas/search' === $pathinfo && $request->isXmlHttpRequest()) {
                            $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaController::searchAction',  '_route' => 'admin_ajax_empresa_listado',);
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_admin_ajax_empresa_listado;
                            }

                            return $ret;
                        }
                        not_admin_ajax_empresa_listado:

                    }

                    // admin_ajax_empresas_get_search
                    if ('/admin/ajax/search/empresas' === $pathinfo && $request->isXmlHttpRequest()) {
                        $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaController::getSearchOrganizacionAction',  '_route' => 'admin_ajax_empresas_get_search',);
                        if (!in_array($canonicalMethod, ['GET'])) {
                            $allow = array_merge($allow, ['GET']);
                            goto not_admin_ajax_empresas_get_search;
                        }

                        return $ret;
                    }
                    not_admin_ajax_empresas_get_search:

                    if (0 === strpos($pathinfo, '/admin/ajax/reclamos')) {
                        // admin_ajax_reclamos_listado
                        if ('/admin/ajax/reclamos/search' === $pathinfo && $request->isXmlHttpRequest()) {
                            $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\ReclamoController::searchAction',  '_route' => 'admin_ajax_reclamos_listado',);
                            if (!in_array($requestMethod, ['POST'])) {
                                $allow = array_merge($allow, ['POST']);
                                goto not_admin_ajax_reclamos_listado;
                            }

                            return $ret;
                        }
                        not_admin_ajax_reclamos_listado:

                        // admin_ajax_reclamos_get_id
                        if (preg_match('#^/admin/ajax/reclamos/(?P<id>[^/]++)$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
                            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_ajax_reclamos_get_id']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\ReclamoController::ajaxGetById',));
                            if (!in_array($canonicalMethod, ['GET'])) {
                                $allow = array_merge($allow, ['GET']);
                                goto not_admin_ajax_reclamos_get_id;
                            }

                            return $ret;
                        }
                        not_admin_ajax_reclamos_get_id:

                    }

                    // admin_ajax_archivocsv_listado_search
                    if ('/admin/ajax/archivocsv/listado/search' === $pathinfo && $request->isXmlHttpRequest()) {
                        $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\ArchivoCsvController::searchArchivoCsvAction',  '_route' => 'admin_ajax_archivocsv_listado_search',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_admin_ajax_archivocsv_listado_search;
                        }

                        return $ret;
                    }
                    not_admin_ajax_archivocsv_listado_search:

                    // admin_ajax_persona_get_search
                    if ('/admin/ajax/personas/search' === $pathinfo && $request->isXmlHttpRequest()) {
                        $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\PersonaController::getSearchAction',  '_route' => 'admin_ajax_persona_get_search',);
                        if (!in_array($canonicalMethod, ['GET'])) {
                            $allow = array_merge($allow, ['GET']);
                            goto not_admin_ajax_persona_get_search;
                        }

                        return $ret;
                    }
                    not_admin_ajax_persona_get_search:

                }

                // admin_archivocsv_listado
                if ('/admin/archivocsv/listado' === $pathinfo) {
                    return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\ArchivoCsvController::archivoCsvListadoAction',  '_route' => 'admin_archivocsv_listado',);
                }

                if (0 === strpos($pathinfo, '/admin/usuarios')) {
                    // admin_usuarios_listado
                    if ('/admin/usuarios' === $pathinfo) {
                        return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\UsuarioController::usuarioListadoAction',  '_route' => 'admin_usuarios_listado',);
                    }

                    // admin_usuarios_nuevo
                    if ('/admin/usuarios/nuevo' === $pathinfo) {
                        return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\UsuarioController::usuarioNuevoAction',  '_route' => 'admin_usuarios_nuevo',);
                    }

                    // admin_usuarios_modificacion
                    if (preg_match('#^/admin/usuarios/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_usuarios_modificacion']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\UsuarioController::usuarioModificacionAction',));
                    }

                }

                // admin_uploadcsv
                if ('/admin/uploadcsv' === $pathinfo && $request->isXmlHttpRequest()) {
                    $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\CreditoController::uploadcsvAction',  '_route' => 'admin_uploadcsv',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_admin_uploadcsv;
                    }

                    return $ret;
                }
                not_admin_uploadcsv:

                // admin_previewcsv
                if ('/admin/previewcsv' === $pathinfo && $request->isXmlHttpRequest()) {
                    $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\CreditoController::previewcsvAction',  '_route' => 'admin_previewcsv',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_admin_previewcsv;
                    }

                    return $ret;
                }
                not_admin_previewcsv:

                // admin_creditos_listado_organismos_personas
                if ('/admin/creditos/listado/organismoPersona' === $pathinfo) {
                    return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\CreditoController::creditosOrganismoPersonaListadoAction',  '_route' => 'admin_creditos_listado_organismos_personas',);
                }

                // admin_credito_listado_organismo_persona_detalle
                if (0 === strpos($pathinfo, '/admin/credito/listado/organismo/persona') && preg_match('#^/admin/credito/listado/organismo/persona/(?P<credito>[^/]++)/detalle$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_credito_listado_organismo_persona_detalle']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\CreditoController::detalleListadoOrganismoPersonaByCreditoAction',));
                }

                if (0 === strpos($pathinfo, '/admin/empresa')) {
                    if (0 === strpos($pathinfo, '/admin/empresa-concentradora')) {
                        // admin_empresaconc_nueva
                        if ('/admin/empresa-concentradora/nueva' === $pathinfo) {
                            return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaController::organizacionNuevaAction',  '_route' => 'admin_empresaconc_nueva',);
                        }

                        // admin_empresaconc_modificacion
                        if (preg_match('#^/admin/empresa\\-concentradora/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_empresaconc_modificacion']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaController::organizacionesModificacionAction',));
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/empresa-base')) {
                        // admin_empresabase_nueva
                        if ('/admin/empresa-base/nueva' === $pathinfo) {
                            return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaController::empresaNuevaAction',  '_route' => 'admin_empresabase_nueva',);
                        }

                        // admin_empresabase_modificacion
                        if (preg_match('#^/admin/empresa\\-base/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_empresabase_modificacion']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaController::empresasModificacionAction',));
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/empresas')) {
                        // admin_empresas_listado
                        if ('/admin/empresas' === $pathinfo) {
                            return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaController::empresasListadoAction',  '_route' => 'admin_empresas_listado',);
                        }

                        // admin_empresa_importar_credito
                        if (preg_match('#^/admin/empresas/(?P<empresaId>[^/]++)/importarcredito$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_empresa_importar_credito']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaController::importarCreditoAction',));
                        }

                    }

                }

                // admin_reclamos_listado
                if ('/admin/reclamos' === $pathinfo) {
                    return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\ReclamoController::reclamosListadoAction',  '_route' => 'admin_reclamos_listado',);
                }

            }

            elseif (0 === strpos($pathinfo, '/api/public/credito/search')) {
                // argentina_paisdigital_microcredito_public_creditosearch
                if ('/api/public/credito/search' === $pathinfo) {
                    $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\PublicController::creditoSearchAction',  '_route' => 'argentina_paisdigital_microcredito_public_creditosearch',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_argentina_paisdigital_microcredito_public_creditosearch;
                    }

                    return $ret;
                }
                not_argentina_paisdigital_microcredito_public_creditosearch:

                // argentina_paisdigital_microcredito_public_creditosearchcuil
                if ('/api/public/credito/search/cuil' === $pathinfo) {
                    $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\PublicController::creditoSearchCuilAction',  '_route' => 'argentina_paisdigital_microcredito_public_creditosearchcuil',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_argentina_paisdigital_microcredito_public_creditosearchcuil;
                    }

                    return $ret;
                }
                not_argentina_paisdigital_microcredito_public_creditosearchcuil:

            }

            // argentina_paisdigital_microcredito_public_postreclamos
            if ('/api/public/reclamos' === $pathinfo) {
                $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\PublicController::postReclamosAction',  '_route' => 'argentina_paisdigital_microcredito_public_postreclamos',);
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_argentina_paisdigital_microcredito_public_postreclamos;
                }

                return $ret;
            }
            not_argentina_paisdigital_microcredito_public_postreclamos:

        }

        // admin_my
        if ('/mis-datos' === $pathinfo) {
            return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\HomeController::myAction',  '_route' => 'admin_my',);
        }

        // admin_admin_modificacion_misdatos_clave
        if ('/modificar-mis-datos-clave' === $pathinfo) {
            return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\HomeController::adminModificacionMisDatosClaveAction',  '_route' => 'admin_admin_modificacion_misdatos_clave',);
        }

        // admin_busquedas_listado
        if ('/busquedas' === $pathinfo) {
            return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\HomeController::busquedasListadoAction',  '_route' => 'admin_busquedas_listado',);
        }

        // downloadcreditoscsv
        if ('/downloadcreditoscsv' === $pathinfo) {
            $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaController::downloadCreditosCsvAction',  '_route' => 'downloadcreditoscsv',);
            if (!in_array($canonicalMethod, ['GET'])) {
                $allow = array_merge($allow, ['GET']);
                goto not_downloadcreditoscsv;
            }

            return $ret;
        }
        not_downloadcreditoscsv:

        // ajax_empresa_get_exists_by_cuit
        if (0 === strpos($pathinfo, '/common/ajax/empresas/cuit') && preg_match('#^/common/ajax/empresas/cuit/(?P<cuit>[^/]++)/exists$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
            $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ajax_empresa_get_exists_by_cuit']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Common\\EmpresaController::getExistsEmpresaByCuitAction',));
            if (!in_array($canonicalMethod, ['GET'])) {
                $allow = array_merge($allow, ['GET']);
                goto not_ajax_empresa_get_exists_by_cuit;
            }

            return $ret;
        }
        not_ajax_empresa_get_exists_by_cuit:

        // ajax_persona_get_search
        if ('/common/ajax/personas/search' === $pathinfo && $request->isXmlHttpRequest()) {
            $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Common\\PersonaController::getSearchAction',  '_route' => 'ajax_persona_get_search',);
            if (!in_array($canonicalMethod, ['GET'])) {
                $allow = array_merge($allow, ['GET']);
                goto not_ajax_persona_get_search;
            }

            return $ret;
        }
        not_ajax_persona_get_search:

        if (0 === strpos($pathinfo, '/empresa-concentradora')) {
            // empresaconc_home
            if ('/empresa-concentradora/inicio' === $pathinfo) {
                return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\HomeController::empresaHomeAction',  '_route' => 'empresaconc_home',);
            }

            // empresaconc_my
            if ('/empresa-concentradora/mis-datos' === $pathinfo) {
                return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\HomeController::myAction',  '_route' => 'empresaconc_my',);
            }

            if (0 === strpos($pathinfo, '/empresa-concentradora/modificar-mis-datos')) {
                // empresaconc_empresaconc_modificacion_misdatos
                if ('/empresa-concentradora/modificar-mis-datos' === $pathinfo) {
                    return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\EmpresaController::empresasModificacionMisDatosAction',  '_route' => 'empresaconc_empresaconc_modificacion_misdatos',);
                }

                // empresaconc_empresaconc_modificacion_misdatos_clave
                if ('/empresa-concentradora/modificar-mis-datos-clave' === $pathinfo) {
                    return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\EmpresaController::empresasModificacionMisDatosClaveAction',  '_route' => 'empresaconc_empresaconc_modificacion_misdatos_clave',);
                }

            }

            elseif (0 === strpos($pathinfo, '/empresa-concentradora/empresas')) {
                // empresaconc_empresabase_nueva
                if ('/empresa-concentradora/empresas-base/nueva' === $pathinfo) {
                    return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\EmpresaController::empresaNuevaAction',  '_route' => 'empresaconc_empresabase_nueva',);
                }

                // empresaconc_empresa_listado
                if ('/empresa-concentradora/empresas' === $pathinfo) {
                    return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\EmpresaController::empresasListadoAction',  '_route' => 'empresaconc_empresa_listado',);
                }

                // empresaconc_empresa_importar_credito
                if (preg_match('#^/empresa\\-concentradora/empresas/(?P<empresaId>[^/]++)/importarcredito$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'empresaconc_empresa_importar_credito']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\EmpresaController::importarCreditoAction',));
                }

            }

            // empresaconc_empresabase_modificacion
            if (0 === strpos($pathinfo, '/empresa-concentradora/empresa-base') && preg_match('#^/empresa\\-concentradora/empresa\\-base/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'empresaconc_empresabase_modificacion']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\EmpresaController::empresasModificacionAction',));
            }

            if (0 === strpos($pathinfo, '/empresa-concentradora/ajax')) {
                if (0 === strpos($pathinfo, '/empresa-concentradora/ajax/empresas')) {
                    // empresaconc_ajax_empresa_listado
                    if ('/empresa-concentradora/ajax/empresas/search' === $pathinfo && $request->isXmlHttpRequest()) {
                        $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\EmpresaController::searchAction',  '_route' => 'empresaconc_ajax_empresa_listado',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_empresaconc_ajax_empresa_listado;
                        }

                        return $ret;
                    }
                    not_empresaconc_ajax_empresa_listado:

                    // empresaconc_ajax_empresa_save
                    if ('/empresa-concentradora/ajax/empresas' === $pathinfo && $request->isXmlHttpRequest()) {
                        $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\EmpresaController::postSaveAction',  '_route' => 'empresaconc_ajax_empresa_save',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_empresaconc_ajax_empresa_save;
                        }

                        return $ret;
                    }
                    not_empresaconc_ajax_empresa_save:

                    // empresaconc_ajax_empresa_get_by_id
                    if (preg_match('#^/empresa\\-concentradora/ajax/empresas/(?P<id>[^/]++)$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'empresaconc_ajax_empresa_get_by_id']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\EmpresaController::getEmpresaByIdAction',));
                        if (!in_array($canonicalMethod, ['GET'])) {
                            $allow = array_merge($allow, ['GET']);
                            goto not_empresaconc_ajax_empresa_get_by_id;
                        }

                        return $ret;
                    }
                    not_empresaconc_ajax_empresa_get_by_id:

                    // empresaconc_ajax_empresa_update
                    if (preg_match('#^/empresa\\-concentradora/ajax/empresas/(?P<id>[^/]++)$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'empresaconc_ajax_empresa_update']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\EmpresaController::putUpdateAction',));
                        if (!in_array($requestMethod, ['PUT'])) {
                            $allow = array_merge($allow, ['PUT']);
                            goto not_empresaconc_ajax_empresa_update;
                        }

                        return $ret;
                    }
                    not_empresaconc_ajax_empresa_update:

                    if (0 === strpos($pathinfo, '/empresa-concentradora/ajax/empresas-my')) {
                        // empresaconc_ajax_empresa_update_my
                        if ('/empresa-concentradora/ajax/empresas-my' === $pathinfo && $request->isXmlHttpRequest()) {
                            $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\EmpresaController::putUpdateMisDatosAction',  '_route' => 'empresaconc_ajax_empresa_update_my',);
                            if (!in_array($requestMethod, ['PUT'])) {
                                $allow = array_merge($allow, ['PUT']);
                                goto not_empresaconc_ajax_empresa_update_my;
                            }

                            return $ret;
                        }
                        not_empresaconc_ajax_empresa_update_my:

                        // empresaconc_ajax_usuario_clave
                        if ('/empresa-concentradora/ajax/empresas-my/usuario-clave' === $pathinfo && $request->isXmlHttpRequest()) {
                            $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\EmpresaController::putClaveUpdateAction',  '_route' => 'empresaconc_ajax_usuario_clave',);
                            if (!in_array($requestMethod, ['PUT'])) {
                                $allow = array_merge($allow, ['PUT']);
                                goto not_empresaconc_ajax_usuario_clave;
                            }

                            return $ret;
                        }
                        not_empresaconc_ajax_usuario_clave:

                    }

                }

                elseif (0 === strpos($pathinfo, '/empresa-concentradora/ajax/credito')) {
                    // empresaconc_ajax_credito_detalle
                    if (preg_match('#^/empresa\\-concentradora/ajax/credito/(?P<credito>[^/]++)/searchDetalle$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'empresaconc_ajax_credito_detalle']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\CreditoController::searchDetalleCreditoAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_empresaconc_ajax_credito_detalle;
                        }

                        return $ret;
                    }
                    not_empresaconc_ajax_credito_detalle:

                    // empresaconc_ajax_credito_detalle_descarga
                    if ('/empresa-concentradora/ajax/credito/detalle/descarga' === $pathinfo) {
                        return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\CreditoController::descargaAction',  '_route' => 'empresaconc_ajax_credito_detalle_descarga',);
                    }

                    // empresaconc_ajax_creditos_listado_organismoPersona_search
                    if ('/empresa-concentradora/ajax/creditos/listado/organismoPersona/search' === $pathinfo && $request->isXmlHttpRequest()) {
                        $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\CreditoController::searchOrganismoPersonaByCreditoAction',  '_route' => 'empresaconc_ajax_creditos_listado_organismoPersona_search',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_empresaconc_ajax_creditos_listado_organismoPersona_search;
                        }

                        return $ret;
                    }
                    not_empresaconc_ajax_creditos_listado_organismoPersona_search:

                }

                elseif (0 === strpos($pathinfo, '/empresa-concentradora/ajax/usuarios')) {
                    // empresaconc_ajax_usuario_listado
                    if ('/empresa-concentradora/ajax/usuarios/search' === $pathinfo && $request->isXmlHttpRequest()) {
                        $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\UsuarioController::searchAction',  '_route' => 'empresaconc_ajax_usuario_listado',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_empresaconc_ajax_usuario_listado;
                        }

                        return $ret;
                    }
                    not_empresaconc_ajax_usuario_listado:

                    // empresaconc_usuario_save
                    if ('/empresa-concentradora/ajax/usuarios' === $pathinfo && $request->isXmlHttpRequest()) {
                        $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\UsuarioController::postSaveAction',  '_route' => 'empresaconc_usuario_save',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_empresaconc_usuario_save;
                        }

                        return $ret;
                    }
                    not_empresaconc_usuario_save:

                    // empresaconc_ajax_usuario_get_by_id
                    if (preg_match('#^/empresa\\-concentradora/ajax/usuarios/(?P<id>[^/]++)$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'empresaconc_ajax_usuario_get_by_id']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\UsuarioController::getUsuarioByIdAction',));
                        if (!in_array($canonicalMethod, ['GET'])) {
                            $allow = array_merge($allow, ['GET']);
                            goto not_empresaconc_ajax_usuario_get_by_id;
                        }

                        return $ret;
                    }
                    not_empresaconc_ajax_usuario_get_by_id:

                    // empresaconc_ajax_usuario_update
                    if (preg_match('#^/empresa\\-concentradora/ajax/usuarios/(?P<id>[^/]++)$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'empresaconc_ajax_usuario_update']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\UsuarioController::putUpdateAction',));
                        if (!in_array($requestMethod, ['PUT'])) {
                            $allow = array_merge($allow, ['PUT']);
                            goto not_empresaconc_ajax_usuario_update;
                        }

                        return $ret;
                    }
                    not_empresaconc_ajax_usuario_update:

                }

                // empresaconc_ajax_usuario_persona_dni_exists
                if (0 === strpos($pathinfo, '/empresa-concentradora/ajax/usuario/persona/dni') && preg_match('#^/empresa\\-concentradora/ajax/usuario/persona/dni/(?P<dni>[^/]++)/exists$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
                    $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'empresaconc_ajax_usuario_persona_dni_exists']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\UsuarioController::personaExistsAction',));
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_empresaconc_ajax_usuario_persona_dni_exists;
                    }

                    return $ret;
                }
                not_empresaconc_ajax_usuario_persona_dni_exists:

                // empresaconc_ajax_archivocsv_listado_search
                if ('/empresa-concentradora/ajax/archivocsv/listado/search' === $pathinfo && $request->isXmlHttpRequest()) {
                    $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\ArchivoCsvController::searchArchivoCsvAction',  '_route' => 'empresaconc_ajax_archivocsv_listado_search',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_empresaconc_ajax_archivocsv_listado_search;
                    }

                    return $ret;
                }
                not_empresaconc_ajax_archivocsv_listado_search:

            }

            // empresaconc_archivocsv_listado
            if ('/empresa-concentradora/archivocsv/listado' === $pathinfo) {
                return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\ArchivoCsvController::archivoCsvListadoAction',  '_route' => 'empresaconc_archivocsv_listado',);
            }

            if (0 === strpos($pathinfo, '/empresa-concentradora/credito')) {
                // empresaconc_credito_detalle
                if (preg_match('#^/empresa\\-concentradora/credito/(?P<credito>[^/]++)/detalle$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'empresaconc_credito_detalle']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\CreditoController::detalleCreditoAction',));
                }

                // empresaconc_creditos_listado_organismos_personas
                if ('/empresa-concentradora/creditos/listado/organismoPersona' === $pathinfo) {
                    return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\CreditoController::creditosOrganismoPersonaListadoAction',  '_route' => 'empresaconc_creditos_listado_organismos_personas',);
                }

                // empresaconc_credito_listado_organismo_persona_detalle
                if (0 === strpos($pathinfo, '/empresa-concentradora/credito/listado/organismo/persona') && preg_match('#^/empresa\\-concentradora/credito/listado/organismo/persona/(?P<credito>[^/]++)/detalle$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'empresaconc_credito_listado_organismo_persona_detalle']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\CreditoController::detalleListadoOrganismoPersonaByCreditoAction',));
                }

            }

            // empresaconc_previewcsv
            if ('/empresa-concentradora/previewcsv' === $pathinfo && $request->isXmlHttpRequest()) {
                $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\CreditoController::previewcsvAction',  '_route' => 'empresaconc_previewcsv',);
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_empresaconc_previewcsv;
                }

                return $ret;
            }
            not_empresaconc_previewcsv:

            // empresaconc_uploadcsv
            if ('/empresa-concentradora/uploadcsv' === $pathinfo && $request->isXmlHttpRequest()) {
                $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\CreditoController::uploadcsvAction',  '_route' => 'empresaconc_uploadcsv',);
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_empresaconc_uploadcsv;
                }

                return $ret;
            }
            not_empresaconc_uploadcsv:

            if (0 === strpos($pathinfo, '/empresa-concentradora/usuarios')) {
                // empresaconc_usuarios_listado
                if ('/empresa-concentradora/usuarios' === $pathinfo) {
                    return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\UsuarioController::usuarioListadoAction',  '_route' => 'empresaconc_usuarios_listado',);
                }

                // empresaconc_usuarios_nuevo
                if ('/empresa-concentradora/usuarios/nuevo' === $pathinfo) {
                    return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\UsuarioController::usuarioNuevoAction',  '_route' => 'empresaconc_usuarios_nuevo',);
                }

                // empresaconc_usuarios_modificacion
                if (preg_match('#^/empresa\\-concentradora/usuarios/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'empresaconc_usuarios_modificacion']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaConc\\UsuarioController::usuarioModificacionAction',));
                }

            }

        }

        elseif (0 === strpos($pathinfo, '/empresa-base')) {
            // empresabase_home
            if ('/empresa-base/inicio' === $pathinfo) {
                return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaBase\\HomeController::empresaHomeAction',  '_route' => 'empresabase_home',);
            }

            // empresabase_empresa_importar_credito
            if ('/empresa-base/importarcredito' === $pathinfo) {
                return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaBase\\EmpresaController::importarCreditoAction',  '_route' => 'empresabase_empresa_importar_credito',);
            }

            // empresabase_my
            if ('/empresa-base/my' === $pathinfo) {
                return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaBase\\HomeController::myAction',  '_route' => 'empresabase_my',);
            }

            if (0 === strpos($pathinfo, '/empresa-base/modificar-mis-datos')) {
                // empresabase_empresabase_modificacion_misdatos
                if ('/empresa-base/modificar-mis-datos' === $pathinfo) {
                    return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaBase\\EmpresaController::empresasModificacionMisDatosAction',  '_route' => 'empresabase_empresabase_modificacion_misdatos',);
                }

                // empresabase_empresabase_modificacion_misdatos_clave
                if ('/empresa-base/modificar-mis-datos-clave' === $pathinfo) {
                    return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaBase\\EmpresaController::empresasModificacionMisDatosClaveAction',  '_route' => 'empresabase_empresabase_modificacion_misdatos_clave',);
                }

            }

            elseif (0 === strpos($pathinfo, '/empresa-base/ajax')) {
                if (0 === strpos($pathinfo, '/empresa-base/ajax/empresas-my')) {
                    // empresabase_ajax_empresa_update_my
                    if ('/empresa-base/ajax/empresas-my' === $pathinfo && $request->isXmlHttpRequest()) {
                        $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaBase\\EmpresaController::putUpdateMisDatosAction',  '_route' => 'empresabase_ajax_empresa_update_my',);
                        if (!in_array($requestMethod, ['PUT'])) {
                            $allow = array_merge($allow, ['PUT']);
                            goto not_empresabase_ajax_empresa_update_my;
                        }

                        return $ret;
                    }
                    not_empresabase_ajax_empresa_update_my:

                    // empresabase_ajax_usuario_clave
                    if ('/empresa-base/ajax/empresas-my/usuario-clave' === $pathinfo && $request->isXmlHttpRequest()) {
                        $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaBase\\EmpresaController::putClaveUpdateAction',  '_route' => 'empresabase_ajax_usuario_clave',);
                        if (!in_array($requestMethod, ['PUT'])) {
                            $allow = array_merge($allow, ['PUT']);
                            goto not_empresabase_ajax_usuario_clave;
                        }

                        return $ret;
                    }
                    not_empresabase_ajax_usuario_clave:

                }

                elseif (0 === strpos($pathinfo, '/empresa-base/ajax/credito')) {
                    // empresabase_ajax_creditos_listado_organismoPersona_search
                    if ('/empresa-base/ajax/creditos/listado/organismoPersona/search' === $pathinfo && $request->isXmlHttpRequest()) {
                        $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaBase\\CreditoController::searchOrganismoPersonaByCreditoAction',  '_route' => 'empresabase_ajax_creditos_listado_organismoPersona_search',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_empresabase_ajax_creditos_listado_organismoPersona_search;
                        }

                        return $ret;
                    }
                    not_empresabase_ajax_creditos_listado_organismoPersona_search:

                    // empresabase_ajax_credito_detalle
                    if (preg_match('#^/empresa\\-base/ajax/credito/(?P<credito>[^/]++)/searchDetalle$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'empresabase_ajax_credito_detalle']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaBase\\CreditoController::searchDetalleCreditoAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_empresabase_ajax_credito_detalle;
                        }

                        return $ret;
                    }
                    not_empresabase_ajax_credito_detalle:

                    // empresabase_ajax_credito_detalle_descarga
                    if ('/empresa-base/ajax/credito/detalle/descarga' === $pathinfo) {
                        return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaBase\\CreditoController::descargaAction',  '_route' => 'empresabase_ajax_credito_detalle_descarga',);
                    }

                }

                // empresabase_ajax_archivocsv_listado_search
                if ('/empresa-base/ajax/archivocsv/listado/search' === $pathinfo && $request->isXmlHttpRequest()) {
                    $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaBase\\ArchivoCsvController::searchArchivoCsvAction',  '_route' => 'empresabase_ajax_archivocsv_listado_search',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_empresabase_ajax_archivocsv_listado_search;
                    }

                    return $ret;
                }
                not_empresabase_ajax_archivocsv_listado_search:

            }

            // empresabase_archivocsv_listado
            if ('/empresa-base/archivocsv/listado' === $pathinfo) {
                return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaBase\\ArchivoCsvController::archivoCsvListadoAction',  '_route' => 'empresabase_archivocsv_listado',);
            }

            if (0 === strpos($pathinfo, '/empresa-base/credito')) {
                // empresabase_creditos_listado_organismos_personas
                if ('/empresa-base/creditos/listado/organismoPersona' === $pathinfo) {
                    return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaBase\\CreditoController::creditosOrganismoPersonaListadoAction',  '_route' => 'empresabase_creditos_listado_organismos_personas',);
                }

                // empresabase_credito_detalle
                if (preg_match('#^/empresa\\-base/credito/(?P<credito>[^/]++)/detalle$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'empresabase_credito_detalle']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaBase\\CreditoController::detalleCreditoAction',));
                }

                // empresabase_credito_listado_organismo_persona_detalle
                if (0 === strpos($pathinfo, '/empresa-base/credito/listado/organismo/persona') && preg_match('#^/empresa\\-base/credito/listado/organismo/persona/(?P<credito>[^/]++)/detalle$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'empresabase_credito_listado_organismo_persona_detalle']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaBase\\CreditoController::detalleListadoOrganismoPersonaByCreditoAction',));
                }

            }

            // empresabase_previewcsv
            if ('/empresa-base/previewcsv' === $pathinfo && $request->isXmlHttpRequest()) {
                $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaBase\\CreditoController::previewcsvAction',  '_route' => 'empresabase_previewcsv',);
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_empresabase_previewcsv;
                }

                return $ret;
            }
            not_empresabase_previewcsv:

            // empresabase_uploadcsv
            if ('/empresa-base/uploadcsv' === $pathinfo && $request->isXmlHttpRequest()) {
                $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\EmpresaBase\\CreditoController::uploadcsvAction',  '_route' => 'empresabase_uploadcsv',);
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_empresabase_uploadcsv;
                }

                return $ret;
            }
            not_empresabase_uploadcsv:

        }

        elseif (0 === strpos($pathinfo, '/hacienda')) {
            // hacienda_home
            if ('/hacienda/inicio' === $pathinfo) {
                return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\HomeController::haciendaHomeAction',  '_route' => 'hacienda_home',);
            }

            // hacienda_my
            if ('/hacienda/mis-datos' === $pathinfo) {
                return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\HomeController::myAction',  '_route' => 'hacienda_my',);
            }

            // hacienda_hacienda_modificacion_misdatos_clave
            if ('/hacienda/modificar-mis-datos-clave' === $pathinfo) {
                return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\HomeController::haciendaModificacionMisDatosClaveAction',  '_route' => 'hacienda_hacienda_modificacion_misdatos_clave',);
            }

            if (0 === strpos($pathinfo, '/hacienda/ajax')) {
                // hacienda_ajax_usuario_clave
                if ('/hacienda/ajax/usuario-clave' === $pathinfo && $request->isXmlHttpRequest()) {
                    $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\HomeController::putClaveUpdateAction',  '_route' => 'hacienda_ajax_usuario_clave',);
                    if (!in_array($requestMethod, ['PUT'])) {
                        $allow = array_merge($allow, ['PUT']);
                        goto not_hacienda_ajax_usuario_clave;
                    }

                    return $ret;
                }
                not_hacienda_ajax_usuario_clave:

                // hacienda_ajax_busquedas__search
                if ('/hacienda/ajax/busquedas/search' === $pathinfo && $request->isXmlHttpRequest()) {
                    $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\HomeController::busquedasSearchAction',  '_route' => 'hacienda_ajax_busquedas__search',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_hacienda_ajax_busquedas__search;
                    }

                    return $ret;
                }
                not_hacienda_ajax_busquedas__search:

                // hacienda_ajax_busquedas_descargar
                if ('/hacienda/ajax/busquedas/descargar' === $pathinfo) {
                    return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\HomeController::descargaAction',  '_route' => 'hacienda_ajax_busquedas_descargar',);
                }

                if (0 === strpos($pathinfo, '/hacienda/ajax/empresas')) {
                    // hacienda_ajax_empresa_listado
                    if ('/hacienda/ajax/empresas/search' === $pathinfo && $request->isXmlHttpRequest()) {
                        $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\EmpresaController::searchAction',  '_route' => 'hacienda_ajax_empresa_listado',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_hacienda_ajax_empresa_listado;
                        }

                        return $ret;
                    }
                    not_hacienda_ajax_empresa_listado:

                    // hacienda_ajax_empresa_save
                    if ('/hacienda/ajax/empresas' === $pathinfo && $request->isXmlHttpRequest()) {
                        $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\EmpresaController::postSaveAction',  '_route' => 'hacienda_ajax_empresa_save',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_hacienda_ajax_empresa_save;
                        }

                        return $ret;
                    }
                    not_hacienda_ajax_empresa_save:

                    // hacienda_ajax_empresa_get_by_id
                    if (preg_match('#^/hacienda/ajax/empresas/(?P<id>[^/]++)$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'hacienda_ajax_empresa_get_by_id']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\EmpresaController::getEmpresaByIdAction',));
                        if (!in_array($canonicalMethod, ['GET'])) {
                            $allow = array_merge($allow, ['GET']);
                            goto not_hacienda_ajax_empresa_get_by_id;
                        }

                        return $ret;
                    }
                    not_hacienda_ajax_empresa_get_by_id:

                    // hacienda_ajax_empresa_update
                    if (preg_match('#^/hacienda/ajax/empresas/(?P<id>[^/]++)$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'hacienda_ajax_empresa_update']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\EmpresaController::putUpdateAction',));
                        if (!in_array($requestMethod, ['PUT'])) {
                            $allow = array_merge($allow, ['PUT']);
                            goto not_hacienda_ajax_empresa_update;
                        }

                        return $ret;
                    }
                    not_hacienda_ajax_empresa_update:

                }

                // hacienda_ajax_empresas_get_search
                if ('/hacienda/ajax/search/empresas' === $pathinfo && $request->isXmlHttpRequest()) {
                    $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\EmpresaController::getSearchOrganizacionAction',  '_route' => 'hacienda_ajax_empresas_get_search',);
                    if (!in_array($canonicalMethod, ['GET'])) {
                        $allow = array_merge($allow, ['GET']);
                        goto not_hacienda_ajax_empresas_get_search;
                    }

                    return $ret;
                }
                not_hacienda_ajax_empresas_get_search:

                if (0 === strpos($pathinfo, '/hacienda/ajax/credito')) {
                    // ajax_credito_detalle
                    if (preg_match('#^/hacienda/ajax/credito/(?P<credito>[^/]++)/searchDetalle$#sD', $pathinfo, $matches) && $request->isXmlHttpRequest()) {
                        $ret = $this->mergeDefaults(array_replace($matches, ['_route' => 'ajax_credito_detalle']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\CreditoController::searchDetalleCreditoAction',));
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_ajax_credito_detalle;
                        }

                        return $ret;
                    }
                    not_ajax_credito_detalle:

                    // hacienda_ajax_credito_detalle_descarga
                    if ('/hacienda/ajax/credito/detalle/descarga' === $pathinfo) {
                        return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\CreditoController::descargaAction',  '_route' => 'hacienda_ajax_credito_detalle_descarga',);
                    }

                    // ajax_creditos_listado_organismoPersona_search
                    if ('/hacienda/ajax/creditos/listado/organismoPersona/search' === $pathinfo && $request->isXmlHttpRequest()) {
                        $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\CreditoController::searchOrganismoPersonaByCreditoAction',  '_route' => 'ajax_creditos_listado_organismoPersona_search',);
                        if (!in_array($requestMethod, ['POST'])) {
                            $allow = array_merge($allow, ['POST']);
                            goto not_ajax_creditos_listado_organismoPersona_search;
                        }

                        return $ret;
                    }
                    not_ajax_creditos_listado_organismoPersona_search:

                }

                // hacienda_ajax_archivocsv_listado_search
                if ('/hacienda/ajax/archivocsv/listado/search' === $pathinfo && $request->isXmlHttpRequest()) {
                    $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\ArchivoCsvController::searchArchivoCsvAction',  '_route' => 'hacienda_ajax_archivocsv_listado_search',);
                    if (!in_array($requestMethod, ['POST'])) {
                        $allow = array_merge($allow, ['POST']);
                        goto not_hacienda_ajax_archivocsv_listado_search;
                    }

                    return $ret;
                }
                not_hacienda_ajax_archivocsv_listado_search:

            }

            // hacienda_archivocsv_listado
            if ('/hacienda/archivocsv/listado' === $pathinfo) {
                return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\ArchivoCsvController::archivoCsvListadoAction',  '_route' => 'hacienda_archivocsv_listado',);
            }

            // hacienda_busquedas_listado
            if ('/hacienda/busquedas' === $pathinfo) {
                return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\HomeController::busquedasListadoAction',  '_route' => 'hacienda_busquedas_listado',);
            }

            if (0 === strpos($pathinfo, '/hacienda/empresa')) {
                if (0 === strpos($pathinfo, '/hacienda/empresa-concentradora')) {
                    // hacienda_empresaconc_nueva
                    if ('/hacienda/empresa-concentradora/nueva' === $pathinfo) {
                        return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\EmpresaController::organizacionNuevaAction',  '_route' => 'hacienda_empresaconc_nueva',);
                    }

                    // hacienda_empresaconc_modificacion
                    if (preg_match('#^/hacienda/empresa\\-concentradora/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'hacienda_empresaconc_modificacion']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\EmpresaController::organizacionesModificacionAction',));
                    }

                }

                elseif (0 === strpos($pathinfo, '/hacienda/empresa-base')) {
                    // hacienda_empresabase_nueva
                    if ('/hacienda/empresa-base/nueva' === $pathinfo) {
                        return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\EmpresaController::empresaNuevaAction',  '_route' => 'hacienda_empresabase_nueva',);
                    }

                    // hacienda_empresabase_modificacion
                    if (preg_match('#^/hacienda/empresa\\-base/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'hacienda_empresabase_modificacion']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\EmpresaController::empresasModificacionAction',));
                    }

                }

                elseif (0 === strpos($pathinfo, '/hacienda/empresas')) {
                    // hacienda_empresas_listado
                    if ('/hacienda/empresas' === $pathinfo) {
                        return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\EmpresaController::empresasListadoAction',  '_route' => 'hacienda_empresas_listado',);
                    }

                    // hacienda_empresa_importar_credito
                    if (preg_match('#^/hacienda/empresas/(?P<empresaId>[^/]++)/importarcredito$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'hacienda_empresa_importar_credito']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\EmpresaController::importarCreditoAction',));
                    }

                }

            }

            elseif (0 === strpos($pathinfo, '/hacienda/credito')) {
                // hacienda_credito_detalle
                if (preg_match('#^/hacienda/credito/(?P<credito>[^/]++)/detalle$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'hacienda_credito_detalle']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\CreditoController::detalleCreditoAction',));
                }

                // hacienda_creditos_listado_organismos_personas
                if ('/hacienda/creditos/listado/organismoPersona' === $pathinfo) {
                    return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\CreditoController::creditosOrganismoPersonaListadoAction',  '_route' => 'hacienda_creditos_listado_organismos_personas',);
                }

                // hacienda_credito_listado_organismo_persona_detalle
                if (0 === strpos($pathinfo, '/hacienda/credito/listado/organismo/persona') && preg_match('#^/hacienda/credito/listado/organismo/persona/(?P<credito>[^/]++)/detalle$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'hacienda_credito_listado_organismo_persona_detalle']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\CreditoController::detalleListadoOrganismoPersonaByCreditoAction',));
                }

            }

            // hacienda_previewcsv
            if ('/hacienda/previewcsv' === $pathinfo && $request->isXmlHttpRequest()) {
                $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\CreditoController::previewcsvAction',  '_route' => 'hacienda_previewcsv',);
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_hacienda_previewcsv;
                }

                return $ret;
            }
            not_hacienda_previewcsv:

            // hacienda_uploadcsv
            if ('/hacienda/uploadcsv' === $pathinfo && $request->isXmlHttpRequest()) {
                $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\Hacienda\\CreditoController::uploadcsvAction',  '_route' => 'hacienda_uploadcsv',);
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_hacienda_uploadcsv;
                }

                return $ret;
            }
            not_hacienda_uploadcsv:

        }

        elseif (0 === strpos($pathinfo, '/user/public')) {
            // public_nueva_clave
            if (0 === strpos($pathinfo, '/user/public/generar_clave') && preg_match('#^/user/public/generar_clave/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'public_nueva_clave']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\PublicUserController::generarClaveAction',));
            }

            if (0 === strpos($pathinfo, '/user/public/recuperar_clave')) {
                // public_recuperar_clave
                if ('/user/public/recuperar_clave' === $pathinfo) {
                    return array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\PublicUserController::recuperarClaveAction',  '_route' => 'public_recuperar_clave',);
                }

                // public_recuperar_clave_update
                if (0 === strpos($pathinfo, '/user/public/recuperar_clave_update') && preg_match('#^/user/public/recuperar_clave_update/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'public_recuperar_clave_update']), array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\PublicUserController::recuperarClaveUpdateAction',));
                }

            }

            // public_ajax_usuario_clave_update
            if ('/user/public/ajax/usuarios/clave' === $pathinfo && $request->isXmlHttpRequest()) {
                $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\PublicUserController::putClaveUpdateAction',  '_route' => 'public_ajax_usuario_clave_update',);
                if (!in_array($requestMethod, ['PUT'])) {
                    $allow = array_merge($allow, ['PUT']);
                    goto not_public_ajax_usuario_clave_update;
                }

                return $ret;
            }
            not_public_ajax_usuario_clave_update:

            // public_ajax_usuario_recuperar_clave_email
            if ('/user/public/ajax/usuario/recuperar_clave_email' === $pathinfo && $request->isXmlHttpRequest()) {
                $ret = array (  '_controller' => 'Argentina\\PaisDigital\\MicroCreditoBundle\\Controller\\PublicUserController::recuperarClaveEmailAction',  '_route' => 'public_ajax_usuario_recuperar_clave_email',);
                if (!in_array($requestMethod, ['POST'])) {
                    $allow = array_merge($allow, ['POST']);
                    goto not_public_ajax_usuario_recuperar_clave_email;
                }

                return $ret;
            }
            not_public_ajax_usuario_recuperar_clave_email:

        }

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
