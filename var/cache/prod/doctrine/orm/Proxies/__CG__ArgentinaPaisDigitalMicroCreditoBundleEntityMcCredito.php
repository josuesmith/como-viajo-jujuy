<?php

namespace Proxies\__CG__\Argentina\PaisDigital\MicroCreditoBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class McCredito extends \Argentina\PaisDigital\MicroCreditoBundle\Entity\McCredito implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Proxy\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'id', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'montoCredito', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'fechaEntregaCredito', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'tasaInteres', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'saldoTotalCapital', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'saldoTotalCapitalInteres', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'vencidoCapitalInteres', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'diasAtraso', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'observaciones', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'creditoIdCsv', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'modoCobro', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'diasPrestamo', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'personaFisica', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'personaJuridica', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'personaJuridicaConcentradora', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'situacion', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'cuotasAtrasadas', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'fechaEstimadaFin', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'archivoCsv', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'fechaCreacion', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'reclamos'];
        }

        return ['__isInitialized__', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'id', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'montoCredito', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'fechaEntregaCredito', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'tasaInteres', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'saldoTotalCapital', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'saldoTotalCapitalInteres', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'vencidoCapitalInteres', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'diasAtraso', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'observaciones', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'creditoIdCsv', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'modoCobro', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'diasPrestamo', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'personaFisica', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'personaJuridica', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'personaJuridicaConcentradora', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'situacion', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'cuotasAtrasadas', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'fechaEstimadaFin', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'archivoCsv', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'fechaCreacion', '' . "\0" . 'Argentina\\PaisDigital\\MicroCreditoBundle\\Entity\\McCredito' . "\0" . 'reclamos'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (McCredito $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setId(int $id)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setId', [$id]);

        return parent::setId($id);
    }

    /**
     * {@inheritDoc}
     */
    public function getMontoCredito()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMontoCredito', []);

        return parent::getMontoCredito();
    }

    /**
     * {@inheritDoc}
     */
    public function setMontoCredito(float $montoCredito)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMontoCredito', [$montoCredito]);

        return parent::setMontoCredito($montoCredito);
    }

    /**
     * {@inheritDoc}
     */
    public function getFechaEntregaCredito()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFechaEntregaCredito', []);

        return parent::getFechaEntregaCredito();
    }

    /**
     * {@inheritDoc}
     */
    public function setFechaEntregaCredito($fechaEntregaCredito)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFechaEntregaCredito', [$fechaEntregaCredito]);

        return parent::setFechaEntregaCredito($fechaEntregaCredito);
    }

    /**
     * {@inheritDoc}
     */
    public function getTasaInteres()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTasaInteres', []);

        return parent::getTasaInteres();
    }

    /**
     * {@inheritDoc}
     */
    public function setTasaInteres(float $tasaInteres)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTasaInteres', [$tasaInteres]);

        return parent::setTasaInteres($tasaInteres);
    }

    /**
     * {@inheritDoc}
     */
    public function getSaldoTotalCapital()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSaldoTotalCapital', []);

        return parent::getSaldoTotalCapital();
    }

    /**
     * {@inheritDoc}
     */
    public function setSaldoTotalCapital(float $saldoTotalCapital)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSaldoTotalCapital', [$saldoTotalCapital]);

        return parent::setSaldoTotalCapital($saldoTotalCapital);
    }

    /**
     * {@inheritDoc}
     */
    public function getSaldoTotalCapitalInteres()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSaldoTotalCapitalInteres', []);

        return parent::getSaldoTotalCapitalInteres();
    }

    /**
     * {@inheritDoc}
     */
    public function setSaldoTotalCapitalInteres(float $saldoTotalCapitalInteres)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSaldoTotalCapitalInteres', [$saldoTotalCapitalInteres]);

        return parent::setSaldoTotalCapitalInteres($saldoTotalCapitalInteres);
    }

    /**
     * {@inheritDoc}
     */
    public function getVencidoCapitalInteres()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getVencidoCapitalInteres', []);

        return parent::getVencidoCapitalInteres();
    }

    /**
     * {@inheritDoc}
     */
    public function setVencidoCapitalInteres(float $vencidoCapitalInteres)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setVencidoCapitalInteres', [$vencidoCapitalInteres]);

        return parent::setVencidoCapitalInteres($vencidoCapitalInteres);
    }

    /**
     * {@inheritDoc}
     */
    public function getDiasAtraso()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDiasAtraso', []);

        return parent::getDiasAtraso();
    }

    /**
     * {@inheritDoc}
     */
    public function setDiasAtraso(int $diasAtraso)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDiasAtraso', [$diasAtraso]);

        return parent::setDiasAtraso($diasAtraso);
    }

    /**
     * {@inheritDoc}
     */
    public function getObservaciones()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getObservaciones', []);

        return parent::getObservaciones();
    }

    /**
     * {@inheritDoc}
     */
    public function setObservaciones(string $observaciones)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setObservaciones', [$observaciones]);

        return parent::setObservaciones($observaciones);
    }

    /**
     * {@inheritDoc}
     */
    public function getModoCobro()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getModoCobro', []);

        return parent::getModoCobro();
    }

    /**
     * {@inheritDoc}
     */
    public function setModoCobro(string $modoCobro)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setModoCobro', [$modoCobro]);

        return parent::setModoCobro($modoCobro);
    }

    /**
     * {@inheritDoc}
     */
    public function getDiasPrestamo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDiasPrestamo', []);

        return parent::getDiasPrestamo();
    }

    /**
     * {@inheritDoc}
     */
    public function setDiasPrestamo(int $diasPrestamo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDiasPrestamo', [$diasPrestamo]);

        return parent::setDiasPrestamo($diasPrestamo);
    }

    /**
     * {@inheritDoc}
     */
    public function getPersonaFisica()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPersonaFisica', []);

        return parent::getPersonaFisica();
    }

    /**
     * {@inheritDoc}
     */
    public function setPersonaFisica($personaFisica)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPersonaFisica', [$personaFisica]);

        return parent::setPersonaFisica($personaFisica);
    }

    /**
     * {@inheritDoc}
     */
    public function getPersonaJuridica()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPersonaJuridica', []);

        return parent::getPersonaJuridica();
    }

    /**
     * {@inheritDoc}
     */
    public function setPersonaJuridica($personaJuridica)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPersonaJuridica', [$personaJuridica]);

        return parent::setPersonaJuridica($personaJuridica);
    }

    /**
     * {@inheritDoc}
     */
    public function getSituacion()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSituacion', []);

        return parent::getSituacion();
    }

    /**
     * {@inheritDoc}
     */
    public function setSituacion($situacion)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSituacion', [$situacion]);

        return parent::setSituacion($situacion);
    }

    /**
     * {@inheritDoc}
     */
    public function getCuotasAtrasadas()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCuotasAtrasadas', []);

        return parent::getCuotasAtrasadas();
    }

    /**
     * {@inheritDoc}
     */
    public function getFechaEstimadaFin()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFechaEstimadaFin', []);

        return parent::getFechaEstimadaFin();
    }

    /**
     * {@inheritDoc}
     */
    public function setCuotasAtrasadas($cuotasAtrasadas)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCuotasAtrasadas', [$cuotasAtrasadas]);

        return parent::setCuotasAtrasadas($cuotasAtrasadas);
    }

    /**
     * {@inheritDoc}
     */
    public function setFechaEstimadaFin($fechaEstimadaFin)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFechaEstimadaFin', [$fechaEstimadaFin]);

        return parent::setFechaEstimadaFin($fechaEstimadaFin);
    }

    /**
     * {@inheritDoc}
     */
    public function getArchivoCsv()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getArchivoCsv', []);

        return parent::getArchivoCsv();
    }

    /**
     * {@inheritDoc}
     */
    public function setArchivoCsv($archivoCsv)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setArchivoCsv', [$archivoCsv]);

        return parent::setArchivoCsv($archivoCsv);
    }

    /**
     * {@inheritDoc}
     */
    public function getFechaCreacion()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFechaCreacion', []);

        return parent::getFechaCreacion();
    }

    /**
     * {@inheritDoc}
     */
    public function setFechaCreacion($fechaCreacion)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFechaCreacion', [$fechaCreacion]);

        return parent::setFechaCreacion($fechaCreacion);
    }

    /**
     * {@inheritDoc}
     */
    public function getPersonaJuridicaConcentradora()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPersonaJuridicaConcentradora', []);

        return parent::getPersonaJuridicaConcentradora();
    }

    /**
     * {@inheritDoc}
     */
    public function setPersonaJuridicaConcentradora($personaJuridicaConcentradora)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPersonaJuridicaConcentradora', [$personaJuridicaConcentradora]);

        return parent::setPersonaJuridicaConcentradora($personaJuridicaConcentradora);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreditoIdCsv()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreditoIdCsv', []);

        return parent::getCreditoIdCsv();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreditoIdCsv($creditoIdCsv)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreditoIdCsv', [$creditoIdCsv]);

        return parent::setCreditoIdCsv($creditoIdCsv);
    }

    /**
     * {@inheritDoc}
     */
    public function addReclamo(\Argentina\PaisDigital\MicroCreditoBundle\Entity\McReclamo $reclamo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addReclamo', [$reclamo]);

        return parent::addReclamo($reclamo);
    }

    /**
     * {@inheritDoc}
     */
    public function removeReclamo(\Argentina\PaisDigital\MicroCreditoBundle\Entity\McReclamo $reclamo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeReclamo', [$reclamo]);

        return parent::removeReclamo($reclamo);
    }

    /**
     * {@inheritDoc}
     */
    public function getReclamos()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getReclamos', []);

        return parent::getReclamos();
    }

}
