<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @ArgentinaPaisDigitalMicroCredito/Default/base.html.twig */
class __TwigTemplate_5c40f672dabe97c55de8ba782ba16bceaafd29971f5eef9636109cd111d439a4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html data-ng-app=\"app\" lang=\"es\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/node_modules/bootstrap/dist/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/css/droid-serif.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/node_modules/jquery-ui/jquery-ui.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/node_modules/@fortawesome/fontawesome-free/css/all.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link rel=\"shortcut icon\" href=\"favicon.ico\" />
        <meta name=\"viewport\" content=\"width=device-width, user-scalable=yes, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0\" >

    ";
        // line 15
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 16
        echo "    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />

</head>
";
        // line 19
        $this->loadTemplate("@ArgentinaPaisDigitalMicroCredito/Default/config.html.twig", "@ArgentinaPaisDigitalMicroCredito/Default/base.html.twig", 19)->display($context);
        // line 20
        echo "
<body>
    <div class=\" page-content\" data-ng-controller=\"mainController\" style=\"margin-bottom:100px\">
        <main role=\"main\">
            <div style=\"position:relative;\">
                ";
        // line 26
        echo "                <header class=\"container\">
\t\t\t\t<div class=\"tituloLogo align-self-center\">
\t\t\t\t\t\t\t <h1><a href=\"";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_home");
        echo "\">MicroCrédito</a></h1>
\t\t\t\t\t\t\t <div>| Proyecto de microcréditos.</div>
\t\t\t\t</div>
                </header>
                ";
        // line 33
        echo "                ";
        if (twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 33)) {
            // line 34
            echo "                    ";
            $this->loadTemplate("@ArgentinaPaisDigitalMicroCredito/Default/nav.html.twig", "@ArgentinaPaisDigitalMicroCredito/Default/base.html.twig", 34)->display($context);
            // line 35
            echo "                ";
        }
        // line 36
        echo "                ";
        if ((((isset($context["breadcrumbs"]) || array_key_exists("breadcrumbs", $context))) ? (_twig_default_filter(($context["breadcrumbs"] ?? null))) : (""))) {
            // line 37
            echo "                    ";
            // line 38
            echo "                    <nav aria-label=\"breadcrumb\" class=\"container\">
                        <ol class=\"breadcrumb\">
                            ";
            // line 40
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
                // line 41
                echo "                                ";
                if ((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["breadcrumb"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["active"] ?? null) : null)) {
                    // line 42
                    echo "                                    <li class=\"breadcrumb-item active\" aria-current=\"page\">";
                    echo twig_escape_filter($this->env, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["breadcrumb"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["label"] ?? null) : null), "html", null, true);
                    echo "</li>
                                    ";
                } else {
                    // line 44
                    echo "                                    <li class=\"breadcrumb-item\"><a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath((($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["breadcrumb"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["route"] ?? null) : null), (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["breadcrumb"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["params"] ?? null) : null)), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["breadcrumb"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["label"] ?? null) : null), "html", null, true);
                    echo "</a></li>
                                    ";
                }
                // line 46
                echo "                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "                        </ol>
                    </nav>
                ";
        }
        // line 50
        echo "\t\t\t\t<div class=\"container\" style=\"margin-bottom:40px\">
                ";
        // line 52
        echo "                ";
        $this->displayBlock('body', $context, $blocks);
        // line 54
        echo "                
               
\t\t\t\t</div>
\t\t\t\t
            </div>
\t\t\t";
        // line 60
        echo "\t\t\t<footer class=\"footerBase\">
\t\t\t\t\t<div class=\"container\" style=\"text-align:center\">
\t\t\t\t\t\t<img src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/images/logo_modernizacion-escudo-presidencia.svg"), "html", null, true);
        echo "\" style=\"height:80px\"> 
\t\t\t\t\t</div>
\t\t\t\t</footer>

            <script type=\"text/javascript\" src=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/node_modules/angular/angular.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/node_modules/jquery/dist/jquery.min.js"), "html", null, true);
        echo "\" ></script>
            <script type=\"text/javascript\" src=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/node_modules/popper.js/dist/umd/popper.min.js"), "html", null, true);
        echo "\" crossorigin=\"anonymous\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/node_modules/jquery-ui/jquery-ui.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/node_modules/bootstrap/dist/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/node_modules/jquery-ui-datepicker-with-i18n/ui/i18n/jquery.ui.datepicker-es.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/node_modules/downloadjs/download.js"), "html", null, true);
        echo "\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/node_modules/jquery-validation/dist/jquery.validate.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/node_modules/jquery-validation/dist/additional-methods.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/node_modules/jquery-validation/dist/localization/messages_es_AR.js"), "html", null, true);
        echo "\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/node_modules/angular-bootstrap/dist/ui-bootstrap-tpls.js"), "html", null, true);
        echo "\"></script>

            <script type=\"text/javascript\" src=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/js/app.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/js/controllers/mainController.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/js/controllers/utilsController.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/js/services/utilService.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/js/services/ajaxService.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("app/js/directives/custom.directive.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\">

                var acc = document.getElementsByClassName(\"accordion\");
                var i;
                for (i = 0; i < acc.length; i++) {
                    acc[i].addEventListener(\"click\", function () {
                        this.classList.toggle(\"activeAcc\");
                        var panel = this.nextElementSibling;
                        if (panel.style.maxHeight) {
                            panel.style.maxHeight = null;
                        } else {
                            if (panel.id == 'detalle') {
                                panel.style.maxHeight = \"200px\";
                            } else {
                                panel.style.maxHeight = panel.scrollHeight + \"px\";
                            }
                        }
                    });
                }
            </script>

        ";
        // line 107
        $this->displayBlock('javascripts', $context, $blocks);
        // line 108
        echo "    </main>
</div>
</body>
</html>





";
    }

    // line 6
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 15
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 52
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 53
        echo "                ";
    }

    // line 107
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "@ArgentinaPaisDigitalMicroCredito/Default/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  305 => 107,  301 => 53,  297 => 52,  291 => 15,  285 => 6,  272 => 108,  270 => 107,  245 => 85,  241 => 84,  237 => 83,  233 => 82,  229 => 81,  225 => 80,  220 => 78,  215 => 76,  211 => 75,  207 => 74,  202 => 72,  198 => 71,  194 => 70,  190 => 69,  186 => 68,  182 => 67,  178 => 66,  171 => 62,  167 => 60,  160 => 54,  157 => 52,  154 => 50,  149 => 47,  143 => 46,  135 => 44,  129 => 42,  126 => 41,  122 => 40,  118 => 38,  116 => 37,  113 => 36,  110 => 35,  107 => 34,  104 => 33,  97 => 28,  93 => 26,  86 => 20,  84 => 19,  77 => 16,  75 => 15,  68 => 11,  64 => 10,  60 => 9,  56 => 8,  52 => 7,  48 => 6,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@ArgentinaPaisDigitalMicroCredito/Default/base.html.twig", "/Applications/XAMPP/xamppfiles/htdocs/servicios/src/Argentina/PaisDigital/MicroCreditoBundle/Resources/views/Default/base.html.twig");
    }
}
