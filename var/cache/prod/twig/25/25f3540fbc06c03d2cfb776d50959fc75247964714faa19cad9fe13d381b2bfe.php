<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @ArgentinaPaisDigitalMicroCredito/Default/error.html.twig */
class __TwigTemplate_cae0cb06cc43e988fcabd8bab2dc053bb05da59b38e6387118bcbed5b11eb9f7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@ArgentinaPaisDigitalMicroCredito/Default/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@ArgentinaPaisDigitalMicroCredito/Default/base.html.twig", "@ArgentinaPaisDigitalMicroCredito/Default/error.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "    ";
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
        echo "
";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <section>
\t\t<br>
\t\t<br>
\t\t<div class=\"text-center text-danger\" >
\t\t\t";
        // line 10
        if ((($context["error"] ?? null) == 1)) {
            // line 11
            echo "\t\t\t\tLa página solicitada no existe.
\t\t\t";
        }
        // line 13
        echo "\t\t</div>
\t\t<br>
\t\t<br>
\t\t<br>
\t\t<br>
\t\t<div class=\"text-center\" >
\t\t\t";
        // line 19
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 20
            echo "\t\t\t\t<a class=\"btnM colorBase\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_home");
            echo "\" > &nbsp;Inicio</a>
\t\t\t";
        }
        // line 22
        echo "\t\t\t";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ORGANIZACION_CONC")) {
            // line 23
            echo "\t\t\t\t<a class=\"btnM colorBase\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("empresaconc_home");
            echo "\" > &nbsp;Inicio</a>
\t\t\t";
        }
        // line 25
        echo "\t\t</div>
    </section>
";
    }

    public function getTemplateName()
    {
        return "@ArgentinaPaisDigitalMicroCredito/Default/error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 25,  93 => 23,  90 => 22,  84 => 20,  82 => 19,  74 => 13,  70 => 11,  68 => 10,  62 => 6,  58 => 5,  51 => 3,  47 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@ArgentinaPaisDigitalMicroCredito/Default/error.html.twig", "/Applications/XAMPP/xamppfiles/htdocs/servicios/src/Argentina/PaisDigital/MicroCreditoBundle/Resources/views/Default/error.html.twig");
    }
}
