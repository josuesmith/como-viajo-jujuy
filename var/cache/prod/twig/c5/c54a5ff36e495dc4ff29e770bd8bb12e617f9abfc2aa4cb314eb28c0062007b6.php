<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @ArgentinaPaisDigitalMicroCredito/Default/config.html.twig */
class __TwigTemplate_feac7ebe1fd867bee5551c47b59202cab978ca45d595581de5a7cc12a3d9ec58 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<script>
    var baseUrl = \"";
        // line 2
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 2), "getSchemeAndHttpHost", [], "method", false, false, false, 2), "html", null, true);
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 2), "getBaseURL", [], "method", false, false, false, 2), "html", null, true);
        echo "/\";
    var baseUrlTemplates = \"";
        // line 3
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 3), "getSchemeAndHttpHost", [], "method", false, false, false, 3), "html", null, true);
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 3), "getBaseURL", [], "method", false, false, false, 3), "html", null, true);
        echo "/app/\";
    baseUrlTemplates = baseUrlTemplates.replace(/(app.php|app_dev.php)\\//, \"\");
    var baseUrlAdmin = baseUrl + \"admin/\";
    var baseUrlPublic = baseUrl + \"user/public/\";
    
    
    
    ";
        // line 10
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            echo " 
        var baseUrlByRol = baseUrl + \"admin/\";
    ";
        }
        // line 13
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_HACIENDA")) {
            echo " 
        var baseUrlByRol = baseUrl + \"hacienda/\";
    ";
        }
        // line 16
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ORGANIZACION_CONC")) {
            echo " 
        var baseUrlByRol = baseUrl + \"empresa-concentradora/\";
    ";
        }
        // line 19
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ORGANIZACION_BASE")) {
            echo " 
        var baseUrlByRol = baseUrl + \"empresa-base/\";
    ";
        }
        // line 22
        echo "    
    var baseUrlEmpresaConc = baseUrl + \"empresa-concentradora/\";
    var baseUrlEmpresaBase = baseUrl + \"empresa-base/\";
    var baseUrlHacienda = baseUrl + \"hacienda/\";
    var baseUrlCommon = baseUrl + \"common/\";
    var baseUrlRedirect = \"\";
    var urlImages = \"\";
    var urlLogos = \"\";
</script>
";
    }

    public function getTemplateName()
    {
        return "@ArgentinaPaisDigitalMicroCredito/Default/config.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 22,  76 => 19,  69 => 16,  62 => 13,  56 => 10,  45 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@ArgentinaPaisDigitalMicroCredito/Default/config.html.twig", "/Applications/XAMPP/xamppfiles/htdocs/servicios/src/Argentina/PaisDigital/MicroCreditoBundle/Resources/views/Default/config.html.twig");
    }
}
