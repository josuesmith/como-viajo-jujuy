<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'Argentina\PaisDigital\MicroCreditoBundle\Dto\ReclamoRequestDto' shared autowired service.

include_once $this->targetDirs[3].'/src/Argentina/PaisDigital/MicroCreditoBundle/Dto/ReclamoRequestDto.php';

return $this->services['Argentina\\PaisDigital\\MicroCreditoBundle\\Dto\\ReclamoRequestDto'] = new \Argentina\PaisDigital\MicroCreditoBundle\Dto\ReclamoRequestDto();
