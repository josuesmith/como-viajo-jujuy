<?php

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder4df30 = null;
    private $initializerd5d6a = null;
    private static $publicProperties5dd1b = [
        
    ];
    public function getConnection()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'getConnection', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->getConnection();
    }
    public function getMetadataFactory()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'getMetadataFactory', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->getMetadataFactory();
    }
    public function getExpressionBuilder()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'getExpressionBuilder', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->getExpressionBuilder();
    }
    public function beginTransaction()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'beginTransaction', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->beginTransaction();
    }
    public function getCache()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'getCache', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->getCache();
    }
    public function transactional($func)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'transactional', array('func' => $func), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->transactional($func);
    }
    public function commit()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'commit', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->commit();
    }
    public function rollback()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'rollback', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->rollback();
    }
    public function getClassMetadata($className)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'getClassMetadata', array('className' => $className), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->getClassMetadata($className);
    }
    public function createQuery($dql = '')
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'createQuery', array('dql' => $dql), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->createQuery($dql);
    }
    public function createNamedQuery($name)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'createNamedQuery', array('name' => $name), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->createNamedQuery($name);
    }
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->createNativeQuery($sql, $rsm);
    }
    public function createNamedNativeQuery($name)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->createNamedNativeQuery($name);
    }
    public function createQueryBuilder()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'createQueryBuilder', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->createQueryBuilder();
    }
    public function flush($entity = null)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'flush', array('entity' => $entity), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->flush($entity);
    }
    public function find($entityName, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'find', array('entityName' => $entityName, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->find($entityName, $id, $lockMode, $lockVersion);
    }
    public function getReference($entityName, $id)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->getReference($entityName, $id);
    }
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->getPartialReference($entityName, $identifier);
    }
    public function clear($entityName = null)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'clear', array('entityName' => $entityName), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->clear($entityName);
    }
    public function close()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'close', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->close();
    }
    public function persist($entity)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'persist', array('entity' => $entity), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->persist($entity);
    }
    public function remove($entity)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'remove', array('entity' => $entity), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->remove($entity);
    }
    public function refresh($entity)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'refresh', array('entity' => $entity), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->refresh($entity);
    }
    public function detach($entity)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'detach', array('entity' => $entity), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->detach($entity);
    }
    public function merge($entity)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'merge', array('entity' => $entity), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->merge($entity);
    }
    public function copy($entity, $deep = false)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->copy($entity, $deep);
    }
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->lock($entity, $lockMode, $lockVersion);
    }
    public function getRepository($entityName)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'getRepository', array('entityName' => $entityName), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->getRepository($entityName);
    }
    public function contains($entity)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'contains', array('entity' => $entity), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->contains($entity);
    }
    public function getEventManager()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'getEventManager', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->getEventManager();
    }
    public function getConfiguration()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'getConfiguration', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->getConfiguration();
    }
    public function isOpen()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'isOpen', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->isOpen();
    }
    public function getUnitOfWork()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'getUnitOfWork', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->getUnitOfWork();
    }
    public function getHydrator($hydrationMode)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->getHydrator($hydrationMode);
    }
    public function newHydrator($hydrationMode)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->newHydrator($hydrationMode);
    }
    public function getProxyFactory()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'getProxyFactory', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->getProxyFactory();
    }
    public function initializeObject($obj)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'initializeObject', array('obj' => $obj), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->initializeObject($obj);
    }
    public function getFilters()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'getFilters', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->getFilters();
    }
    public function isFiltersStateClean()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'isFiltersStateClean', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->isFiltersStateClean();
    }
    public function hasFilters()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'hasFilters', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->hasFilters();
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? $reflection = new \ReflectionClass(__CLASS__);
        $instance = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);
        $instance->initializerd5d6a = $initializer;
        return $instance;
    }
    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;
        if (! $this->valueHolder4df30) {
            $reflection = $reflection ?: new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder4df30 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
        }
        $this->valueHolder4df30->__construct($conn, $config, $eventManager);
    }
    public function & __get($name)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, '__get', ['name' => $name], $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        if (isset(self::$publicProperties5dd1b[$name])) {
            return $this->valueHolder4df30->$name;
        }
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4df30;
            $backtrace = debug_backtrace(false);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    get_parent_class($this),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
            return;
        }
        $targetObject = $this->valueHolder4df30;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4df30;
            return $targetObject->$name = $value;
            return;
        }
        $targetObject = $this->valueHolder4df30;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, '__isset', array('name' => $name), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4df30;
            return isset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder4df30;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, '__unset', array('name' => $name), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4df30;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder4df30;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __clone()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, '__clone', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        $this->valueHolder4df30 = clone $this->valueHolder4df30;
    }
    public function __sleep()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, '__sleep', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return array('valueHolder4df30');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializerd5d6a = $initializer;
    }
    public function getProxyInitializer()
    {
        return $this->initializerd5d6a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'initializeProxy', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder4df30;
    }
    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHolder4df30;
    }
}
