<?php

class JMSSerializerAdapter_a5dd94b extends \FOS\RestBundle\Serializer\JMSSerializerAdapter implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder4df30 = null;
    private $initializerd5d6a = null;
    private static $publicProperties5dd1b = [
        
    ];
    public function serialize($data, $format, \FOS\RestBundle\Context\Context $context)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'serialize', array('data' => $data, 'format' => $format, 'context' => $context), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->serialize($data, $format, $context);
    }
    public function deserialize($data, $type, $format, \FOS\RestBundle\Context\Context $context)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'deserialize', array('data' => $data, 'type' => $type, 'format' => $format, 'context' => $context), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return $this->valueHolder4df30->deserialize($data, $type, $format, $context);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? $reflection = new \ReflectionClass(__CLASS__);
        $instance = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\FOS\RestBundle\Serializer\JMSSerializerAdapter $instance) {
            unset($instance->serializer, $instance->serializationContextFactory, $instance->deserializationContextFactory);
        }, $instance, 'FOS\\RestBundle\\Serializer\\JMSSerializerAdapter')->__invoke($instance);
        $instance->initializerd5d6a = $initializer;
        return $instance;
    }
    public function __construct(\JMS\Serializer\SerializerInterface $serializer, ?\JMS\Serializer\ContextFactory\SerializationContextFactoryInterface $serializationContextFactory = null, ?\JMS\Serializer\ContextFactory\DeserializationContextFactoryInterface $deserializationContextFactory = null)
    {
        static $reflection;
        if (! $this->valueHolder4df30) {
            $reflection = $reflection ?: new \ReflectionClass('FOS\\RestBundle\\Serializer\\JMSSerializerAdapter');
            $this->valueHolder4df30 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\FOS\RestBundle\Serializer\JMSSerializerAdapter $instance) {
            unset($instance->serializer, $instance->serializationContextFactory, $instance->deserializationContextFactory);
        }, $this, 'FOS\\RestBundle\\Serializer\\JMSSerializerAdapter')->__invoke($this);
        }
        $this->valueHolder4df30->__construct($serializer, $serializationContextFactory, $deserializationContextFactory);
    }
    public function & __get($name)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, '__get', ['name' => $name], $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        if (isset(self::$publicProperties5dd1b[$name])) {
            return $this->valueHolder4df30->$name;
        }
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4df30;
            $backtrace = debug_backtrace(false);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    get_parent_class($this),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
            return;
        }
        $targetObject = $this->valueHolder4df30;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4df30;
            return $targetObject->$name = $value;
            return;
        }
        $targetObject = $this->valueHolder4df30;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, '__isset', array('name' => $name), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4df30;
            return isset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder4df30;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, '__unset', array('name' => $name), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4df30;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder4df30;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __clone()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, '__clone', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        $this->valueHolder4df30 = clone $this->valueHolder4df30;
    }
    public function __sleep()
    {
        $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, '__sleep', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
        return array('valueHolder4df30');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\FOS\RestBundle\Serializer\JMSSerializerAdapter $instance) {
            unset($instance->serializer, $instance->serializationContextFactory, $instance->deserializationContextFactory);
        }, $this, 'FOS\\RestBundle\\Serializer\\JMSSerializerAdapter')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializerd5d6a = $initializer;
    }
    public function getProxyInitializer()
    {
        return $this->initializerd5d6a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializerd5d6a && ($this->initializerd5d6a->__invoke($valueHolder4df30, $this, 'initializeProxy', array(), $this->initializerd5d6a) || 1) && $this->valueHolder4df30 = $valueHolder4df30;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder4df30;
    }
    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHolder4df30;
    }
}
