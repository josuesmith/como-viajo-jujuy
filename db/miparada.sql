-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:8889
-- Tiempo de generación: 17-11-2019 a las 00:30:18
-- Versión del servidor: 5.7.26
-- Versión de PHP: 7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `miparada`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mc_empresas`
--

CREATE TABLE `mc_empresas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `mc_empresas`
--

INSERT INTO `mc_empresas` (`id`, `nombre`, `descripcion`) VALUES
(1, 'Union Bus', 'Una empresa de la capital.'),
(2, 'Xibi Xibi', 'Una empresa de la capital.'),
(3, 'El Urbano', 'El Urbano\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mc_lineas_colectivos`
--

CREATE TABLE `mc_lineas_colectivos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `mc_lineas_colectivos`
--

INSERT INTO `mc_lineas_colectivos` (`id`, `nombre`, `descripcion`, `id_empresa`) VALUES
(1, '12 A', 'Campo verde', 1),
(2, '9', 'ida', 3),
(3, '9', 'vuelta', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mc_paradas_colectivos`
--

CREATE TABLE `mc_paradas_colectivos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `id_linea` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `mc_paradas_colectivos`
--

INSERT INTO `mc_paradas_colectivos` (`id`, `nombre`, `latitud`, `longitud`, `id_linea`) VALUES
(1, 'Parada 1', -24.23981282289644, -24.23981282289644, 2),
(2, 'Parada 2', -24.23827108511887, -24.23827108511887, 2),
(3, 'Parada 3', -24.237467038375662, -24.237467038375662, 2),
(4, 'Parada 4', -24.236788944816315, -24.236788944816315, 2),
(5, 'Parada 5', -24.236120936582513, -24.236120936582513, 2),
(6, 'Parada 6', -24.23572777311798, -24.23572777311798, 2),
(7, 'Parada 7', -24.235280188970904, -24.235280188970904, 2),
(8, 'Parada 8', -24.234948168566273, -24.234948168566273, 2),
(9, 'Parada 9', -24.234564479082536, -24.234564479082536, 2),
(10, 'Parada 10', -24.23419332337381, -24.23419332337381, 2),
(11, 'Parada 11', -24.233856714171644, -24.233856714171644, 2),
(12, 'Parada 12', -24.23199999728091, -24.23199999728091, 2),
(13, 'Parada 13', -24.232720919059393, -24.232720919059393, 2),
(14, 'Parada 14', -24.229723166196596, -24.229723166196596, 2),
(15, 'Parada 15', -24.22589156533028, -24.22589156533028, 2),
(16, 'Parada 16', -24.20915463006611, -24.20915463006611, 2),
(17, 'Parada 17', -24.2002286092198, -24.2002286092198, 2),
(18, 'Parada 18', -24.197131312205006, -24.197131312205006, 2),
(19, 'Parada 19', -24.196093052538682, -24.196093052538682, 2),
(20, 'Parada 20', -24.193308211816888, -24.193308211816888, 2),
(21, 'Parada 21', -24.192660771980908, -24.192660771980908, 2),
(22, 'Parada 22', -24.189005443000028, -24.189005443000028, 2),
(23, 'Parada 23', -24.18422356893976, -24.18422356893976, 2),
(24, 'Parada 24', -24.18136781616852, -24.18136781616852, 2),
(25, 'Parada 25', -24.18078882797554, -24.18078882797554, 2),
(26, 'Parada 26', -24.179995735288802, -24.179995735288802, 2),
(27, 'Parada 27', -24.180297312957588, -24.180297312957588, 2),
(28, 'Parada 1', -24.18020035550894, -65.31467456370592, 3),
(29, 'Parada 2', -24.182177722344417, -65.31209796667099, 3),
(30, 'Parada 3', -24.185234704807005, -65.31121551990509, 3),
(31, 'Parada 4', -24.18909566531594, -65.30709028244019, 3),
(32, 'Parada 5', -24.193273653231305, -65.30201956629753, 3),
(33, 'Parada 6', -24.19540892807329, -65.29810857027769, 3),
(34, 'Parada 7', -24.19653588125946, -65.29432196170092, 3),
(35, 'Parada 8', -24.200429833061825, -65.28882544487715, 3),
(36, 'Parada 9', -24.20927388816348, -65.28315525501966, 3),
(37, 'Parada 10', -24.22588820208066, -65.27467511594296, 3),
(38, 'Parada 11', -24.22990110719493, -65.27587205171585, 3),
(39, 'Parada 12', -24.23350267737455, -65.27396902441978, 3),
(40, 'Parada 13', -24.233152308383126, -65.27154061943293, 3),
(41, 'Parada 14', -24.233435416386428, -65.26917390525341, 3),
(42, 'Parada 15', -24.233913580093898, -65.2669171616435, 3),
(43, 'Parada 16', -24.23436697790933, -65.26549626141787, 3),
(44, 'Parada 17', -24.234858284440357, -65.26395700871944, 3),
(45, 'Parada 18', -24.234972015364534, -65.26265110820532, 3),
(46, 'Parada 19', -24.235359983800944, -65.2614089101553, 3),
(47, 'Parada 20', -24.235756511390594, -65.26013653725386, 3),
(48, 'Parada 21', -24.236294282763126, -65.25842864066362, 3),
(49, 'Parada 22', -24.236907871314994, -65.25651354342699, 3),
(50, 'Parada 23', -24.237530017125227, -65.25452669709921, 3),
(51, 'Parada 24', -24.23803017735011, -65.25293748825788, 3),
(52, 'Parada 25', -24.238622968616383, -65.25106262415648, 3),
(53, 'Parada 26', -24.239720496472238, -65.25148808956146, 3),
(54, 'Parada 27', -24.19757138495869, -65.29103625565769, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mc_personas`
--

CREATE TABLE `mc_personas` (
  `id` int(8) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `apellido` varchar(255) DEFAULT NULL,
  `cuil` varchar(11) DEFAULT NULL,
  `dni` varchar(20) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_actualizacion` datetime DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mc_personas`
--

INSERT INTO `mc_personas` (`id`, `nombre`, `apellido`, `cuil`, `dni`, `fecha_nacimiento`, `fecha_creacion`, `fecha_actualizacion`, `email`) VALUES
(1, 'Mateo Ezequiel', 'Cruz', '20306662423', '30666242', '1984-05-19', '2017-12-04 00:00:00', '2019-11-15 14:24:16', 'noahtecnologias@gmail.com'),
(4592, 'Josué Joel', 'Salazar', '20328772060', '32877206', '1987-09-10', '2019-11-15 14:17:48', NULL, 'josuej.salazar@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mc_roles`
--

CREATE TABLE `mc_roles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `mc_roles`
--

INSERT INTO `mc_roles` (`id`, `nombre`, `descripcion`) VALUES
(1, 'ROLE_ADMIN', 'ADMINISTRADOR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mc_usuarios`
--

CREATE TABLE `mc_usuarios` (
  `id` int(11) NOT NULL,
  `usuario` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `id_persona` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `mc_usuarios`
--

INSERT INTO `mc_usuarios` (`id`, `usuario`, `password`, `salt`, `estado`, `fecha_creacion`, `fecha_actualizacion`, `id_persona`) VALUES
(1, 'mcruz', '$2y$04$M1.MkoZianLWp15oanhkuuPxWmWEa2kqSNURg6yzaKuM.eXxWfn8K', '', 1, '2019-01-01 00:00:00', '2019-11-15 14:24:16', 1),
(2, 'josuesmith', '$2y$04$gZt1.q/jBgtyiDYDnmhtm.RFtI4HaToIBSEZNdy0DabcM/OLMkh0u', '', 1, '2019-11-15 14:17:48', NULL, 4592);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mc_usuarios_roles`
--

CREATE TABLE `mc_usuarios_roles` (
  `usuario_id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `mc_usuarios_roles`
--

INSERT INTO `mc_usuarios_roles` (`usuario_id`, `rol_id`) VALUES
(1, 1),
(2, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `mc_empresas`
--
ALTER TABLE `mc_empresas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mc_lineas_colectivos`
--
ALTER TABLE `mc_lineas_colectivos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_empresa` (`id_empresa`);

--
-- Indices de la tabla `mc_paradas_colectivos`
--
ALTER TABLE `mc_paradas_colectivos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_linea` (`id_linea`);

--
-- Indices de la tabla `mc_personas`
--
ALTER TABLE `mc_personas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mc_roles`
--
ALTER TABLE `mc_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mc_usuarios`
--
ALTER TABLE `mc_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mc_usuarios_roles`
--
ALTER TABLE `mc_usuarios_roles`
  ADD PRIMARY KEY (`usuario_id`,`rol_id`),
  ADD KEY `IDX_F680F5D3DB38439E` (`usuario_id`),
  ADD KEY `IDX_F680F5D34BAB96C` (`rol_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `mc_empresas`
--
ALTER TABLE `mc_empresas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `mc_lineas_colectivos`
--
ALTER TABLE `mc_lineas_colectivos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `mc_paradas_colectivos`
--
ALTER TABLE `mc_paradas_colectivos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT de la tabla `mc_personas`
--
ALTER TABLE `mc_personas`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4593;

--
-- AUTO_INCREMENT de la tabla `mc_roles`
--
ALTER TABLE `mc_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `mc_usuarios`
--
ALTER TABLE `mc_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `mc_lineas_colectivos`
--
ALTER TABLE `mc_lineas_colectivos`
  ADD CONSTRAINT `mc_lineas_colectivos_ibfk_1` FOREIGN KEY (`id_empresa`) REFERENCES `mc_empresas` (`id`);

--
-- Filtros para la tabla `mc_paradas_colectivos`
--
ALTER TABLE `mc_paradas_colectivos`
  ADD CONSTRAINT `mc_paradas_colectivos_ibfk_1` FOREIGN KEY (`id_linea`) REFERENCES `mc_lineas_colectivos` (`id`);
